#pragma once

class Player;

#include "IForm.h"

class PlayerForm : public IForm
{
public:
	PlayerForm(const std::string defaultBodyPath);
	~PlayerForm();

	void updateForm(const Player& player);

private:
	IForm::TextPosition m_name;
	IForm::TextPosition m_resources;
	IForm::TextPosition m_devCards;
	IForm::TextPosition m_turnedDevelopementCards;
	std::vector<IForm::TextPosition> m_colorPositions;

private:
	void initPositions(const std::string defaultBodyPath);
	void initColorsPositions();

	void setName(const Player& player);
	void setResources(const Player& player);
	void setDevCards(const Player& player);
	void setTurnedDevelopementCards(const Player& player);
	void setColor(const Player& player);

};

