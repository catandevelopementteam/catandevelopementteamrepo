#pragma once
#include "Factory.h"
#include "IResource.h"
#include "IDevCard.h"
#include "IBuilding.h"
#include "Road.h"
#include "Settlement.h"
#include "City.h"
#include "Rules.h"
#include "IResourceHolder.h"

#include <vector>

class Bank : public IResourceHolder
{
private:
	using lumber_ptr = IResourceHolder::lumber_ptr;
	using brick_ptr = IResourceHolder::brick_ptr;
	using wool_ptr = IResourceHolder::wool_ptr;
	using grain_ptr = IResourceHolder::grain_ptr;
	using ore_ptr = IResourceHolder::ore_ptr;

	using knite_ptr = std::unique_ptr<Knite>;
	using monopoly_ptr = std::unique_ptr<Monopoly>;
	using roadBuilding_ptr = std::unique_ptr<RoadBuilding>;
	using yearOfPlenty_ptr = std::unique_ptr<YearOfPlenty>;
	using victoryPoint_ptr = std::unique_ptr<VictoryPoint>;

	using devCard_ptr = std::unique_ptr<IDevCard>;

	using road_ptr = IResourceHolder::road_ptr;
	using settlement_ptr = IResourceHolder::settlement_ptr;
	using city_ptr = IResourceHolder::city_ptr;

private:
	
	std::vector<devCard_ptr> m_devCards;
	
private:
	void initResourceCardStacks();

	void initDevCardStack();
	void initKnites();
	void initMonopoly();
	void initRoadBuiding();
	void initYearOfPlenty();
	void initVictoryPoint();

	void initBuildings();

	void initRoads();
	void initSettlements();
	void initCities();

public:
	Bank();
	//static Bank& getInstance();
	Bank(const Bank&) = delete;
	Bank& operator=(const Bank&) = delete;
	~Bank();

	bool isEmptyDevCardStack() const;
	bool areEnoughResources(ResourceType type, unsigned int number) const;

	void addDevCard(devCard_ptr type);
	devCard_ptr getDevCard();
	
};

