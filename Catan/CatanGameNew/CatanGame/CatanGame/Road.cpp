#include "Road.h"

uint8_t Road::s_instancesCreated = 0;

Road::Road()
{
	m_type = BuildingType::road;
	m_instancesAllowed = Rules::getNumOfRoads();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

void Road::setEdge(edge_ptr edge)
{
	m_edge = edge;
}

Road::edge_ptr Road::getEdge() const
{
	return m_edge;
}

Road::~Road()
{
}

void Road::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}