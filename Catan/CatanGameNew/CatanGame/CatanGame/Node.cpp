#include "Node.h"




Node::Node(const Node & other)
{
	*this = other;
}

Node & Node::operator=(const Node & other)
{
	m_id = other.m_id;
	m_portType = other.m_portType;	
	m_ownerId = other.m_ownerId;

	return *this;
}

Node::Node(Node && other)
{
	m_id = std::move(other.m_id);
	m_portType = std::move(other.m_portType);	
	m_ownerId = std::move(other.m_ownerId);
}

Node & Node::operator=(Node && other)
{
	if (this != &other)
	{
		m_id = std::move(other.m_id);
		m_portType = std::move(other.m_portType);		
		m_ownerId = std::move(other.m_ownerId);
	}
	return *this;
}

int Node::getId() const
{
	return m_id;
}

void Node::setOwnerId(int ownerId)
{
	m_ownerId = ownerId;
}

int  Node::getOwnerId() const
{
	return m_ownerId;
}

void Node::setPortType(int type)
{
	switch (type)
	{
	case 0: m_portType = PortType::genericPort; break;
	case 1: m_portType = PortType::brickPort; break;
	case 2: m_portType = PortType::lumberPort; break;
	case 3: m_portType = PortType::orePort; break;
	case 4: m_portType = PortType::woolPort; break;
	case 5: m_portType = PortType::grainPort; break;
	default: m_portType = PortType::none; break;
	}
}


Node::Node()
{
}

Node::Node(int id)
{
	m_id = id;	
	m_ownerId = 9;
}


PortType Node::getType() const
{
	return m_portType;
}

std::string Node::typePortName() const
{
	switch (m_portType)
	{
	case PortType::genericPort: return "GenericPort"; break;
	case PortType::brickPort: return "BrickPort"; break;
	case PortType::lumberPort: return "LumberPort"; break;
	case PortType::orePort: return "OrePort"; break;
	case PortType::woolPort: return "WoolPort"; break;
	case PortType::grainPort: return "GrainPort"; break;
	default: return "No Port"; break;

	}
}

void Node::printIntersection() const
{
	std::cout << "Intersection no. " << m_id << " is " << typePortName() << std::endl;
}



Node::~Node()
{
}

