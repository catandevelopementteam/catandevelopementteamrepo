#pragma once

//forward declaration of Factory
class Factory;

#include "IDevCard.h"

class Monopoly : public IDevCard
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	Monopoly();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~Monopoly();

	Monopoly(const Monopoly& other) = delete;
	Monopoly& operator=(const Monopoly& other) = delete;

	Monopoly(Monopoly&& other) = default;
	Monopoly& operator=(Monopoly&& other) = default;

};

