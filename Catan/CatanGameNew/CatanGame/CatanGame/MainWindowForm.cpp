#include "MainWindowForm.h"

MainWindowForm::MainWindowForm()
{
}

MainWindowForm::MainWindowForm(std::string defaultBodyPath, const Board& board) : IForm(defaultBodyPath)
{
	initForms(board);
	initFormPositions();
}

MainWindowForm::MainWindowForm(int rows, int columns, const Board& board) : IForm(rows, columns)
{
	initBackground();
	initForms(board);
	initFormPositions();
}


MainWindowForm::~MainWindowForm()
{
}

void MainWindowForm::updateForm(const Player& currentPlayer, 
	const std::vector<Player>& players, 
	const std::pair<uint8_t, uint8_t> dice,
	const Board& board)
{
	updateAllForms(currentPlayer, players, dice, board);
	includeAllForms();
}

void MainWindowForm::showIntersectiunID()
{
	m_boardForm.showIntersectiunID();
	includeBoardForm();
}

void MainWindowForm::showIntesectionOwner(const std::vector<Player>& players)
{
	m_boardForm.showIntesectionOwner(players);
	includeBoardForm();
}

void MainWindowForm::showRoadsID()
{
	m_boardForm.showRoadsID();
	includeBoardForm();
}

void MainWindowForm::showRoadsOwner(const std::vector<Player>& players)
{
	m_boardForm.showRoadsOwner(players);
	includeBoardForm();
}

void MainWindowForm::showHexagonsID()
{
	m_boardForm.showHexagonsID();
	includeBoardForm();
}

void MainWindowForm::hideHexagonID(const Board& board)
{
	m_boardForm.hideHexagonID(board);
	includeBoardForm();
}

void MainWindowForm::showMessages(std::vector<std::string> messages)
{
	m_optionsForm.addMessages(messages);
	includeOptionsForm();
}

void MainWindowForm::initForms(const Board& board)
{
	m_boardForm = BoardForm("txtFiles/BoardForm.txt", board);

	m_playerForms.push_back(PlayerForm("txtFiles/playerForm.txt"));
	m_playerForms.push_back(PlayerForm("txtFiles/playerForm.txt"));
	m_playerForms.push_back(PlayerForm("txtFiles/playerForm.txt"));
	m_playerForms.push_back(PlayerForm("txtFiles/playerForm.txt"));

	m_currentPlayerForm = CurrentPlayerForm("txtFiles/currentPlayerForm.txt");

	m_boardForm = BoardForm("txtFiles/boardForm.txt", board);

	m_optionsForm = OptionsForm("txtFiles/optionsForm.txt");

	m_diceForm = DiceForm("txtFiles/diceForm.txt");
}

void MainWindowForm::initFormPositions()
{
	m_playerFrom_pos.emplace_back(IForm::TextPosition(0, 1, 0));
	m_playerFrom_pos.emplace_back(IForm::TextPosition(0, 37, 0));
	m_playerFrom_pos.emplace_back(IForm::TextPosition(0, 73, 0));
	m_playerFrom_pos.emplace_back(IForm::TextPosition(0, 109, 0));

	m_currentPlayerForm_pos = IForm::TextPosition(8, 53, 0);

	m_boardForm_pos = IForm::TextPosition(8, 79, 0);

	m_optionsForm_pos = IForm::TextPosition(8, 1, 0);

	m_diceForm_pos = IForm::TextPosition(30, 53, 0);
}

void MainWindowForm::initBackground()
{
	int i = 0;
	for (auto& c : m_formBody)
	{
		if (m_formBody[i].getSymbol() == ' ')
		{
			m_formBody[i].setSymbol(' ');
		}
		i++;
	}

}

void MainWindowForm::makeEOL()
{
	for (size_t row = 0; row < m_formRows; row++)
	{
		changeCharacter(row, m_formColumns - 2, '\n');
	}
}

void MainWindowForm::updateAllForms(const Player& currentPlayer, 
	const std::vector<Player>& players, 
	const std::pair<uint8_t, uint8_t> dice, 
	const Board& board)
{
	//update Player Forms by passing the corresponding player
	for (uint8_t i = 0; i < players.size(); i++)
	{
		m_playerForms[i].updateForm(players[i]);
	}

	m_currentPlayerForm.updateForm(currentPlayer);
	m_boardForm.updateForm(players, board);
	m_optionsForm.updateForm();
	m_diceForm.updateForm(dice);
}

void MainWindowForm::updateCurrentPlayerForm(const Player & player)
{
	m_currentPlayerForm.updateForm(player);								
	includeCurrentPlayerForm();
}

void MainWindowForm::updatePlayerForms(const std::vector<Player>& players)
{
	for (size_t i = 0; i < players.size(); i++)
	{
		m_playerForms[i].updateForm(players[i]);						
		includePlayerForms();
	}
}

void MainWindowForm::updateDiceForm(const std::pair<uint8_t, uint8_t> dice)
{
	m_diceForm.updateForm(dice);						
	includeDiceForm();
}

void MainWindowForm::updateBoardForm(const std::vector<Player>& players, const Board& board)
{
	m_boardForm.updateForm(players, board);						
	includeBoardForm();
}

void MainWindowForm::updateOptionsForm()
{
	m_optionsForm.updateForm();
	includeOptionsForm();
}

void MainWindowForm::includeAllForms()
{
	//copy from each playerForms inside vector m_playerForms to the main form
	for(uint8_t playerID = 0 ; playerID < m_playerForms.size() ; playerID++)
	{
		copyFormToForm(m_playerForms[playerID], *this, m_playerFrom_pos[playerID]);
	}

	copyFormToForm(m_currentPlayerForm, *this, m_currentPlayerForm_pos);
	copyFormToForm(m_boardForm, *this, m_boardForm_pos);
	copyFormToForm(m_optionsForm, *this, m_optionsForm_pos);
	copyFormToForm(m_diceForm, *this, m_diceForm_pos);
}

void MainWindowForm::includePlayerForms()
{
	for (uint8_t playerID = 0; playerID < m_playerForms.size(); playerID++)
	{
		copyFormToForm(m_playerForms[playerID], *this, m_playerFrom_pos[playerID]);
	}
}

void MainWindowForm::includeCurrentPlayerForm()
{
	copyFormToForm(m_currentPlayerForm, *this, m_currentPlayerForm_pos);
}

void MainWindowForm::includeBoardForm()
{
	copyFormToForm(m_boardForm, *this, m_boardForm_pos);
}

void MainWindowForm::includeOptionsForm()
{
	copyFormToForm(m_optionsForm, *this, m_optionsForm_pos);
}

void MainWindowForm::includeDiceForm()
{
	copyFormToForm(m_diceForm, *this, m_diceForm_pos);
}
