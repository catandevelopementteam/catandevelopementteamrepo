#pragma once

#include <iostream>
#include <vector>

#include "ResourceType.h"
#include "BuildingType.h"
#include "DevCardType.h"
#include "PortType.h"
#include "TradeType.h"
#include "ActionType.h"

class Player;

class IInput
{
public:
	IInput();
	virtual ~IInput();

public:
	virtual DevCardType chooseTurnDevCard() = 0;

	virtual bool chooseTurnKnite() = 0;
	
	virtual uint8_t chooseHex() = 0;
	
	virtual const Player& choosePlayer(const std::vector<Player>& players, const Player& currentPlayer) = 0;
	
	virtual std::vector<ResourceType> chooseDiscardedCards(uint8_t totalNumber) = 0;
	
	virtual ActionType chooseAction() = 0;
	
	virtual BuildingType chooseBuild() = 0;

	virtual uint8_t chooseIntersection() = 0;

	virtual uint8_t chooseEdge() = 0;

	// !!!!!! trebuie rediscutat port-ul  !!!!!!
	virtual TradeType chooseTrade() = 0;

	virtual PortType choosePortType() = 0;

	//returns a vector of pairs. Each pair represents one resource type and its quantity
	virtual std::vector < ResourceType > chooseResourcesToReceive() = 0;

	//returns a vector of pairs. Each pair represents one resource type and its quantity
	virtual std::vector < ResourceType > chooseResourcesToTrade() = 0;

	virtual ResourceType chooseResourceToMonopolize() = 0;
};

