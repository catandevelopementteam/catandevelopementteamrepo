#pragma once

#include <memory>
#include <vector>

#include "Brick.h"
#include "Lumber.h"
#include "Grain.h"
#include "Ore.h"
#include "Wool.h"
#include "Road.h"
#include "Settlement.h"
#include "City.h"
#include "Rules.h"
#include "Container.h"


class IResourceHolder
{
public:
	using lumber_ptr = std::unique_ptr<Lumber>;
	using lumbers_type = container<lumber_ptr>;	
	using brick_ptr = std::unique_ptr<Brick>;
	using bricks_type = container<brick_ptr>;
	using wool_ptr = std::unique_ptr<Wool>;
	using wools_type = container<wool_ptr>;
	using grain_ptr = std::unique_ptr<Grain>;
	using grains_type = container<grain_ptr>;
	using ore_ptr = std::unique_ptr<Ore>;
	using ores_type = container<ore_ptr>;
	using settlement_ptr = std::unique_ptr<Settlement>;
	using settlements_type = container<settlement_ptr>;	
	using city_ptr = std::unique_ptr<City>;
	using cities_type = container<city_ptr>;	
	using road_ptr = std::unique_ptr<Road>;
	using roads_type = container<road_ptr>;
	

protected:
	lumbers_type m_lumbers;
	bricks_type m_bricks;
	wools_type m_wools;
	grains_type m_grains;
	ores_type m_ores;	
	settlements_type m_settlements;
	cities_type m_cities;
	roads_type m_roads;

public:
	IResourceHolder();
	virtual ~IResourceHolder();

	const lumbers_type& getLumbers() const;
	const bricks_type& getBricks() const;
	const wools_type& getWools() const;
	const grains_type& getGrains() const;
	const ores_type& getOres() const;	
		
	const settlements_type& getSettlements() const;
	settlements_type& getSettlements();
	const cities_type& getCities() const;
	cities_type& getCities();
	const roads_type& getRoads() const;
	roads_type& getRoads();

	void buyLumber(lumber_ptr lumber);
	lumber_ptr sellLumber();
	void buyBrick(brick_ptr brick);
	brick_ptr sellBrick();
	void buyWool(wool_ptr wool);
	wool_ptr sellWool();
	void buyGrain(grain_ptr grain);
	grain_ptr sellGrain();
	void buyOre(ore_ptr ore);
	ore_ptr sellOre();

	void addSettlement(settlement_ptr sett);
	settlement_ptr getSettlement();
	void addCity(city_ptr city);
	city_ptr getCity();
	void addRoad(road_ptr road);
	road_ptr getRoad();
	
};

