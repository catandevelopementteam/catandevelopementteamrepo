#include "DiceForm.h"


DiceForm::DiceForm()
{
}

DiceForm::DiceForm(std::string defaultBodyPath) : IForm(defaultBodyPath)
{
	m_dice0 = IForm("txtFiles/0.txt");	//0 = no number on the DiceForm
	m_dice1 = IForm("txtFiles/1.txt");
	m_dice2 = IForm("txtFiles/2.txt");
	m_dice3 = IForm("txtFiles/3.txt");
	m_dice4 = IForm("txtFiles/4.txt");
	m_dice5 = IForm("txtFiles/5.txt");
	m_dice6 = IForm("txtFiles/6.txt");

	m_FirstDicePos = IForm::TextPosition(2, 4, 49);
	m_SecondDicePos = IForm::TextPosition(2, 14, 49);
}


DiceForm::~DiceForm()
{
}

void DiceForm::updateForm(const std::pair<uint8_t, uint8_t> dice)
{
	updateDice(dice.first, m_FirstDicePos);
	updateDice(dice.second, m_SecondDicePos);
}

void DiceForm::updateDice(uint8_t diceNumber, const IForm::TextPosition& offset) {
	switch (diceNumber)
	{
	case 1: copyFormToForm(m_dice1, *this, offset);
		return;
	case 2: copyFormToForm(m_dice2, *this, offset);
		return;
	case 3: copyFormToForm(m_dice3, *this, offset);
		return;
	case 4: copyFormToForm(m_dice4, *this, offset);
		return;
	case 5: copyFormToForm(m_dice5, *this, offset);
		return;
	case 6: copyFormToForm(m_dice6, *this, offset);
		return;
	default:
		break;
	}
}

