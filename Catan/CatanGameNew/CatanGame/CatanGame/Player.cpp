#include "Player.h"
#include <cassert>


Player::Player(int id, std::string name, uint16_t color)
{
	m_id = id;
	m_name = name;
	m_color = color;
	m_maxLength = 0;
	m_largestArmy = false;
	m_longestRoad = false;
	m_canTurnDevCard = true;
}


Player & Player::operator=(Player && other)
{
	if (this != &other)
	{
		m_bricks = std::move(other.m_bricks);
		m_lumbers = std::move(other.m_lumbers);
		m_grains = std::move(other.m_grains);
		m_wools = std::move(other.m_wools);
		m_ores = std::move(other.m_ores);
		m_visibleDevCards = std::move(other.m_visibleDevCards);
		m_hiddenDevCards = std::move(other.m_hiddenDevCards);
		m_settlements = std::move(other.m_settlements);
		m_cities = std::move(other.m_cities);
		m_roads = std::move(other.m_roads);
		m_largestArmy = std::move(other.m_largestArmy);
		m_longestRoad = std::move(other.m_longestRoad);
		m_name = std::move(other.m_name);
		m_color = std::move(other.m_color);
		m_id = std::move(other.m_id);
		m_maxLength = std::move(other.m_maxLength);
		m_temporaryDevCards = std::move(other.m_temporaryDevCards);
		m_canTurnDevCard = std::move(other.m_canTurnDevCard);
	}
	return *this;
}

Player::Player(Player && other)
{
	m_bricks = std::move(other.m_bricks);
	m_lumbers = std::move(other.m_lumbers);
	m_grains = std::move(other.m_grains);
	m_wools = std::move(other.m_wools);
	m_ores = std::move(other.m_ores);
	m_visibleDevCards = std::move(other.m_visibleDevCards);
	m_hiddenDevCards = std::move(other.m_hiddenDevCards);
	m_settlements = std::move(other.m_settlements);
	m_cities = std::move(other.m_cities);
	m_roads = std::move(other.m_roads);
	m_largestArmy = std::move(other.m_largestArmy);
	m_longestRoad = std::move(other.m_longestRoad);
	m_name = std::move(other.m_name);
	m_color = std::move(other.m_color);
	m_id = std::move(other.m_id);
	m_maxLength = std::move(other.m_maxLength);
	m_temporaryDevCards = std::move(other.m_temporaryDevCards);
	m_canTurnDevCard = std::move(other.m_canTurnDevCard);
}

int Player::getId() const
{
	return m_id;
}


int Player::getLongestRoadLength() const
{
	return m_maxLength;
}

void Player::setLongestRoadLength(int max)
{
	m_maxLength = max;
}

void Player::setLongestRoad(bool longestRoad)
{
	m_longestRoad = longestRoad;
}

void Player::setLargestArmy(bool largestArmy)
{
	m_largestArmy = largestArmy;
}

void Player::resetTurnedDevCard()
{
	m_canTurnDevCard = true;
}

int Player::getNumberOfVictoryPoints() const
{
	int hiddenVP = std::count_if(m_hiddenDevCards.begin(), m_hiddenDevCards.end(), [](const devCard_ptr& card){return (card->getType()) == (DevCardType::victoryPoint); });
		return (hiddenVP);
}

int Player::getNumberOfKnits() const
{
	auto count = std::count_if(m_hiddenDevCards.begin(), m_hiddenDevCards.end(),
		[](const Player::devCard_ptr& devCard) 
	{
		return devCard->getType() == DevCardType::knite;
	});

	return count;
}

bool Player::hasLargestArmy() const
{
	return m_largestArmy;
}

bool Player::hasLongestRoad() const
{
	return m_longestRoad;
}

bool Player::canBuildSettlement() const
{
	return m_settlements.size() < 5 && 
		m_bricks.size() >= 1 && 
		m_lumbers.size() >= 1 && 
		m_wools.size() >= 1 && 
		m_grains.size() >= 1;
}

bool Player::canBuildCity() const
{
	return m_cities.size() < 4 && m_ores.size() >= 3 && m_grains.size() >= 2;
}

bool Player::canBuildRoad() const
{
	return m_roads.size() < 15 && m_lumbers.size() >= 1 && m_bricks.size() >= 1;
}

bool Player::canBuyDevCard() const
{
	return m_wools.size() >= 1 && m_grains.size() >= 1 && m_ores.size() >= 1;
}

bool Player::canTurnKnite() const
{
	return false;
}

bool Player::canTurnMonopoly() const
{
	return false;
}

bool Player::canTurnYearOfPlenty() const
{
	return false;
}

bool Player::canTurnRoadBuilding() const
{
	return false;
}

Player::~Player()
{
}


void Player::buildSettlement(node_ptr intersection)
{
	intersection->setOwnerId(m_id);
	assert(m_settlements.size() > 0);
	settlements_type::value_type& sett = m_settlements[m_settlements.size() - 1];
	assert(nullptr != sett);
	sett->setNode(intersection);
}

void Player::buildCity(node_ptr intersection)
{
	intersection->setOwnerId(m_id);
	assert(m_cities.size() > 0);
	cities_type::value_type& city = m_cities[m_cities.size() - 1];
	assert(nullptr != city);
	city->setNode(intersection);
}

void Player::buildRoad(edge_ptr edge)
{
	edge->setOwnerId(m_id);	
	assert(m_roads.size() > 0);
	roads_type::value_type& road = m_roads[m_roads.size() - 1];
	assert(nullptr != road);
	road->setEdge(edge);
}

void Player::turnDevCard(DevCardType devCardType)
{
	auto it = std::find_if (m_hiddenDevCards.begin(), m_hiddenDevCards.end(),
		[devCardType](const devCard_ptr& DevCard)
	{
		return DevCard->getType() == devCardType;
	}
	);
	if (it != m_hiddenDevCards.end())
	{
		int index = std::distance(m_hiddenDevCards.begin(), it);
		addVisibleDevCard(std::move(m_hiddenDevCards[index]));
		m_hiddenDevCards.erase(m_hiddenDevCards.begin() + index);
	}

	m_canTurnDevCard = false;	
}

bool Player::canTurnDevCard() const
{
	return m_canTurnDevCard;
}


std::string Player::getName() const
{
	return m_name;
}

int Player::getColor() const
{
	return m_color;
}

std::vector<Player::edge_ptr> Player::getEdgesOfRoads() const
{
	std::vector<edge_ptr> edges;
	for (auto const& road : m_roads)
	{
		edges.emplace_back((road)->getEdge());
	}
	return edges;
}

uint8_t Player::getNumberOfSpecificHiddenDevCard(const DevCardType type) const
{
	uint8_t counter = 0;
	for (auto& card : m_hiddenDevCards)
	{
		if ((*card).getType() == type)
			counter++;
	}
	return counter;
}

uint8_t Player::getNumberOfSpecificVisibleDevCard(const DevCardType type) const
{
	uint8_t counter = 0;
	for (auto& card : m_visibleDevCards)
	{
		if ((*card).getType() == type)
			counter++;
	}
	return counter;
}

void Player::addTmpDevCard(devCard_ptr type)
{
	m_temporaryDevCards.push_back(std::move(type));
}

Player::devCard_ptr Player::getTmpDevCard()
{
	if (!m_temporaryDevCards.empty())
	{
		devCard_ptr card = std::move(m_temporaryDevCards.back());
		m_temporaryDevCards.pop_back();
		return card;
	}
	else
	{
		return (Player::devCard_ptr{});
	}

}

bool Player::hasTmpDevCard()
{
	return (!m_temporaryDevCards.empty());
}

const std::vector<Player::devCard_ptr>& Player::getTmpDevCards() const
{
	return m_temporaryDevCards;
}

void Player::addVisibleDevCard(devCard_ptr type)
{
	m_visibleDevCards.push_back(std::move(type));
}

Player::devCard_ptr Player::getVisibleDevCard()
{
	if (!m_visibleDevCards.empty())
	{
		devCard_ptr card = std::move(m_visibleDevCards.back());
		m_visibleDevCards.pop_back();
		return card;
	}
	else
	{
		return (Player::devCard_ptr{});
	}
}
void Player::addHiddenDevCard(devCard_ptr type)
{
	m_hiddenDevCards.push_back(std::move(type));
}

Player::devCard_ptr Player::getHiddenDevCard()
{
	if (!m_hiddenDevCards.empty())
	{
		devCard_ptr card = std::move(m_hiddenDevCards.back());
		m_hiddenDevCards.pop_back();
		return card;
	}
	else
	{
		return (Player::devCard_ptr{});
	}
}

