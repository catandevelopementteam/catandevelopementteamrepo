#pragma once

//forward declaration of Factory
class Factory;

#include "IDevCard.h"

class RoadBuilding : public IDevCard
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	RoadBuilding();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~RoadBuilding();

	RoadBuilding(const RoadBuilding& other) = delete;
	RoadBuilding& operator=(const RoadBuilding& other) = delete;

	RoadBuilding(RoadBuilding&& other) = default;
	RoadBuilding& operator=(RoadBuilding&& other) = default;

};

