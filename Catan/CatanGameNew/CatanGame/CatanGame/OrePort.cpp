#include "OrePort.h"


OrePort::OrePort()
{
	m_type = IPort::PortType::orePort;
}


OrePort::~OrePort()
{
}

OrePort & OrePort::getInstance()
{
	static OrePort instance;
	return instance;
}

std::string OrePort::typeName() const
{
	return "OrePort";
}

IPort::PortType OrePort::getType() const
{
	return m_type;
}
