#include "Knite.h"

uint8_t Knite::s_instancesCreated = 0;

Knite::Knite()
{
	m_type = DevCardType::knite;
	m_instancesAllowed = Rules::getNumOfKniteCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

Knite::~Knite()
{
}

void Knite::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}