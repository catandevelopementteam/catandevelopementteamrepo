#pragma once

//forward declaration of Factory
class Factory;

#include "IResource.h"

class Grain : public IResource
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	Grain();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~Grain();

	Grain(const Grain& other) = delete;
	Grain& operator=(const Grain& other) = delete;

	Grain(Grain&& other) = default;
	Grain& operator=(Grain&& other) = default;

};

