#pragma once

#include <string>
#include <algorithm>

#include "Node.h"
#include "Edge.h"
#include "IResource.h"
#include "IDevCard.h"
#include "Road.h"
#include "Settlement.h"
#include "City.h"
#include "IResourceHolder.h"


class Player : public IResourceHolder
{
private:
	using lumber_ptr = IResourceHolder::lumber_ptr;
	using brick_ptr = IResourceHolder::brick_ptr;
	using wool_ptr = IResourceHolder::wool_ptr;
	using grain_ptr = IResourceHolder::grain_ptr;
	using ore_ptr = IResourceHolder::ore_ptr;
	using devCard_ptr = std::unique_ptr<IDevCard>;	
	using node_ptr = std::shared_ptr<Node>;
	using edge_ptr = std::shared_ptr<Edge>;
	using road_ptr = IResourceHolder::road_ptr;
	using settlement_ptr = IResourceHolder::settlement_ptr;
	using city_ptr = IResourceHolder::city_ptr;

	//player's id
	int m_id;

	//length of longest road
	int m_maxLength;

	//player's name
	std::string m_name;

	//player's color
	uint16_t m_color;
		
	//player's visible Development Cards
	std::vector<devCard_ptr> m_visibleDevCards;

	//player's hidden Development Cards
	std::vector<devCard_ptr> m_hiddenDevCards;

	//player's temporary Develoment Cards (temporary becomes hidden only at the end of turn)
	std::vector<devCard_ptr> m_temporaryDevCards;
		
	//player has the largest army?
	bool m_largestArmy;

	//player has the largest road?
	bool m_longestRoad;

	//player can turn development card?
	bool m_canTurnDevCard;
	
public:
	Player(int id, std::string name, uint16_t color);
	~Player();
	Player(const Player& other) = delete;
	Player& operator = (const Player& other) = delete;
	Player& operator = (Player&& other);
	Player(Player&& other);

	int getId() const;
	
	int getLongestRoadLength() const;
	void setLongestRoadLength(int max);
	void setLongestRoad(bool longestRoad);
	void setLargestArmy(bool largestArmy);
	void resetTurnedDevCard();

	
	int getNumberOfVictoryPoints() const;	

	int getNumberOfKnits() const;

	bool hasLargestArmy() const;		
	bool hasLongestRoad() const;

	bool canBuildSettlement() const;
	bool canBuildCity() const;
	bool canBuildRoad() const;
	bool canBuyDevCard() const;

	bool canTurnKnite() const;
	bool canTurnMonopoly() const ;
	bool canTurnYearOfPlenty() const;
	bool canTurnRoadBuilding() const;

	//player has a vector of the Settlement/City/Road objects, and each object has a pointer to the Node/Edge where is placed 	
	void buildSettlement(node_ptr node);
	void buildCity(node_ptr node);
	void buildRoad(edge_ptr edg);

	//hidden Dev Card becomes visible
	void turnDevCard(DevCardType devCardType);

	bool canTurnDevCard() const;
	
	std::string getName() const;
	int getColor() const;

	std::vector<edge_ptr> getEdgesOfRoads() const;

	uint8_t getNumberOfSpecificHiddenDevCard(const DevCardType type) const;
	uint8_t getNumberOfSpecificVisibleDevCard(const DevCardType type) const;

	//a Dev Card cannot be used in the same round that it was bought, so a temporary vector to store these dev cards it was used
	void addTmpDevCard(devCard_ptr type);
	devCard_ptr getTmpDevCard();
	bool hasTmpDevCard();
	const std::vector<devCard_ptr>& getTmpDevCards() const;

	void addVisibleDevCard(devCard_ptr type);
	devCard_ptr getVisibleDevCard();
	void addHiddenDevCard(devCard_ptr type);
	devCard_ptr getHiddenDevCard();
};

