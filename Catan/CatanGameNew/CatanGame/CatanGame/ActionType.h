#pragma once

enum class ActionType 
{
	build,
	trade,
	turnDevCard,
	buyDevCard,
	endTurn
};