#pragma once

//forward declaration of Factory
class Factory;

#include "IDevCard.h"

class Knite : public IDevCard
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	Knite();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~Knite();

	Knite(const Knite& other) = delete;
	Knite& operator=(const Knite& other) = delete;

	Knite(Knite&& other) = default;
	Knite& operator=(Knite&& other) = default;

};

