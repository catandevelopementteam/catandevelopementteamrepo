#pragma once
#include "IPort.h"


class BrickPort :public IPort
{
private:
	PortType m_type;
	BrickPort();
	~BrickPort();
public:
	static BrickPort& getInstance();
	std::string typeName() const;
	PortType getType() const;
	BrickPort(BrickPort const&) = delete;
	void operator=(BrickPort const&) = delete;


};