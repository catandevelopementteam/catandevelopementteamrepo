#include "DrawConsole.h"
#include <stdlib.h>
#include <Windows.h>



DrawConsole::DrawConsole(const Board& board, 
	const std::vector<Player>& players,
	const Player& currentPlayer)
{
	//m_mainWindowForm = MainWindowForm("txtFiles/mainWindowForm.txt");
	m_mainWindowForm = MainWindowForm(41, 143, board);
	m_mainWindowForm.updateBoardForm(players, board);
	m_mainWindowForm.updateCurrentPlayerForm(currentPlayer);
	m_mainWindowForm.updatePlayerForms(players);
	m_mainWindowForm.updateOptionsForm();
}

DrawConsole::~DrawConsole()
{
}

void DrawConsole::draw()
{
	//print two empty lines first to go the previous window higher in the console
	//std::cout << "\n\n";

	//clear screen
	system("CLS");

	auto& matrix = m_mainWindowForm.getFormBody();

	//const iterator to be able to iterate trough constatnt vector
	std::vector<Cell_info>::const_iterator it_elem;
	int elem = 1;

	for (it_elem = matrix.cbegin(); it_elem != matrix.cend(); it_elem++)
	{
		std::cout << *it_elem;

		if (elem % m_mainWindowForm.getColumns() == 0)
		{
			std::cout << std::endl;
		}

		elem++;
	}
}

void DrawConsole::displayMessage(const std::string message)
{
	std::vector<std::string> messages;
	messages.emplace_back(message);

	m_mainWindowForm.showMessages(messages);
}

void DrawConsole::highlightIntersections()
{
	m_mainWindowForm.showIntersectiunID();
}

void DrawConsole::unhighlightIntersections(const std::vector<Player>& players)
{
	m_mainWindowForm.showIntesectionOwner(players);
}

void DrawConsole::highlightEdges()
{
	m_mainWindowForm.showRoadsID();
}

void DrawConsole::unhighlightEdges(const std::vector<Player>& players)
{
	m_mainWindowForm.showRoadsOwner(players);
}

void DrawConsole::highlightHexagons()
{
	m_mainWindowForm.showHexagonsID();
}

void DrawConsole::unhighlightHexagons(const Board& board)
{
	m_mainWindowForm.hideHexagonID(board);
}

void DrawConsole::turnKniteOrNot()
{
	std::vector<std::string> messages;
	std::string s1 = "Would you like to turn a knite? y/n";
	messages.push_back(s1);

	m_mainWindowForm.showMessages(messages);
}

void DrawConsole::chooseTurnDevCard()
{
	std::vector<std::string> text;
	text.emplace_back("What developement card would you like to choose?");
	text.emplace_back("1 - Knite");
	text.emplace_back("2 - Monopoly");
	text.emplace_back("3 - Road building");
	text.emplace_back("4 - Year of plenty");
	text.emplace_back("0 - Cancel");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseTurnKnite()
{
	std::vector<std::string> text;
	text.emplace_back("Do you want to turn a knite?");
	text.emplace_back("Y - Yes");
	text.emplace_back("N - No");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseHex()
{
	std::vector<std::string> text;
	text.emplace_back("Select a hex where you want to place the robber.");
	text.emplace_back("Choose a number between 0 and 18 or 0 to cancel.");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::choosePlayer(const std::vector<Player>& players, const Player& currentPlayer)
{
	std::vector<std::string> text;
	text.emplace_back("Select one of the players.");
	uint8_t option = 1;
	for (const Player& player: players)
	{
		if (player.getName() != currentPlayer.getName())
		{
			std::string message = std::to_string(option) + " - " + player.getName();
			text.emplace_back(message);
			option++;
		}
	}
	text.emplace_back("0 - cancel");

	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseDiscardedCards(uint8_t totalNumber)
{
	std::vector<std::string> text;
	text.emplace_back("Select " + std::to_string(totalNumber) + " resources to discard:");
	text.emplace_back("1 - lumber");
	text.emplace_back("2 - brick");
	text.emplace_back("3 - wool");
	text.emplace_back("4 - grain");
	text.emplace_back("5 - ore");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseAction()
{
	std::vector<std::string> text;
	text.emplace_back("Choose what do you want to do:");
	text.emplace_back("1 - Build");
	text.emplace_back("2 - Trade");
	text.emplace_back("3 - Turn developement card");
	text.emplace_back("4 - Buy developement card (wool, grain, ore)");
	text.emplace_back("0 - End turn");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseBuild()
{
	std::vector<std::string> text;
	text.emplace_back("What do you want to build?");
	text.emplace_back("1 - Settlement (lumber, brick, wool, grain)");
	text.emplace_back("2 - City (grain, grain, grain, ore, ore)");
	text.emplace_back("3 - Road (lumber, brick)");
	text.emplace_back("0 - Cancel");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseIntersection()
{
	std::vector<std::string> text;
	text.emplace_back("Select an intersection where you want to build.");
	text.emplace_back("Choose a number between 0 and 53:");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseEdge()
{
	std::vector<std::string> text;
	text.emplace_back("Select an edge where you want to build.");
	text.emplace_back("Choose a number between 0 and 71:");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseTrade()
{
	std::vector<std::string> text;
	text.emplace_back("Choose a type of trade:");
	text.emplace_back("1 - Trade 4:1");
	text.emplace_back("2 - Trade at port");
	text.emplace_back("3 - Trade with others");
	text.emplace_back("0 - Cancel");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::choosePortType()
{
	std::vector<std::string> text;
	text.emplace_back("Choose a type of port:");
	text.emplace_back("1 - Trade 3:1");
	text.emplace_back("2 - Trade 2:1 lumber");
	text.emplace_back("3 - Trade 2:1 brick");
	text.emplace_back("4 - Trade 2:1 wool");
	text.emplace_back("5 - Trade 2:1 grain");
	text.emplace_back("6 - Trade 2:1 ore");
	text.emplace_back("0 - Cancel");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseResourcesToReceive()
{
	std::vector<std::string> text;
	text.emplace_back("Select resources to receive:");
	text.emplace_back("1 - lumber");
	text.emplace_back("2 - brick");
	text.emplace_back("3 - wool");
	text.emplace_back("4 - grain");
	text.emplace_back("5 - ore");
	text.emplace_back("0 - finish");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseResourcesToTrade()
{
	std::vector<std::string> text;
	text.emplace_back("Select resources to trade:");
	text.emplace_back("1 - lumber");
	text.emplace_back("2 - brick");
	text.emplace_back("3 - wool");
	text.emplace_back("4 - grain");
	text.emplace_back("5 - ore");
	text.emplace_back("0 - finish");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::chooseResourceToMonopolize()
{
	std::vector<std::string> text;
	text.emplace_back("Select resources to monopolize:");
	text.emplace_back("1 - lumber");
	text.emplace_back("2 - brick");
	text.emplace_back("3 - wool");
	text.emplace_back("4 - grain");
	text.emplace_back("5 - ore");
	m_mainWindowForm.showMessages(text);
}

void DrawConsole::updateCurrentPlayer(const Player & player)
{
	m_mainWindowForm.updateCurrentPlayerForm(player);
}

void DrawConsole::updatePlayersForm(const std::vector<Player>& players)
{
	m_mainWindowForm.updatePlayerForms(players);
}

void DrawConsole::updateBoardForm(const std::vector<Player>& players, const Board& board)
{
	m_mainWindowForm.updateBoardForm(players, board);
}

void DrawConsole::updateDiceForm(const std::pair<uint8_t, uint8_t> dice)
{
	m_mainWindowForm.updateDiceForm(dice);
}

void DrawConsole::updateBoardAndPlayers(const std::vector<Player>& players, const Player & player, const Board& board)
{
	m_mainWindowForm.updateBoardForm(players, board);
	m_mainWindowForm.updateCurrentPlayerForm(player);
	m_mainWindowForm.updatePlayerForms(players);
}

std::ostream & operator<<(std::ostream & out, const Cell_info& cell)
{
	//set cell background and foreground color
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), cell.getColor());

	//pint symbol
	out << cell.getSymbol();

	//get back to the default background and foreground colors
	//SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);

	return out;
}
