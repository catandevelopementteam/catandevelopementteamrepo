#pragma once

#include "Cell_info.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <memory>

// 10 - wool_type - verde deschis
//  8 - ore_type - gri inchis
// 12 - brick_type - caramiziu
// 14 - grain_type - galbui
//  6 - wood - portocaliu
//  3 - Player1 - Albastru deschis
//  4 - Player2 - Rosu
// 13 - Player3 - Mov
//  7 - player4 - gri deschis

class IForm
{
public:
	IForm();
	IForm(std::string defaultBodyPath);
	IForm(int rows, int columns);
	virtual ~IForm();

	uint16_t getRows() const noexcept;
	uint16_t getColumns() const noexcept;
	const std::vector<Cell_info>& getFormBody();

public:
	class TextPosition
	{
	public:
		TextPosition();
		TextPosition(int row, int column, int capacity);
		~TextPosition();

		uint16_t getRows() const noexcept;		
		uint16_t getColumns() const noexcept;
		uint16_t getCapacity() const noexcept;

	private:
		uint16_t m_posRows;		//one based
		uint16_t m_posColumns;	//one based
		int m_capacity;
	};

protected:
	std::vector<Cell_info> m_formBody;
	uint16_t m_formRows;				//one based
	uint16_t m_formColumns;				//one based

protected:
	void changeText(IForm::TextPosition pos, std::string text);
	void changeCharacter(const int rowIndex, const int colIndex, char character);
	void changeColor(IForm::TextPosition pos, int color);
	void copyFormToForm(const IForm& source, IForm& destination, IForm::TextPosition offset);
	void getMultiPositions(std::vector<std::vector<IForm::TextPosition> >& positions, std::string path, std::string positionIdentifiers);
	void getSinglePositions(std::vector<IForm::TextPosition>& positions, std::string path, char positionIdentifier);

private:
	void initFormBody(std::string path, std::vector<Cell_info>& formText);
	void deleteText(IForm::TextPosition pos);
	void setText(IForm::TextPosition pos, std::string text);

	void createHexPositions(std::vector<IForm::TextPosition>& hexPositions, uint8_t row, uint8_t col);
	void createForwardRowPositions(std::vector<IForm::TextPosition>& forwardRowPositions, uint8_t row, uint8_t col);
	void createBackwardRowPositions(std::vector<IForm::TextPosition>& backwardRowPositions, uint8_t row, uint8_t col);
};

