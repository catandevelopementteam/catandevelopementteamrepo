#pragma once
#include "PortType.h"
#include "IBuilding.h"
#include <iostream>

/*
					Class Node
	represents the Intersections from the Catan Game Board
*/

class Node
{
private:
	int m_id; //node id (from 0 to 53) - fixed
	PortType m_portType; //each node is a port type	
	int m_ownerId; // if owned, m_ownerId is equal with the owner's id (Player's id)

public:
	Node();
	Node(int id);
	Node& operator =(const Node& other);
	Node(const Node& other);
	Node(Node&& other);
	Node& operator =(Node&& other);
	~Node();

	int getId() const;
	void setPortType(int type);
	PortType getType() const;

	void setOwnerId(int ownerId);
	int getOwnerId() const;
		

	//optional methods used for initial tests
	std::string typePortName() const;
	void printIntersection() const;

	
};


