#pragma once

#include "Node.h"

/*
					Class Edge
	represents the edges (hexes sides) from the Catan Board
*/


class Edge
{	
	using node_ptr = std::shared_ptr<Node>;

private:
	node_ptr m_id1, m_id2; //Edge is defined by two nodes 
	int m_id; //redundant, but neccessary for displaying edges on console	
	int m_ownerId; // if owned, m_ownerId is equal with the player's id that owns it

public:
	Edge();
	Edge(node_ptr id1, node_ptr id2);
	Edge& operator =(const Edge& other);
	Edge(const Edge& other);
	~Edge();
	
	//method that returns the Nodes that define the Edge, as a pair
	std::pair<node_ptr, node_ptr> getPairOfNodes() const;

	//get ids of the two Nodes
	int getId1() const;
	int getId2() const;

	//get&set edge id
	void setId(int id);
	int getId() const;
	
	//when a road is built, the edge has an owner Id
	void setOwnerId(int ownerId);
	int getOwnerId() const;
	
};
