#include "Lumber.h"

uint8_t Lumber::s_instancesCreated = 0;

Lumber::Lumber()
{
	m_type = ResourceType::lumber;
	m_color = Rules::getConsoleColorOfLumber();
	m_instancesAllowed = Rules::getNumOfLumberCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

void Lumber::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}

Lumber::~Lumber()
{
}
