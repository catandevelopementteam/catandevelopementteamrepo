#pragma once

#include "ResourceType.h"
#include "BuildingType.h"
#include "PortType.h"
#include "DevCardType.h"

#include <iostream>
#include <vector>
#include <map>
#include <string>

class Rules
{
private:
	static uint8_t s_numOfSettlements;
	static uint8_t s_numOfCities;
	static uint8_t s_numOfRoads;
				   
	static uint8_t s_numOfHex;
	static uint8_t s_numOfIntersections;
				   
	static uint8_t s_numOfDesertHex;
	static uint8_t s_numOfLumberHex;
	static uint8_t s_numOfBrickHex;
	static uint8_t s_numOfWoolHex;
	static uint8_t s_numOfGrainsHex;
	static uint8_t s_numOfOreHex;
				   
	static uint8_t s_numOfLumberCards;
	static uint8_t s_numOfBrickCards;
	static uint8_t s_numOfWoolCards;
	static uint8_t s_numOfGrainCards;
	static uint8_t s_numOfOreCards;
				   
	static uint8_t s_numOfKniteCards;
	static uint8_t s_numOfMonopolyCards;
	static uint8_t s_numOfYearOfPlentyCards;
	static uint8_t s_numOfVictoryPointCards;
	static uint8_t s_numOfRoadBuildingCards;
				   
	static uint8_t s_numOfGenericPorts;
	static uint8_t s_numOfLumberPorts;
	static uint8_t s_numOfBrickPorts;
	static uint8_t s_numOfWhoolPorts;
	static uint8_t s_numOfGrainPorts;
	static uint8_t s_numOfOrePorts;
				   
	static uint8_t s_consoleColorPlayer1;
	static uint8_t s_consoleColorPlayer2;
	static uint8_t s_consoleColorPlayer3;
	static uint8_t s_consoleColorPlayer4;
			   
	static uint8_t s_consoleColorLumber;
	static uint8_t s_consoleColorBrick;
	static uint8_t s_consoleColorWool;
	static uint8_t s_consoleColorGrain;
	static uint8_t s_consoleColorOre;
	static uint8_t s_consoleColorDesert;

	static std::vector<ResourceType> s_settlementCost;
	static std::vector<ResourceType> s_cityCost;
	static std::vector<ResourceType> s_roadCost;
	static std::vector<ResourceType> s_devCardCost;

	static std::map<ResourceType, std::string> s_resourceToString;
	static std::map<BuildingType, std::string> s_buildingToString;
	static std::map<PortType, std::string> s_portTypeToString;
	static std::map<DevCardType, std::string> s_devCardToString;

public:
	Rules();
	~Rules();

public:
	static uint8_t getNumOfSettlements() { return s_numOfSettlements; }
	static uint8_t getNumOfCities() { return s_numOfCities; }
	static uint8_t getNumOfRoads() { return s_numOfRoads; }
		
	static uint8_t getNumOfHex() { return s_numOfHex; }
	static uint8_t getNumOfIntersections() { return s_numOfIntersections; }
		
	static uint8_t getNumOfDesertHex() { return s_numOfDesertHex; }
	static uint8_t getNumOfLumberHex() { return s_numOfLumberHex; }
	static uint8_t getNumOfBrickHex() { return s_numOfBrickHex; }
	static uint8_t getNumOfWoolHex() { return s_numOfWoolHex; }
	static uint8_t getNumOfGrainsHex() { return s_numOfGrainsHex; }
	static uint8_t getNumOfOreHex() { return s_numOfOreHex; }
		   
	static uint8_t getNumOfLumberCards() { return s_numOfLumberCards; }
	static uint8_t getNumOfBrickCards() { return s_numOfBrickCards; }
	static uint8_t getNumOfWoolCards() { return s_numOfWoolCards; }
	static uint8_t getNumOfGrainCards() { return s_numOfGrainCards; }
	static uint8_t getNumOfOreCards() { return s_numOfOreCards; }
		   
	static uint8_t getNumOfKniteCards() { return s_numOfKniteCards; }
	static uint8_t getNumOfMonopolyCards() { return s_numOfMonopolyCards; }
	static uint8_t getNumOfYearOfPlentyCards() { return s_numOfYearOfPlentyCards; }
	static uint8_t getNumOfVictoryPointCards() { return s_numOfVictoryPointCards; }
	static uint8_t getNumOfRoadBuildingCards() { return s_numOfRoadBuildingCards; }
		   
	static uint8_t getNumOfGenericPorts() { return s_numOfGenericPorts; }
	static uint8_t getNumOfLumberPorts() { return s_numOfLumberPorts; }
	static uint8_t getNumOfBrickPorts() { return s_numOfBrickPorts; }
	static uint8_t getNumOfWhoolPorts() { return s_numOfWhoolPorts; }
	static uint8_t getNumOfGrainPorts() { return s_numOfGrainPorts; }
	static uint8_t getNumOfOrePorts() { return s_numOfOrePorts; }

	static uint8_t getConsoleColorOfPlayer1() { return s_consoleColorPlayer1; }
	static uint8_t getConsoleColorOfPlayer2() { return s_consoleColorPlayer2; }
	static uint8_t getConsoleColorOfPlayer3() { return s_consoleColorPlayer3; }
	static uint8_t getConsoleColorOfPlayer4() { return s_consoleColorPlayer4; }
					  
	static uint8_t getConsoleColorOfLumber() { return s_consoleColorLumber; }
	static uint8_t getConsoleColorOfBrick() { return s_consoleColorBrick; }
	static uint8_t getConsoleColorOfWool() { return s_consoleColorWool; }
	static uint8_t getConsoleColorOfGrain() { return s_consoleColorGrain; }
	static uint8_t getConsoleColorOfOre() { return s_consoleColorOre; }
	static uint8_t getConsoleColorOfDesert() { return s_consoleColorDesert; }

	static std::vector<ResourceType>& getsettlementCost() { return s_settlementCost; };
	static std::vector<ResourceType>& getCityCost() { return s_cityCost; };
	static std::vector<ResourceType>& getRoadCost() { return s_roadCost; };
	static std::vector<ResourceType>& getDevCardCost() { return s_devCardCost; };

	static std::string getMapedValue(const ResourceType type) { return s_resourceToString[type]; }
	static std::string getMapedValue(const BuildingType type) { return s_buildingToString[type]; }
	static std::string getMapedValue(const PortType type) { return s_portTypeToString[type]; }
	static std::string getMapedValue(const DevCardType type) { return s_devCardToString[type]; }
};


