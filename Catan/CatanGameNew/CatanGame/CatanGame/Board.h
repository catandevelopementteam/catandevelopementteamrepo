#pragma once

#include "Hex.h"
#include "IPort.h"
#include "Node.h"
#include "Edge.h"
#include "Graph.h"
#include "Cell_info.h"
#include <fstream>
#include <string>


/*
								Class Board
	contains all the game's board elements (Nodes & Edges-linked in a graph, Hexes)
	             and all the methods related to the board 
*/

class Board
{
public:
	using node_ptr = std::shared_ptr<Node>;
	using edge_ptr = std::shared_ptr<Edge>;

private:
	Graph m_graph;
	std::vector<Hex> m_hexes;
	std::string m_path;

public:
	//returns a constant vector of hexagons to be used, for ex, in printing the numbers on the hexagons
	const std::vector<Hex>& getVectorOfHex() const;

	//methods to find a Node/Edge using it's id
	node_ptr findNode(int id) const;
	edge_ptr findEdge(int Node1Id, int Node2Id) const;
	edge_ptr findEdge(int EdgeId) const;

	//method that returns the neighbours of a node
	std::vector<node_ptr> getNeighbours(int id) const;

	//method that returns the incident edges of a node
	std::vector<edge_ptr> getIncidentEdges(node_ptr node) const;
	
	//methods to place&remove robber 
	void placeRobber(uint8_t hexID);
	void removeRobber(uint8_t hexID);

	//method to obtain the longest road value
	int longestRoad(int currentLength, edge_ptr edge, int PlayerId);

	//method that finds out of a player has acces to a certain hex
	bool hasAccesToHex(uint8_t playerID, uint8_t hexID);

	//necessary methods to obtain a vector with the lengths of the player's roads 	
private:
	std::string fullFilePath(std::string const& fileName) const;
	void lengthOfRoads(int currentLength, edge_ptr edge, int PlayerId,
		std::vector<edge_ptr>& visited_edges, std::vector<node_ptr>& visited_nodes, std::vector<int>& length_of_roads);
	bool visitedEdge(edge_ptr edge, std::vector<edge_ptr> vector);
	bool visitedNode(node_ptr node, std::vector<node_ptr> vector);

	//constructor, destructor and initialization methods
public:
	Board(std::string path);
	~Board();
	//static Board& getInstance();

	void init_graph();
	void init_hexes();
	void init_hexes_resources();
	void init_colors_of_hexes();
	void init_hexes_assigned_no();
	void init_robber();
	
	
};


