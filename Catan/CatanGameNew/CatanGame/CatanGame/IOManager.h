#pragma once

#include "IInput.h"
#include "IDraw.h"
#include "InputConsole.h"
#include "DrawConsole.h"
#include "IBuilding.h"
#include "IDevCard.h"
#include "IResource.h"
#include "IPort.h"
#include "ActionType.h"
#include "..\Log\Logging.h"

class IOManager
{
public:
	enum class VisualisatioType
	{
		console,
		window
	};

private:
	std::shared_ptr<IInput> m_input;
	std::shared_ptr<IDraw> m_output;
	std::shared_ptr<Logging> m_logging;

public:
	IOManager();
	IOManager(IOManager::VisualisatioType visualisationType, 
		const Board& board, 
		const std::vector<Player>& players, 
		const Player& currentPlayer);
	~IOManager();

	DevCardType chooseTurnDevCard();

	bool chooseTurnKnite();
	
	uint8_t chooseHex(const Board& board);
	
	const Player& choosePlayer(const std::vector<Player>& players, const Player& currentPlayer);
	
	std::vector < ResourceType > chooseDiscardedCards(const Player& currentPlayer,
		const Player& temporaryCurrentPlayer,
		uint8_t totalNumber);
	
	//action = 1-Build, 2-Trade, 3-Turn DevCard, 4-BuyDevCard, 0-EndTurn
	ActionType chooseAction();
	
	BuildingType chooseBuild();

	uint8_t chooseIntersection(const std::vector<Player>& players);

	uint8_t chooseEdge(const std::vector<Player>& players);

	TradeType chooseTrade();

	PortType choosePortType();

	//returns a vector of pairs. Each pair represents one resource type and its quantity
	std::vector < ResourceType > chooseResourcesToReceive();

	//returns a vector of pairs. Each pair represents one resource type and its quantity
	std::vector < ResourceType > chooseResourcesToTrade();

	ResourceType chooseResourceToMonopolize();

	void displayMessage(const std::string message);

	void updatePlayersForm(const std::vector<Player>& players);
	void updateCurrentPlayerForm(const Player& player);
	void updateBoardForm(const std::vector<Player>& players, const Board& board);
	void updateDiceForm(const std::pair<uint8_t, uint8_t> dice);
	void updateBoardAndPlayers(const std::vector<Player>& players, const Player & currentPlayer, const Board& board);
};

