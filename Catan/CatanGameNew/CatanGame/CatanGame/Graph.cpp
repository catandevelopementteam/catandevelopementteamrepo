#include "Graph.h"
#include <cassert>


void Graph::addEdge(node_ptr u, node_ptr v, edge_ptr wt)
{
	m_adj[u->getId()].emplace_back(std::make_pair(v, wt));	
}

Graph::Graph()
{
	m_noOfNodes = Rules::getNumOfIntersections();
	m_adj.resize(m_noOfNodes);
	for (int i = 0; i < m_noOfNodes; i++)
	{
		m_adj[i].reserve(3);
	}
}

/*
void Graph::printGraph() const
{
	for (int u = 0; u < m_noOfNodes; u++)
	{
		std::cout << "Node " << u << " makes an edge with \n";
		for (auto it = m_adj[u].begin(); it != m_adj[u].end(); it++)
		{
			auto v = it->first;
			auto w = it->second;
			std::cout << "\tNode " << v->getId() << " witch is " << v->typePortName()
				<< "and it has a settlement " << v->hasSettlement() << " or city " << v->hasCity()
				<< " ,with edge ids = " << w->getId1() << " " << w->getId2() << "that has road " << w->hasRoad() << "\n";
		}
		std::cout << "\n";
	}
}
*/


Graph::node_ptr Graph::findNode(int id) const
{
	node_ptr i;
	auto it = std::find_if(
		m_nodes.begin(),
		m_nodes.end(),
		[id](const node_ptr& intersec)
	{
		return intersec->getId() == id;
	}
	);
	if (it != m_nodes.end())
	{
		i = (*it);
	}

	return i;
}

Graph::edge_ptr Graph::findEdge(int id1, int id2) const
{
	edge_ptr e;
	auto it = std::find_if(
		m_edges.begin(),
		m_edges.end(),
		[id1, id2](const edge_ptr& edg)
	{
		return (
			((edg->getId1() == id1) && (edg->getId2() == id2)) ||
			((edg->getId2() == id1) && (edg->getId1() == id2))
			);
	}
	);
	if (it != m_edges.end())
	{
		e = (*it);
	}

	return e;
}

Graph::edge_ptr Graph::findEdge(int id) const
{
	edge_ptr e;
	auto it = std::find_if(
		m_edges.begin(),
		m_edges.end(),
		[id](const edge_ptr& edg)
	{
		return edg->getId() == id;
	}
	);
	if (it != m_edges.end())
	{
		e = (*it);
	}

	return e;
}

std::vector<Graph::node_ptr> Graph::getNeighbours(node_ptr node) const
{
	std::vector<node_ptr> nodes;
	int id = node->getId();
	for (auto i = m_adj[id].begin(); i != m_adj[id].end(); ++i)
	{
		auto n = i->first;
		nodes.emplace_back(n);
	}
	return nodes;
}


void Graph::initGraph(std::string path)
{
	//init m_nodes
	int numOfInt;
	std::ifstream fin(path);
	assert(fin.is_open());

	// read the number of node_ptr and create the objects
	fin >> numOfInt;
	for (int i = 0; i < numOfInt; i++)
	{
		m_nodes.push_back(std::make_shared<Node>(i));
	}

	// read node_ptr inforamtion and create edge_ptr
	while (numOfInt--)
	{
		int int1;
		int port;
		int neighbours;

		fin >> int1 >> port >> neighbours;

		node_ptr i1 = findNode(int1);		
		i1->setPortType(port);

		// iterate through all neighbours
		for (int i = 0; i < neighbours; i++)
		{
			int int2;

			// read neighbour information and create edge (if necessary)
			fin >> int2;
			node_ptr i2 = findNode(int2);			

			edge_ptr edg1 = findEdge(int1, int2);
			if (nullptr == edg1)
			{
				// no corresponding edge exists, create and add it to the edge list
				edg1 = std::make_shared<Edge>(i1, i2);
				m_edges.push_back(edg1);
			}

			addEdge(i1, i2, edg1);
		}
	}
	
}

bool sortById(std::shared_ptr<Edge> e1, std::shared_ptr<Edge> e2)
{
	return (e1->getId() < e2->getId());
}
void Graph::setAllEdgesIDs(std::string fileName)
{
	std::ifstream fin(fileName);
	int noOfEdges;
	fin >> noOfEdges;
	while (noOfEdges--)
	{
		int n1, n2;
		fin >> n1 >> n2;
		auto edge = findEdge(n1, n2);
		int id;
		fin >> id;
		edge->setId(id);
	}

	std::sort(m_edges.begin(), m_edges.end(), sortById);
}


std::vector<Graph::edge_ptr> Graph::getIncidentEdges(node_ptr node) const
{
	std::vector<edge_ptr> edges;
	int id = node->getId();
	for (auto i = m_adj[id].begin(); i != m_adj[id].end(); ++i)
	{
		auto e = i->second;
		edges.emplace_back(e);
	}
	return edges;
}

Graph::~Graph()
{
}
