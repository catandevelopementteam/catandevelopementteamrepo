#include "CurrentPlayerForm.h"



CurrentPlayerForm::CurrentPlayerForm()
{
}

CurrentPlayerForm::CurrentPlayerForm(std::string defaultBodyPath) : IForm(defaultBodyPath)
{
	std::vector<IForm::TextPosition> positions;
	getSinglePositions(positions, defaultBodyPath, '#');

	m_name = positions[0];
	m_resources = positions[1];
	m_lumbers = positions[2];
	m_bricks = positions[3];
	m_wools = positions[4];
	m_grains = positions[5];
	m_ores = positions[6];
	m_hiddenDevCards = positions[7];
	m_knites = positions[8];
	m_monopolys = positions[9];
	m_yearOfPlenty = positions[10];
	m_roadBuilding = positions[11];
	m_victoryPoints = positions[12];

}

CurrentPlayerForm::~CurrentPlayerForm()
{
}

#include "Player.h"
void CurrentPlayerForm::updateForm(const Player & player)
{
	setBackgrounColor(player);
	setName(player.getName());
	
	setLumbers(std::to_string(player.getLumbers().size()));
	setBricks(std::to_string(player.getBricks().size()));
	setWools(std::to_string(player.getWools().size()));
	setGrains(std::to_string(player.getGrains().size()));
	setOres(std::to_string(player.getOres().size()));

	setResources(std::to_string(
		player.getLumbers().size() +
		player.getBricks().size() +
		player.getWools().size() +
		player.getGrains().size() +
		player.getOres().size()));

	setKnites(std::to_string(player.getNumberOfSpecificHiddenDevCard(DevCardType::knite)));
	setMonopolys(std::to_string(player.getNumberOfSpecificHiddenDevCard(DevCardType::monopoly)));
	setYearOfPlenty(std::to_string(player.getNumberOfSpecificHiddenDevCard(DevCardType::yearOfPlenty)));
	setRoadBuilding(std::to_string(player.getNumberOfSpecificHiddenDevCard(DevCardType::roadBuilding)));
	setVictoryPoints(std::to_string(player.getNumberOfSpecificHiddenDevCard(DevCardType::victoryPoint)));
	
	setHiddenDevCards(std::to_string(
		player.getNumberOfSpecificHiddenDevCard(DevCardType::knite) +
		player.getNumberOfSpecificHiddenDevCard(DevCardType::monopoly) +
		player.getNumberOfSpecificHiddenDevCard(DevCardType::roadBuilding) +
		player.getNumberOfSpecificHiddenDevCard(DevCardType::victoryPoint) +
		player.getNumberOfSpecificHiddenDevCard(DevCardType::yearOfPlenty)));
}

void CurrentPlayerForm::setName(std::string text)
{
	changeText(m_name, text);
}

void CurrentPlayerForm::setResources(std::string text)
{
	changeText(m_resources, text);
}

void CurrentPlayerForm::setLumbers(std::string text)
{
	changeText(m_lumbers, text);
}

void CurrentPlayerForm::setBricks(std::string text)
{
	changeText(m_bricks, text);
}

void CurrentPlayerForm::setWools(std::string text)
{
	changeText(m_wools, text);
}

void CurrentPlayerForm::setGrains(std::string text)
{
	changeText(m_grains, text);
}

void CurrentPlayerForm::setOres(std::string text)
{
	changeText(m_ores, text);
}

void CurrentPlayerForm::setHiddenDevCards(std::string text)
{
	changeText(m_hiddenDevCards, text);
}

void CurrentPlayerForm::setKnites(std::string text)
{
	changeText(m_knites, text);
}

void CurrentPlayerForm::setMonopolys(std::string text)
{
	changeText(m_monopolys, text);
}

void CurrentPlayerForm::setYearOfPlenty(std::string text)
{
	changeText(m_yearOfPlenty, text);
}

void CurrentPlayerForm::setRoadBuilding(std::string text)
{
	changeText(m_roadBuilding, text);
}

void CurrentPlayerForm::setVictoryPoints(std::string text)
{
	changeText(m_victoryPoints, text);
}

void CurrentPlayerForm::setBackgrounColor(const Player& player)
{
	for (int cellIndex = m_formColumns; cellIndex < m_formRows * m_formColumns; cellIndex++)
	{
		//modify everything that it's not on the first and last column
		if (cellIndex % m_formColumns != 0 && cellIndex % m_formColumns != m_formColumns - 1)
		{
			m_formBody[cellIndex].setColor(player.getColor());
		}
	}
}
