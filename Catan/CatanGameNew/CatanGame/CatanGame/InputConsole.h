#pragma once

#include <conio.h>
#include <string>
#include <windows.h>
#include <iterator>

#include "IInput.h"
#include "Player.h"

class InputConsole : public IInput
{
public:
	InputConsole();
	~InputConsole();


	// Inherited via IInput
	virtual DevCardType chooseTurnDevCard() override;

	virtual bool chooseTurnKnite() override;

	virtual uint8_t chooseHex() override;

	virtual const Player& choosePlayer(const std::vector<Player>& players, const Player& currentPlayer) override;

	virtual std::vector<ResourceType> chooseDiscardedCards(uint8_t totalNumber) override;

	virtual ActionType chooseAction() override;

	virtual BuildingType chooseBuild() override;

	virtual uint8_t chooseIntersection() override;

	virtual uint8_t chooseEdge() override;

	virtual TradeType chooseTrade() override;

	virtual PortType choosePortType() override;

	virtual std::vector<ResourceType> chooseResourcesToReceive() override;

	virtual std::vector<ResourceType> chooseResourcesToTrade() override;

	virtual ResourceType chooseResourceToMonopolize() override;

private:
	char getKey(std::vector<char> options) const;
	void chooseResources(std::vector<ResourceType>& resources);
	uint8_t getCurrentPlayerIndex(const std::vector<Player>& players, const Player& currentPlayer);
};

