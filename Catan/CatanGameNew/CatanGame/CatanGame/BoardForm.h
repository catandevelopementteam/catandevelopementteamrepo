#pragma once

#include "IForm.h"
#include "Player.h"
#include "Board.h"

class BoardForm : public IForm
{
public:
	BoardForm();
	BoardForm(const std::string defaultBodyPath, const Board& board);
	~BoardForm();

	void updateForm(const std::vector<Player>& players, const Board& board);

	void showIntersectiunID();
	void showIntesectionOwner(const std::vector<Player>& players);
	void showRoadsID();
	void showRoadsOwner(const std::vector<Player>& players);
	void showHexagonsID();
	void hideHexagonID(const Board& board);

private:
	std::vector<IForm::TextPosition> m_intersectionsPosition;
	std::vector< std::vector<IForm::TextPosition> > m_roadsPosition;
	std::vector<IForm::TextPosition> m_hexagonsIDPosition;
	std::vector< std::vector<IForm::TextPosition> > m_hexagonsBodyPosition;
	std::vector<IForm::TextPosition> m_legentPositions;
	uint8_t m_robberPosition;

private:
	void initIntersectionsPosition(const std::string defaultBodyPath);
	void initRoadsPosition(const std::string defaultBodyPath);
	void initHexagonsIDPosition(const std::string defaultBodyPath);
	void initHexagonsBodyPosition(const std::string defaultBodyPath);
	void initLegendPosition(const std::string defaultBodyPath);

	//removes all the special characters used in finding all the positions
	void deleteSpecialCharacters();

	void setHexagonsDice(const Board& board);
	void setHexagonsColor(const Board& board);
	void setHexagonBodyText(const Board& board);
	void setLegendTextAndColors();

	void resetIntersectionText();
	void resetEdgeText();
	void setIntersectionsTextAndColor(const Player& player);
	void setRoadsTextAndColor(const Player& player);
};

