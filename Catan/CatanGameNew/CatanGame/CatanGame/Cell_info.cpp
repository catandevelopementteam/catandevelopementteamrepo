#include "Cell_info.h"

Cell_info::Cell_info(char symbol, uint16_t color)
{
	m_symbol = symbol;
	m_color = color;
}


Cell_info::~Cell_info()
{
}

void Cell_info::setSymbol(char symbol)
{
	m_symbol = symbol;
}

void Cell_info::setColor(uint16_t color)
{
	m_color = color;
}

char Cell_info::getSymbol() const
{
	return m_symbol;
}

uint16_t Cell_info::getColor() const
{
	return m_color;
}
