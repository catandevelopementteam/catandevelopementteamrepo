#pragma once

//forward declaration of Factory
class Factory;

#include "IDevCard.h"

class VictoryPoint : public IDevCard
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	VictoryPoint();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~VictoryPoint();

	VictoryPoint(const VictoryPoint& other) = delete;
	VictoryPoint& operator=(const VictoryPoint& other) = delete;

	VictoryPoint(VictoryPoint&& other) = default;
	VictoryPoint& operator=(VictoryPoint&& other) = default;

};

