#pragma once
#include "IPort.h"


class GrainPort :public IPort
{
private:
	PortType m_type;
	GrainPort();
	~GrainPort();
public:
	static GrainPort& getInstance();
	std::string typeName() const;
	PortType getType() const;
	GrainPort(GrainPort const&) = delete;
	void operator=(GrainPort const&) = delete;


};
