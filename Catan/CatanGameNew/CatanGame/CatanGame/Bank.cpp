#include "Bank.h"
#include <chrono>
#include <random>

Bank::Bank()
{
	initResourceCardStacks();
	initDevCardStack();
	initRoads();
	initSettlements();
	initCities();

	//initBuildings();
}

Bank::~Bank()
{
}

//Bank& Bank::getInstance() {
//	static Bank bank;
//	return bank;
//}

bool Bank::isEmptyDevCardStack() const {
	return m_devCards.empty();
}

bool Bank::areEnoughResources(ResourceType type, unsigned int number) const {
	switch (type)
	{
	case ResourceType::lumber: return m_lumbers.size() >= number;
	case ResourceType::brick: return m_bricks.size() >= number;
	case ResourceType::wool: return m_wools.size() >= number;
	case ResourceType::grain: return m_grains.size() >= number;
	case ResourceType::ore: return m_ores.size() >= number;
	default: return false;
	}
}


void Bank::addDevCard(devCard_ptr type)
{
	m_devCards.push_back(std::move(type));
}

Bank::devCard_ptr Bank::getDevCard()
{
	if (!m_devCards.empty())
	{
		devCard_ptr card = std::move(m_devCards.back());
		m_devCards.pop_back();
		return card;
	}
	else
	{
		return (Bank::devCard_ptr{});
	}
}



void Bank::initResourceCardStacks() {
	m_lumbers.swap(Factory::getInstance().createBankItem<Lumber>());
	m_bricks.swap(Factory::getInstance().createBankItem<Brick>());
	m_wools.swap(Factory::getInstance().createBankItem<Wool>());
	m_grains.swap(Factory::getInstance().createBankItem<Grain>());
	m_ores.swap(Factory::getInstance().createBankItem<Ore>());
}

void Bank::initDevCardStack() {
	initKnites();
	initMonopoly();
	initRoadBuiding();
	initYearOfPlenty();
	initVictoryPoint();

	auto seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(m_devCards.begin(), m_devCards.end(), std::default_random_engine((unsigned int)seed));
}

void Bank::initKnites()
{
	std::vector<knite_ptr> knites = std::move(Factory::getInstance().createBankItem<Knite>());

	//for each Knite, cast it to IDevCard and add it to the deck
	for (size_t i = 0; i < knites.size(); i++)
	{
		m_devCards.push_back(std::move(static_cast<std::unique_ptr<IDevCard>> (std::move(knites[i]))));
	}
}

void Bank::initMonopoly()
{
	std::vector<monopoly_ptr> monopolies = std::move(Factory::getInstance().createBankItem<Monopoly>());

	//for each Monopoly, cast it to IDevCard and add it to the deck
	for (size_t i = 0; i < monopolies.size(); i++)
	{
		m_devCards.push_back(std::move(static_cast<std::unique_ptr<IDevCard>> (std::move(monopolies[i]))));
	}
}

void Bank::initRoadBuiding()
{
	std::vector<roadBuilding_ptr> roadBuildings = std::move(Factory::getInstance().createBankItem<RoadBuilding>());

	//for each Monopoly, cast it to IDevCard and add it to the deck
	for (size_t i = 0; i < roadBuildings.size(); i++)
	{
		m_devCards.push_back(std::move(static_cast<std::unique_ptr<IDevCard>> (std::move(roadBuildings[i]))));
	}
}

void Bank::initYearOfPlenty()
{
	std::vector<yearOfPlenty_ptr> yearOfPlenties = std::move(Factory::getInstance().createBankItem<YearOfPlenty>());

	//for each Monopoly, cast it to IDevCard and add it to the deck
	for (size_t i = 0; i < yearOfPlenties.size(); i++)
	{
		m_devCards.push_back(std::move(static_cast<std::unique_ptr<IDevCard>> (std::move(yearOfPlenties[i]))));
	}
}

void Bank::initVictoryPoint()
{
	std::vector<victoryPoint_ptr> victoryPoints = std::move(Factory::getInstance().createBankItem<VictoryPoint>());

	//for each Monopoly, cast it to IDevCard and add it to the deck
	for (size_t i = 0; i < victoryPoints.size(); i++)
	{
		m_devCards.push_back(std::move(static_cast<std::unique_ptr<IDevCard>> (std::move(victoryPoints[i]))));
	}
}

void Bank::initBuildings() {

}

void Bank::initRoads()
{
	//m_roads = std::move(Factory::getInstance().createBankItem<Road>());
	m_roads.swap(Factory::getInstance().createBankItem<Road>());
}

void Bank::initSettlements()
{
	//m_settlements = std::move(Factory::getInstance().createBankItem<Settlement>());
	m_settlements.swap(Factory::getInstance().createBankItem<Settlement>());
}

void Bank::initCities()
{
	//m_cities = std::move(Factory::getInstance().createBankItem<City>());
	m_cities.swap(Factory::getInstance().createBankItem<City>());

}
