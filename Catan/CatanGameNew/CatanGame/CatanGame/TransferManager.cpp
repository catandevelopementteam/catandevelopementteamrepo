#include "TransferManager.h"
#include "Time.h"



TransferManager::TransferManager()
{
}


TransferManager::~TransferManager()
{
}

void TransferManager::stealRandomResource(Player & robbingPlayer, Player & robedPlayer)
{
	std::vector<ResourceType> resources;

	for (size_t i = 0; i < robedPlayer.getBricks().size(); ++i)
	{
		resources.push_back(ResourceType::brick);
	}
	for (size_t i = 0; i < robedPlayer.getLumbers().size(); ++i)
	{
		resources.push_back(ResourceType::lumber);
	}
	for (size_t i = 0; i < robedPlayer.getGrains().size(); ++i)
	{
		resources.push_back(ResourceType::grain);
	}
	for (size_t i = 0; i < robedPlayer.getOres().size(); ++i)
	{
		resources.push_back(ResourceType::ore);
	}
	for (size_t i = 0; i < robedPlayer.getWools().size(); ++i)
	{
		resources.push_back(ResourceType::wool);
	}

	int noOfRes = resources.size();
	if (noOfRes)
	{
		auto seed = std::chrono::system_clock::now().time_since_epoch().count();		
		std::srand((unsigned int)seed);
		int chooseType = rand() % noOfRes;
		ResourceType type = resources[chooseType];
		switch (type)
		{
		case ResourceType::lumber:
			robbingPlayer.buyLumber(std::move(robedPlayer.sellLumber()));
			break;
		case ResourceType::brick:
			robbingPlayer.buyBrick(std::move(robedPlayer.sellBrick()));
			break;
		case ResourceType::grain:
			robbingPlayer.buyGrain(std::move(robedPlayer.sellGrain()));
			break;
		case ResourceType::ore:
			robbingPlayer.buyOre(std::move(robedPlayer.sellOre()));
			break;
		case ResourceType::wool:
			robbingPlayer.buyWool(std::move(robedPlayer.sellWool()));
			break;
		}
	}	
}

void TransferManager::transferResources(const std::vector<ResourceType>& resources, IResourceHolder & source, IResourceHolder& destination)
{
	for (auto type : resources)
	{
		transferResource(type, source, destination);
	}
}



void TransferManager::transferResource(const ResourceType & type, IResourceHolder & source, IResourceHolder & destination)
{
	switch (type)
	{
	case ResourceType::lumber:
		destination.buyLumber(std::move(source.sellLumber()));
		break;
	case ResourceType::brick:
		destination.buyBrick(std::move(source.sellBrick()));
		break;
	case ResourceType::grain:
		destination.buyGrain(std::move(source.sellGrain()));
		break;
	case ResourceType::ore:
		destination.buyOre(std::move(source.sellOre()));
		break;
	case ResourceType::wool:
		destination.buyWool(std::move(source.sellWool()));
		break;
	}
}

void TransferManager::transferBuilding(const BuildingType building, IResourceHolder & source, IResourceHolder& destination)
{
	switch (building)
	{
	case BuildingType::road:
		destination.addRoad(std::move(source.getRoad()));
		break;
	case BuildingType::settlement:
		destination.addSettlement(std::move(source.getSettlement()));
		break;
	case BuildingType::city:
		destination.addCity(std::move(source.getCity()));
		break;
	}
}


void TransferManager::transferMonopolyResource(const ResourceType resource, std::vector<Player>& source, Player & destination)
{
	switch (resource)
	{
	case ResourceType::lumber:
	{
		for (auto& player : source)
		{
			for (size_t i = 0; i < player.getLumbers().size(); ++i)
			destination.buyLumber(std::move(player.sellLumber()));
		}
	}
		break;
	case ResourceType::brick:
	{
		for (auto& player : source)
		{
			for (size_t i = 0; i < player.getBricks().size(); ++i)
				destination.buyBrick(std::move(player.sellBrick()));
		}
	}
		break;
	case ResourceType::grain:
	{
		for (auto& player : source)
		{
			for (size_t i = 0; i < player.getGrains().size(); ++i)
				destination.buyGrain(std::move(player.sellGrain()));
		}
	}
		break;
	case ResourceType::ore:
	{
		for (auto& player : source)
		{
			for (size_t i = 0; i < player.getOres().size(); ++i)
				destination.buyOre(std::move(player.sellOre()));
		}
	}
		break;
	case ResourceType::wool:
	{
		for (auto& player : source)
		{
			for (size_t i = 0; i < player.getWools().size(); ++i)
				destination.buyWool(std::move(player.sellWool()));
		}
	}
		break;
	}

}

void TransferManager::transferDevCard(Bank & source, Player & destination)
{
	destination.addTmpDevCard(source.getDevCard());
}

void TransferManager::DevCardCanBeUsed(Player & player)
{
	if (player.hasTmpDevCard())
	{
		for (size_t i = 0; i < player.getTmpDevCards().size(); ++i)
		player.addHiddenDevCard(std::move(player.getTmpDevCard()));
	}
}


