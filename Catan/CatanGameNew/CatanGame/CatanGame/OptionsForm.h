#pragma once

#include "IForm.h"

class OptionsForm : public IForm
{
private:
	std::vector<IForm::TextPosition> m_positions;
	std::vector<std::string> m_messages;
	size_t m_totalFormLines;
	int m_totalLinesShown;

public:
	OptionsForm();
	OptionsForm(std::string defaultBodyPath);
	~OptionsForm();

	void updateForm();
	void addMessages(std::vector<std::string>& messages);

private:
	void InitializePositions(std::string defaultBodyPath);
	void InitializeMessages();
};

