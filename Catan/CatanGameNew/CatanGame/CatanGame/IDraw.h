#pragma once

#include <vector>
#include <string>

#include "IForm.h"
#include "Player.h"
#include "Board.h"

class IDraw
{
public:
	IDraw();
	virtual ~IDraw();


public:
	virtual void draw() = 0;

	virtual void displayMessage(const std::string message) = 0;
	virtual void highlightIntersections() = 0;
	virtual void unhighlightIntersections(const std::vector<Player>& players) = 0;
	virtual void highlightEdges() = 0;
	virtual void unhighlightEdges(const std::vector<Player>& players) = 0;
	virtual void highlightHexagons() = 0;
	virtual void unhighlightHexagons(const Board& board) = 0;
	virtual void turnKniteOrNot() = 0;

	virtual void chooseTurnDevCard() = 0;
	virtual void chooseTurnKnite() = 0;
	virtual void chooseHex() = 0;
	virtual void choosePlayer(const std::vector<Player>& players, const Player& currentPlayer) = 0;
	virtual void chooseDiscardedCards(uint8_t totalNumber) = 0;
	virtual void chooseAction() = 0;
	virtual void chooseBuild() = 0;
	virtual void chooseIntersection() = 0;
	virtual void chooseEdge() = 0;
	virtual void chooseTrade() = 0;
	virtual void choosePortType() = 0;
	virtual void chooseResourcesToReceive() = 0;
	virtual void chooseResourcesToTrade() = 0;
	virtual void chooseResourceToMonopolize() = 0;
	virtual void updateCurrentPlayer(const Player& player) = 0;
	virtual void updatePlayersForm(const std::vector<Player>& players) = 0;
	virtual void updateBoardForm(const std::vector<Player>& players, const Board& board) = 0;
	virtual void updateDiceForm(const std::pair<uint8_t, uint8_t> dice) = 0;
	virtual void updateBoardAndPlayers(const std::vector<Player>& players, const Player& player, const Board& board) = 0;
};

