#pragma once

enum class TradeType
{
	fourToOne,
	port,
	customTrade
};