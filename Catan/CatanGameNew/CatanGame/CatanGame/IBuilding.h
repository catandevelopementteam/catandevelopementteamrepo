#pragma once
#include <string>
#include <iostream>
#include <vector>

#include "Rules.h"
#include "BuildingType.h"

class IBuilding
{
protected:
	BuildingType m_type;
	uint8_t m_instancesAllowed;

protected:
	virtual void checkForInstanceOverflow() = 0;

	IBuilding();
	IBuilding(const IBuilding&) = default;
	IBuilding& operator=(const IBuilding&) = default;
	IBuilding(IBuilding&&) = default;
	IBuilding& operator=(IBuilding&&) = default;

public:
	virtual ~IBuilding() = 0;
	virtual BuildingType getType();
};

