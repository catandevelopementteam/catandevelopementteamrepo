#pragma once
#include "IPort.h"


class OrePort :public IPort
{
private:
	PortType m_type;
	OrePort();
	~OrePort();
public:
	static OrePort& getInstance();
	std::string typeName() const;
	PortType getType() const;
	OrePort(OrePort const&) = delete;
	void operator=(OrePort const&) = delete;


};