#include "GrainPort.h"


GrainPort::GrainPort()
{
	m_type = IPort::PortType::grainPort;
}


GrainPort::~GrainPort()
{
}

GrainPort & GrainPort::getInstance()
{
	static GrainPort instance;
	return instance;
}

std::string GrainPort::typeName() const
{
	return "GrainPort";
}

IPort::PortType GrainPort::getType() const
{
	return m_type;
}
