#pragma once
#include "IPort.h"


class WoolPort :public IPort
{
private:
	PortType m_type;
	WoolPort();
	~WoolPort();
public:
	static WoolPort& getInstance();
	std::string typeName() const;
	PortType getType() const;
	WoolPort(WoolPort const&) = delete;
	void operator=(WoolPort const&) = delete;


};