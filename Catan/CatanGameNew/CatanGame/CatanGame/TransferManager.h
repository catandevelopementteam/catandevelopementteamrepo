#pragma once

#include "ResourceType.h"
#include "BuildingType.h"
#include "DevCardType.h"
#include "PortType.h"
#include "TradeType.h"
#include "Player.h" 
#include "Bank.h"
#include "Board.h"

class TransferManager
{
public:
	TransferManager();
	~TransferManager();

	void stealRandomResource(Player& robbingPlayer, Player& robedPlayer);
	
	void transferResources(const std::vector<ResourceType>& resources, IResourceHolder& source, IResourceHolder& destination);	

	void transferResource(const ResourceType& type, IResourceHolder& source, IResourceHolder& destination); 	
	
	void transferBuilding(const BuildingType building, IResourceHolder& source, IResourceHolder& destination); 	
	
	void transferMonopolyResource(const ResourceType resource, std::vector<Player>& source, Player& destination); 

	void transferDevCard(Bank& source, Player& destination);

	void DevCardCanBeUsed(Player& player);
	
};

