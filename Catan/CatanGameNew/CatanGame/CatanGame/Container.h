#pragma once

#include <vector>
#include <cassert>


/*
generic wrapper container class
*/

template <typename T>
struct container
{
	using value_type = T;
	using container_type = std::vector<value_type>;
	using size_type = typename container_type::size_type;
	using iterator = typename container_type::iterator;
	using const_iterator = typename container_type::const_iterator;

private:
	container_type m_items;

public:
	container() = default;
	container(container const&) = delete;
	container& operator=(container const&) = delete;
	container(container&&) = default;
	container& operator=(container&& other) = default;


	value_type& operator[](size_type pos)
	{
		assert(pos < size());
		return m_items[pos];
	}


	value_type const& operator[](size_type pos) const
	{
		assert(pos < size());
		return m_items[pos];
	}


	std::size_t size() const noexcept
	{
		return m_items.size();
	}


	void push(value_type item)
	{
		m_items.emplace_back(std::move(item));
	}


	value_type pop()
	{
		value_type r;
		if (!m_items.empty())
		{
			r = std::move(m_items.back());
			m_items.pop_back();
		}

		return r;
	}


	container_type const& get() const
	{
		return m_items;
	}


	void swap(container_type other)
	{
		m_items = std::move(other);
	}


	iterator begin()
	{
		return m_items.begin();
	}

	const_iterator begin() const
	{
		return m_items.begin();
	}

	const_iterator cbegin() const
	{
		return m_items.cbegin();
	}

	iterator end()
	{
		return m_items.end();
	}

	const_iterator end() const
	{
		return m_items.end();
	}

	const_iterator cend() const
	{
		return m_items.cend();
	}
};

