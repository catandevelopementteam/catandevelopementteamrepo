#include "YearOfPlenty.h"

uint8_t YearOfPlenty::s_instancesCreated = 0;

YearOfPlenty::YearOfPlenty()
{
	m_type = DevCardType::yearOfPlenty;
	m_instancesAllowed = Rules::getNumOfYearOfPlentyCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

YearOfPlenty::~YearOfPlenty()
{
}

void YearOfPlenty::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}