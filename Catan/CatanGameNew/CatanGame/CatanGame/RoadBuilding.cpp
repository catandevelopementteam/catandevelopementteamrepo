#include "RoadBuilding.h"

uint8_t RoadBuilding::s_instancesCreated = 0;

RoadBuilding::RoadBuilding()
{
	m_type = DevCardType::roadBuilding;
	m_instancesAllowed = Rules::getNumOfRoadBuildingCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

RoadBuilding::~RoadBuilding()
{
}

void RoadBuilding::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}