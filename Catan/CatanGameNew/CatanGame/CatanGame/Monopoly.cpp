#include "Monopoly.h"

uint8_t Monopoly::s_instancesCreated = 0;

Monopoly::Monopoly()
{
	m_type = DevCardType::monopoly;
	m_instancesAllowed = Rules::getNumOfMonopolyCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

Monopoly::~Monopoly()
{
}

void Monopoly::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}