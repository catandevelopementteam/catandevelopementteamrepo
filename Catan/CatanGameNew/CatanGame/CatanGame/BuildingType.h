#pragma once

enum class BuildingType
{
	settlement,
	city,
	road,
	none
};