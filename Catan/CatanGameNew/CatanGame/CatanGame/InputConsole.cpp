#include "InputConsole.h"

InputConsole::InputConsole()
{

}


InputConsole::~InputConsole()
{
}

DevCardType InputConsole::chooseTurnDevCard()
{
	std::vector<char> options = { '0', '1', '2', '3', '4' };
	//iterate until I het one of the options
	while (true)
	{
		switch (getKey(options))
		{
		case '1': return DevCardType::knite;
		case '2': return DevCardType::monopoly;
		case '3': return DevCardType::roadBuilding;
		case '4': return DevCardType::yearOfPlenty;
		case '0': throw std::string("Turn developement card canceled.");
		default:
			break;
		}
	}


}

bool InputConsole::chooseTurnKnite()
{
	std::vector<char> options = { 'Y', 'N' };
	//iterate until I het one of the options
	while (true)
	{
		switch (getKey(options))
		{
		case 'Y': return true;
		case 'N': return false;
		default: return false;
		}
	}
}

uint8_t InputConsole::chooseHex()
{
	std::string input;
	uint8_t option;
	//iterate until I one of the options is selected
	while (true)
	{
		std::cin >> input;
		//try converting input to number
		try
		{
			option = std::stoi(input);
		}
		catch (const std::exception&)
		{
			continue;
		}

		if (option >= 0 && option <= 18)
			return option;
	}
}

const Player& InputConsole::choosePlayer(const std::vector<Player>& players, const Player& currentPlayer)
{
	std::vector<char> options;
	options.emplace_back('0');
	options.emplace_back('1');
	options.emplace_back('2');
	if (players.size() == 4) { options.emplace_back('3'); }

	//find index of current player in vector
	uint8_t currentPlayerIndex = getCurrentPlayerIndex(players, currentPlayer);

	uint8_t input;
	while (true)
	{
		//try converting input to number
		try
		{
			input = getKey(options);
		}
		catch (const std::exception&)
		{
			continue;
		}

		//transform from character '0' to letter 0
		input -= 48;

		if (input == 0)
			throw std::string("Choose player canceled");
		
		if (currentPlayerIndex <= input - 1) // -1 because user input starts with 1 for player 1...
			return players[input];
		else
			return players[input - 1];
	}
}

std::vector<ResourceType> InputConsole::chooseDiscardedCards(uint8_t totalNumber)
{
	std::vector<char> options = { '1', '2', '3', '4', '5' };
	std::vector<ResourceType> discardedCards;
	for (size_t i = 0; i < totalNumber; i++)
	{
		//iterate until I het one of the options
		while (discardedCards.size() < totalNumber)
		{
			Sleep(500);
			switch (getKey(options))
			{
			case '1': discardedCards.emplace_back(ResourceType::lumber);
				break;
			case '2': discardedCards.emplace_back(ResourceType::brick);
				break;
			case '3': discardedCards.emplace_back(ResourceType::wool);
				break;
			case '4': discardedCards.emplace_back(ResourceType::grain);
				break;
			case '5': discardedCards.emplace_back(ResourceType::ore);
				break;
			default:
				break;
			}
		}
	}

	return discardedCards;
}

ActionType InputConsole::chooseAction()
{
	std::vector<char> options = { '0', '1', '2', '3', '4' };
	//iterate until I het one of the options
	switch (getKey(options))
	{
	case '1': return ActionType::build;
	case '2': return ActionType::trade;
	case '3': return ActionType::turnDevCard;
	case '4': return ActionType::buyDevCard;
	case '0': return ActionType::endTurn;
	default: return ActionType::build;
	}
}

BuildingType InputConsole::chooseBuild()
{
	std::vector<char> options = { '0', '1', '2', '3' };
	//iterate until I het one of the options
	while (true)
	{
		switch (getKey(options))
		{
		case '1': return BuildingType::settlement;
		case '2': return BuildingType::city;
		case '3': return BuildingType::road;
		case '0': throw std::string("Building construction canceled.");
		default:
			break;
		}
	}
}

uint8_t InputConsole::chooseIntersection()
{
	std::string input;
	uint8_t option;
	//iterate until I one of the options is selected
	while (true)
	{
		std::cin >> input;
		//try converting input to number
		try
		{
			option = std::stoi(input);
		}
		catch (const std::exception&)
		{
			continue;
		}

		if (option >= 0 && option <= 53)
			return option;
	}
}

uint8_t InputConsole::chooseEdge()
{
	std::string input;
	uint8_t option;
	//iterate until I one of the options is selected
	while (true)
	{
		std::cin >> input;
		//try converting input to number
		try
		{
			option = std::stoi(input);
		}
		catch (const std::exception&)
		{
			continue;
		}

		if (option >= 0 && option <= 71)
			return option;
	}
}

TradeType InputConsole::chooseTrade()
{
	std::vector<char> options = { '0', '1', '2', '3' };
	//iterate until I het one of the options
	while (true)
	{
		switch (getKey(options))
		{
		case '1': return TradeType::fourToOne;
		case '2': return TradeType::port;
		case '3': return TradeType::customTrade;
		case '0': throw std::string("Trading operation canceled.");
		default:
			throw std::string("Trading operation canceled.");
		}
	}
}

PortType InputConsole::choosePortType()
{
	std::vector<char> options = { '0', '1', '2', '3', '4', '5', '6' };
	//iterate until I het one of the options
	while (true)
	{
		switch (getKey(options))
		{
		case '1': return PortType::genericPort;
		case '2': return PortType::lumberPort;
		case '3': return PortType::brickPort;
		case '4': return PortType::woolPort;
		case '5': return PortType::grainPort;
		case '6': return PortType::orePort;
		case '0': throw std::string("Trading operation canceled.");
		default:
			throw std::string("Trading operation canceled.");
		}
	}
}

std::vector<ResourceType> InputConsole::chooseResourcesToReceive()
{
	std::vector<ResourceType> resources;
	chooseResources(resources);
	return resources;
}

std::vector<ResourceType> InputConsole::chooseResourcesToTrade()
{
	std::vector<ResourceType> resources;
	chooseResources(resources);
	return resources;
}

ResourceType InputConsole::chooseResourceToMonopolize()
{
	std::vector<char> options = { '1', '2', '3', '4', '5' };

	switch (getKey(options))
	{
	case '1': return ResourceType::lumber;
	case '2': return ResourceType::brick;
	case '3': return ResourceType::wool;
	case '4': return ResourceType::grain;
	case '5': return ResourceType::ore;
	default: return ResourceType::lumber;
	}
}
char InputConsole::getKey(std::vector<char> options) const
{
	while (true)
	{
		for (char c : options)
		{
			if (GetAsyncKeyState(c) & 0x8000)
			{
				return c;
			}
		}
	}
}

void InputConsole::chooseResources(std::vector<ResourceType>& resources)
{
	std::vector<char> options = { '0', '1', '2', '3', '4', '5' };
	//iterate until I get one of the options
	while (true)
	{
		Sleep(500);
		switch (getKey(options))
		{
		case '1': resources.emplace_back(ResourceType::lumber);
			break;
		case '2': resources.emplace_back(ResourceType::brick);
			break;
		case '3': resources.emplace_back(ResourceType::wool);
			break;
		case '4': resources.emplace_back(ResourceType::grain);
			break;
		case '5': resources.emplace_back(ResourceType::ore);
			break;
		case '0': return;
		default:
			return;
		}
	}
}

uint8_t InputConsole::getCurrentPlayerIndex(const std::vector<Player>& players, const Player& currentPlayer)
{
	uint8_t currentPlayerIndex = 0;
	for (size_t i = 0; i < players.size(); i++)
	{
		if (currentPlayer.getId() == players[i].getId())
			break;

		currentPlayerIndex++;
	}
	return currentPlayerIndex;
}




