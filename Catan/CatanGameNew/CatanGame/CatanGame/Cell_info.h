#pragma once

#include <iostream>
#include <string.h>

class Cell_info
{
public:
	Cell_info(char symbol, uint16_t color = 0x0007);
	~Cell_info();

	void setSymbol(char symbol);
	void setColor(uint16_t color);

	char getSymbol() const;
	uint16_t getColor() const;

private:
	char m_symbol;
	uint16_t m_color;
};

