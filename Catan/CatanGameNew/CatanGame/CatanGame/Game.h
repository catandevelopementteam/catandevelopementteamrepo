#pragma once

#include <time.h>

#include "Player.h"
#include "Bank.h"
#include "Board.h"
#include "TransferManager.h"
#include "ChecksManager.h"
#include "IOManager.h"


class Game
{
private:
	
	Board m_board;
	Bank m_bank;
	TransferManager m_tm;
	ChecksManager m_cm;
	IOManager m_iom;

	std::vector<Player> m_players;	
	std::pair<uint8_t, uint8_t> m_currentDice;
	uint16_t m_turns;

public:
	Game(std::string path);
	~Game();
	void play();

private:
	std::pair<uint8_t, uint8_t> getDice() const;
	//std::vector<Player>& getPlayers();
	const std::vector<Player> & getPlayers() const;
	Player& getCurrentPlayer();

	uint8_t getNumberOfPlayers();
	void getPlayerNames(std::vector<std::string>& names, uint8_t numberOfPlayers);
	void placeInitialBuildings();
	void placeInitialBuildings_auto();
	void placeFreeSettlement(Player& player);
	void placeFreeSettlement(Player& player, int id);
	void placeFreeRoad(Player& player);
	void placeFreeRoad(Player& player, int id);
	void splitResourcesAfterSecondSettlement();

	void rollDice();
	void splitResources(uint8_t diceValue);
	void chooseTurnKnite();
	void moveRobber();
	void robPlayer(uint8_t hexID);
	bool shouldAnyoneDiscard();
	void discardCards(Player& player, uint8_t numberOfResources);

	bool chooseAction();

	void build();
	void buildSettlement();
	void buildCity();
	void buildRoad();

	void trade();
	void tradeFourToOne();
	void tradeAtPort();
	void tradeThreeToOne();
	void tradeTwoToOne(ResourceType resourceType);
	PortType resourceToPort(ResourceType resourceType);
	void tradeWithPlayers();

	void devCardsCanBeUsed();
	void turnDevCard();
	void turnKnite();
	void turnMonopoly();
	void turnRoadBuilding();
	void turnYearOfPlenty();
	void buyDevCard();
	void endTurn();

	void initPlayers();
	bool isGameOver();
	void calculateLargestArmy();
	
	//method that returns the player who has the longest road from all the players
	void calculateLongestRoad();

	void createTradeMessage(const Player& currentPlayer,
		const Player& playerWhoAgrees,
		const std::vector<ResourceType>& tradedResources,
		const std::vector<ResourceType>& receivedResources);
	void createTradeMessage(const Player& currentPlayer, 
		const std::vector<ResourceType>& tradedResources, 
		const std::vector<ResourceType>& receivedResources);
	void createTurnDevCardMessage(const Player& currentPlayer, const DevCardType type);
	void createBuildingMessage(const Player& currentPlayer, const BuildingType type);
	void createDiscardMessage(const Player& player, uint8_t numberOfResources);
	void createDiscardedResourcesMessage(const Player& player, std::vector<ResourceType> resources);
	void createRobPlayerMessage(const Player& robingPlayer, const Player& robedPlayer);
	void createBuyDevCardMessage(const Player& player);
	void createEndTurnMessage(const Player& player);
	void vectorOfResourcesToString(std::string& message, std::vector<ResourceType> resources);
};

