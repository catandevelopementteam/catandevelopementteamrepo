#pragma once

//forward declaration of Factory
class Factory;

#include "Node.h"
#include "IBuilding.h"

class City : public IBuilding
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	//no initialisation in constructor because the default value for a smart pointer is nullptr
	std::shared_ptr<Node> m_node;
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

private:
	City();

public:
	void setNode(std::shared_ptr<Node> intersection);
	std::shared_ptr<Node> getNode();

	~City();

	City(const City& other) = delete;
	City& operator=(const City& other) = delete;

	City(City&& other) = default;
	City& operator=(City&& other) = default;

};