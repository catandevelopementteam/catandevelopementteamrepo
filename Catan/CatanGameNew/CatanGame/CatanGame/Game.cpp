#include "Game.h"


Game::Game(std::string path) :
	m_board(path)
{
	initPlayers();
	m_turns = 0;
	m_iom = IOManager(IOManager::VisualisatioType::console, 
		m_board, 
		m_players, 
		getCurrentPlayer());
}

void Game::initPlayers()
{
	uint8_t numberOfPlayers = getNumberOfPlayers();

	std::vector<std::string> playerNames;
	playerNames.reserve(numberOfPlayers);
	getPlayerNames(playerNames, numberOfPlayers);

	std::vector<uint8_t> playerColors = {
		Rules::getConsoleColorOfPlayer1(),
		Rules::getConsoleColorOfPlayer2(),
		Rules::getConsoleColorOfPlayer3(),
		Rules::getConsoleColorOfPlayer4()
	};

	for (uint8_t i = 0; i < numberOfPlayers; i++)
	{
		m_players.push_back(Player(i, playerNames[i], playerColors[i]));
	}
}

bool Game::isGameOver()
{
	for (auto const& player : m_players)
	{
		int victoryPoints = player.getSettlements().size() + 
			player.getCities().size() * 2 + 
			player.getNumberOfSpecificHiddenDevCard(DevCardType::victoryPoint);

		if (player.hasLargestArmy()) victoryPoints += 2;
		if (player.hasLongestRoad()) victoryPoints += 2;

		if ( victoryPoints >= 10) return true;
	}
	return false;
}

void Game::calculateLargestArmy()
{
	//find largest army owner
	auto it_largestArmyOwner = std::find_if(m_players.begin(), m_players.end(),
		[](Player& player) {return player.hasLargestArmy(); });

	
	//find the player with the maximum number of knits
	auto const& it_largestArmyCandidate = std::max_element(
		m_players.begin(), m_players.end(),
		[](Player const& lhs, Player const& rhs)
	{
		return lhs.getNumberOfKnits() < rhs.getNumberOfKnits();
	}
	);
	//if the candidate has a largest army than the owner, change the owner
	if (it_largestArmyOwner != m_players.end())
	{
		if (it_largestArmyCandidate->getNumberOfKnits() > it_largestArmyOwner->getNumberOfKnits())
		{
			it_largestArmyOwner->setLargestArmy(false);
			it_largestArmyCandidate->setLargestArmy(true);
		}
	}
	//else if there is no previous owner, candidate becomes new owner
	else if (it_largestArmyCandidate->getNumberOfKnits() >= 3)
	{
		it_largestArmyCandidate->setLargestArmy(true);
	}
}	


Game::~Game()
{
}

void Game::play()
{
	placeInitialBuildings();
	splitResourcesAfterSecondSettlement();
	m_iom.updatePlayersForm(m_players);

	while (!isGameOver())
	{
		m_iom.updateCurrentPlayerForm(getCurrentPlayer());
		chooseTurnKnite();
		rollDice();

		bool endTurn = false;
		while (!endTurn)
		{
			endTurn = chooseAction();
		}

		m_turns++;
	}

}

uint8_t Game::getNumberOfPlayers()
{
	std::cout << "You are three or four friends who want to play? ";
	uint8_t numberOfPlayers = 0;
	while (true)
	{
		std::string number;
		std::cin >> number;
		try
		{
			numberOfPlayers = std::stoi(number);
			if (numberOfPlayers == 3 || numberOfPlayers == 4)
			{
				break;
			}
		}
		catch (const std::exception&)
		{
			continue;
		}
	}

	return numberOfPlayers;
}

void Game::getPlayerNames(std::vector<std::string>& names, uint8_t numberOfPlayers)
{
	for (uint8_t i = 0; i < numberOfPlayers; i++)
	{
		std::cout << "Player " << std::to_string(i + 1) << " name: ";
		std::string name;
		std::cin >> name;
		names.push_back(name);
		std::cout << std::endl;
	}
}

void Game::placeInitialBuildings()
{
	//iterate forward - each player place first settlement with road
	for (auto it = m_players.begin(); it != m_players.end(); ++it)
	{
		placeFreeSettlement(*it);
		placeFreeRoad(*it);
	}
	//iterate backrward - each player place second settlement with road
	for (auto it = m_players.rbegin(); it != m_players.rend(); ++it)
	{
		placeFreeSettlement(*it);
		placeFreeRoad(*it);
	}
}

void Game::placeInitialBuildings_auto()
{
	std::vector<std::pair<int, int>> places;
	places.emplace_back(std::make_pair(8, 10));
	places.emplace_back(std::make_pair(15, 19));
	places.emplace_back(std::make_pair(27, 40));
	places.emplace_back(std::make_pair(22, 33));
	places.emplace_back(std::make_pair(34, 50));
	places.emplace_back(std::make_pair(39, 57));
	places.emplace_back(std::make_pair(32, 44));
	places.emplace_back(std::make_pair(37, 55));
	int i = 0;
	//iterate forward - each player place first settlement with road
	for (auto it = m_players.begin(); it != m_players.end(); ++it)
	{
		placeFreeSettlement(*it, places[i].first);
		placeFreeRoad(*it, places[i].second);
		i++;
	}
	//iterate backrward - each player place second settlement with road
	for (auto it = m_players.rbegin(); it != m_players.rend(); ++it)
	{
		placeFreeSettlement(*it, places[i].first);
		placeFreeRoad(*it, places[i].second);
		i++;
	}
}

void Game::placeFreeSettlement(Player& player)
{
	while (true)
	{
		try
		{
			m_iom.updateCurrentPlayerForm(player);
			uint8_t intersectionID = m_iom.chooseIntersection(getPlayers());
			if (m_cm.canBuildAtNode(m_board, intersectionID))
			{
				m_tm.transferBuilding(BuildingType::settlement, m_bank, player);
				player.buildSettlement(m_board.findNode(intersectionID));

				createBuildingMessage(player, BuildingType::settlement);
				break;
			}
		}
		catch (const std::string& message)
		{
			m_iom.displayMessage(message);
		}

	}
	m_iom.updateBoardForm(m_players, m_board);
}

void Game::placeFreeSettlement(Player& player, int id)
{
	while (true)
	{
		try
		{
			m_iom.updateCurrentPlayerForm(player);
			if (m_cm.canBuildAtNode(m_board, id))
			{
				m_tm.transferBuilding(BuildingType::settlement, m_bank, player);
				player.buildSettlement(m_board.findNode(id));

				createBuildingMessage(player, BuildingType::settlement);
				break;
			}
		}
		catch (const std::string& message)
		{
			m_iom.displayMessage(message);
		}

	}
	m_iom.updateBoardForm(m_players, m_board);
}

void Game::placeFreeRoad(Player& player)
{
	while (true)
	{
		try
		{
			m_iom.updateCurrentPlayerForm(player);
			uint8_t edgeID = m_iom.chooseEdge(getPlayers());
			if (m_cm.canBuildAtEdge(player, m_board, edgeID) &&
				player.getRoads().size() < 15)
			{
				m_tm.transferBuilding(BuildingType::road, m_bank, player);
				player.buildRoad(m_board.findEdge(edgeID));

				createBuildingMessage(player, BuildingType::road);
				break;
			}
		}
		catch (const std::string& message)
		{
			m_iom.displayMessage(message);
		}
	}
	m_iom.updateBoardForm(m_players, m_board);
}

void Game::placeFreeRoad(Player& player, int id)
{
	while (true)
	{
		try
		{
			m_iom.updateCurrentPlayerForm(player);
			if (m_cm.canBuildAtEdge(player, m_board, id) &&
				player.getRoads().size() < 15)
			{
				m_tm.transferBuilding(BuildingType::road, m_bank, player);
				player.buildRoad(m_board.findEdge(id));

				createBuildingMessage(player, BuildingType::road);
				break;
			}
		}
		catch (const std::string& message)
		{
			m_iom.displayMessage(message);
		}
	}
	m_iom.updateBoardForm(m_players, m_board);
}

void Game::splitResourcesAfterSecondSettlement()
{
	for (auto player = m_players.begin(); player != m_players.end(); ++player)
	{
		IResourceHolder::settlements_type& setts = (*player).getSettlements();
		assert(setts.size() > 0);
		int nodeId = setts[setts.size() - 1]->getNode()->getId();

		for (auto& hex : m_board.getVectorOfHex())
		{
			std::vector<std::shared_ptr<Node>> hexNodes = hex.getHexNodes();
			auto node = std::find_if(hexNodes.begin(), hexNodes.end(),
				[nodeId](const std::shared_ptr<Node>& node) {
				return (node->getId()) == nodeId;
			}
			);
			if (node != hexNodes.end())
			{
				ResourceType type = hex.getResourceType();
				m_tm.transferResource(type, m_bank, *player);
			}
		}
	}
}

void Game::rollDice()
{
	srand((unsigned int)time(NULL));
	uint8_t dice1 = rand() % 6 + 1;
	uint8_t dice2 = rand() % 6 + 1;
	m_currentDice = std::make_pair(dice1, dice2);
	m_iom.updateDiceForm(m_currentDice);

	if (dice1 + dice2 == 7)
	{
		shouldAnyoneDiscard();
		moveRobber();
	}
	else
	{
		splitResources(dice1+dice2);
		m_iom.updateBoardAndPlayers(m_players, getCurrentPlayer(), m_board);
	}

}

void Game::splitResources(uint8_t diceValue)
{
	using node_ptr = std::shared_ptr<Node>;

	std::vector<Hex> hexes = m_board.getVectorOfHex();
	for (auto hex : hexes)
	{
		if ((hex.getAssignedNumber() == diceValue)&&(hex.hasRobber() == false))
		{
			std::vector<node_ptr> hexNodes = hex.getHexNodes();
			for (auto node : hexNodes)
			{
				int id = node->getOwnerId();
				if (id != 9)
				{
					ResourceType type = hex.getResourceType();
					m_tm.transferResource(type, m_bank, m_players[id]);
				}
			}
		}
	}
}

void Game::chooseTurnKnite()
{
	//try turn knite until no exception is trown
	while (true)
	{
		try
		{
			if (m_iom.chooseTurnKnite())
			{
				turnKnite();
			}
			break;
		}
		catch (const std::string& message)
		{
			m_iom.displayMessage(message);
			continue;
		}
	}
}

void Game::moveRobber()
{
	//find where is the robber placed now:
	auto now = find_if(m_board.getVectorOfHex().begin(), m_board.getVectorOfHex().end(),
		[](const Hex& hex)
	{
		return hex.hasRobber() == true;
	}
	);
	//compare with the desired location to move the robber
	uint8_t next;
	while (true)
	{
		next = m_iom.chooseHex(m_board);
		if ((*now).getId() != next)
		{
			break;
		}
		else m_iom.displayMessage("You have to choose another location to place the robber");
		continue;
	}
	m_board.removeRobber(now->getId());
	m_board.placeRobber(next);
	m_iom.updateBoardForm(m_players, m_board);
	robPlayer(next);
	m_iom.updateBoardAndPlayers(m_players, getCurrentPlayer(), m_board);
}

void Game::robPlayer(uint8_t hexID)
{
	while (true)
	{
		try
		{
			const Player& robedPlayer = m_iom.choosePlayer(getPlayers(), getCurrentPlayer());
			if (m_board.hasAccesToHex(robedPlayer.getId(), hexID))
			{
				m_tm.stealRandomResource(getCurrentPlayer(), const_cast<Player&>(robedPlayer));
				createRobPlayerMessage(getCurrentPlayer(), robedPlayer);
			}
			else
			{
				std::string message;
				message.append(robedPlayer.getName());
				message.append(" does not have access to this hexagon");
				m_iom.displayMessage(message);
			}

			break;
		}
		catch (const std::string&)
		{
			m_iom.displayMessage("You have to choose a player because you moved the robber");
			continue;
		}
	}
}

bool Game::shouldAnyoneDiscard()
{
	for (Player& player : m_players)
	{
		int numberOfResources = player.getLumbers().size() +
			player.getBricks().size() +
			player.getWools().size() +
			player.getGrains().size() +
			player.getOres().size();

		if (numberOfResources > 7)
		{
			//try discard cards until no exception is trown
			createDiscardMessage(player, numberOfResources / 2);

			while (true)
			{
				try
				{
					discardCards(player, numberOfResources / 2);
					break;
				}
				catch (const std::string& message)
				{
					m_iom.displayMessage(message);
					continue;
				}
			}
		}
	}

	return false;
}

void Game::discardCards(Player & player, uint8_t numberOfResources)
{
	auto resourcesToDiscard = m_iom.chooseDiscardedCards(getCurrentPlayer(), player, numberOfResources);
	if (m_cm.canDoThisTrade(player, resourcesToDiscard))
	{
		m_tm.transferResources(resourcesToDiscard, player, m_bank);
		m_iom.updateBoardAndPlayers(m_players, getCurrentPlayer(), m_board);

		createDiscardedResourcesMessage(player, resourcesToDiscard);
	}
}

bool Game::chooseAction()
{
	try
	{
		switch (m_iom.chooseAction())
		{
		case ActionType::build: build();
			return false;
		case ActionType::trade: trade();
			return false;
		case ActionType::turnDevCard: turnDevCard();
			return false;
		case ActionType::buyDevCard: buyDevCard();
			return false;
		case ActionType::endTurn: endTurn();
			return true;
		default:
			return true;
		};
	}
	catch (const std::string& message)
	{
		m_iom.displayMessage(message);
	}
	return false;
}

void Game::build()
{
	BuildingType building = m_iom.chooseBuild();
	if (m_cm.canBuildStructure(getCurrentPlayer(), building))
	{
		switch (building)
		{
		case BuildingType::settlement: buildSettlement();
			break;
		case BuildingType::city: buildCity();
			break;
		case BuildingType::road: buildRoad();
			break;
		default:
			break;
		}
		m_iom.updateBoardAndPlayers(m_players, getCurrentPlayer(), m_board);
	}
}

void Game::buildSettlement()
{
	uint8_t intersectionID = m_iom.chooseIntersection(getPlayers());
	if (m_cm.canBuildAtNode(m_board, intersectionID) &&
		m_cm.canDoThisTrade(getCurrentPlayer(), Rules::getsettlementCost()) &&
		getCurrentPlayer().getSettlements().size() < 5)
	{
		m_tm.transferBuilding(BuildingType::settlement, m_bank, getCurrentPlayer());
		m_tm.transferResources(Rules::getsettlementCost(), getCurrentPlayer(), m_bank);
		getCurrentPlayer().buildSettlement(m_board.findNode(intersectionID));

		createBuildingMessage(getCurrentPlayer(), BuildingType::settlement);
	}
}

void Game::buildCity()
{
	uint8_t intersectionID = m_iom.chooseIntersection(getPlayers());
	if (m_cm.canUpgrade(getCurrentPlayer(), intersectionID) &&
		m_cm.canDoThisTrade(getCurrentPlayer(), Rules::getCityCost()))
	{
		m_tm.transferBuilding(BuildingType::city, m_bank, getCurrentPlayer());
		m_tm.transferResources(Rules::getCityCost(), getCurrentPlayer(), m_bank);
		getCurrentPlayer().buildCity(m_board.findNode(intersectionID));

		createBuildingMessage(getCurrentPlayer(), BuildingType::city);
	}
}

void Game::buildRoad()
{
	uint8_t edgeID = m_iom.chooseEdge(getPlayers());
	if (m_cm.canBuildAtEdge(getCurrentPlayer(), m_board, edgeID) &&
		m_cm.canDoThisTrade(getCurrentPlayer(), Rules::getRoadCost()))
	{
		m_tm.transferBuilding(BuildingType::road, m_bank, getCurrentPlayer());
		m_tm.transferResources(Rules::getRoadCost(), getCurrentPlayer(), m_bank);
		getCurrentPlayer().buildRoad(m_board.findEdge(edgeID));

		createBuildingMessage(getCurrentPlayer(), BuildingType::road);
	}
}

void Game::trade()
{
	TradeType tradeType = m_iom.chooseTrade();
	switch (tradeType)
	{
	case TradeType::fourToOne: tradeFourToOne();
		break;
	case TradeType::port: tradeAtPort();
		break;
	case TradeType::customTrade: tradeWithPlayers();
		break;
	default:
		break;
	}
	m_iom.updateCurrentPlayerForm(getCurrentPlayer());
	m_iom.updatePlayersForm(m_players);
}

void Game::tradeFourToOne()
{
	auto resourcesToReceive = m_iom.chooseResourcesToReceive();
	auto resourcesToTrade = m_iom.chooseResourcesToTrade();

	if (m_cm.canDoThisTrade(getCurrentPlayer(), resourcesToTrade) &&	//can player afford
		m_cm.canDoThisTrade(m_bank, resourcesToReceive) &&				//can bank afford
		m_cm.canTrade(resourcesToReceive, resourcesToTrade, 4))	//is the exchange correct
	{
		m_tm.transferResources(resourcesToReceive, m_bank, getCurrentPlayer());
		m_tm.transferResources(resourcesToTrade, getCurrentPlayer(), m_bank);

		createTradeMessage(getCurrentPlayer(), resourcesToTrade, resourcesToReceive);
	}
}

void Game::tradeAtPort()
{
	PortType portType = m_iom.choosePortType();
	switch (portType)
	{
	case PortType::genericPort: tradeThreeToOne();
		break;
	case PortType::brickPort: tradeTwoToOne(ResourceType::brick);
		break;
	case PortType::lumberPort: tradeTwoToOne(ResourceType::lumber);
		break;
	case PortType::orePort: tradeTwoToOne(ResourceType::ore);
		break;
	case PortType::woolPort: tradeTwoToOne(ResourceType::wool);
		break;
	case PortType::grainPort: tradeTwoToOne(ResourceType::grain);
		break;
	default: tradeThreeToOne();
		break;
	}
}

void Game::tradeThreeToOne()
{
	auto resourcesToReceive = m_iom.chooseResourcesToReceive();
	auto resourcesToTrade = m_iom.chooseResourcesToTrade();

	if (m_cm.canDoThisTrade(getCurrentPlayer(), resourcesToTrade) &&	//can player afford
		m_cm.canDoThisTrade(m_bank, resourcesToReceive) &&				//can bank afford
		m_cm.canTrade(resourcesToReceive, resourcesToTrade, 3) &&	//is the exchange correct
		m_cm.hasAccesToPort(getCurrentPlayer(), PortType::genericPort))	//has acces to this specific port
	{
		m_tm.transferResources(resourcesToReceive, m_bank, getCurrentPlayer());
		m_tm.transferResources(resourcesToTrade, getCurrentPlayer(), m_bank);

		createTradeMessage(getCurrentPlayer(), resourcesToTrade, resourcesToReceive);
	}
}

void Game::tradeTwoToOne(ResourceType resourceType)
{
	auto resourcesToReceive = m_iom.chooseResourcesToReceive();
	auto resourcesToTrade = m_iom.chooseResourcesToTrade();

	if (m_cm.canDoThisTrade(getCurrentPlayer(), resourcesToTrade) &&	//can player afford
		m_cm.canDoThisTrade(m_bank, resourcesToReceive) &&				//can bank afford
		m_cm.canTrade(resourcesToReceive, resourcesToTrade, resourceType, 2) &&	//is the exchange correct
		m_cm.hasAccesToPort(getCurrentPlayer(), resourceToPort(resourceType)))	//has access to this specific port
	{
		m_tm.transferResources(resourcesToReceive, m_bank, getCurrentPlayer());
		m_tm.transferResources(resourcesToTrade, getCurrentPlayer(), m_bank);

		createTradeMessage(getCurrentPlayer(), resourcesToTrade, resourcesToReceive);
	}
}

PortType Game::resourceToPort(ResourceType resourceType)
{
	switch (resourceType)
	{
	case ResourceType::lumber: return PortType::lumberPort;
	case ResourceType::brick:return PortType::brickPort;
	case ResourceType::wool:return PortType::woolPort;
	case ResourceType::grain:return PortType::grainPort;
	case ResourceType::ore:return PortType::orePort;
	default: return PortType::lumberPort;
	}
}

void Game::tradeWithPlayers()
{
	auto resourcesToReceive = m_iom.chooseResourcesToReceive();
	auto resourcesToTrade = m_iom.chooseResourcesToTrade();
	const Player& playerWhoAgrees = m_iom.choosePlayer(m_players, getCurrentPlayer());

	if (m_cm.canDoThisTrade(getCurrentPlayer(), resourcesToTrade) &&	//can player afford
		m_cm.canDoThisTrade(playerWhoAgrees, resourcesToReceive))		//other player can afford
	{
		m_tm.transferResources(resourcesToReceive, const_cast<Player&>(playerWhoAgrees), getCurrentPlayer());
		m_tm.transferResources(resourcesToTrade, getCurrentPlayer(), const_cast<Player&>(playerWhoAgrees));

		createTradeMessage(getCurrentPlayer(), playerWhoAgrees, resourcesToTrade, resourcesToReceive);
	}
}

void Game::devCardsCanBeUsed()
{
	for (Player& player : m_players)
	{
		m_tm.DevCardCanBeUsed(player);
	}
}

void Game::turnDevCard()
{
	DevCardType devCardType = m_iom.chooseTurnDevCard();
	if (m_cm.canTurnDevelopementCard(getCurrentPlayer(), devCardType))
	{
		switch (devCardType)
		{
		case DevCardType::knite: turnKnite();
			break;
		case DevCardType::monopoly: turnMonopoly();
			break;
		case DevCardType::yearOfPlenty: turnYearOfPlenty();
			break;
		case DevCardType::roadBuilding: turnRoadBuilding();
			break;
		default:
			break;
		}
		m_iom.updateBoardAndPlayers(m_players, getCurrentPlayer(), m_board);
	}
}

void Game::turnKnite()
{
	if (m_cm.canTurnDevelopementCard(getCurrentPlayer(), DevCardType::knite))
	{
		moveRobber();
		getCurrentPlayer().turnDevCard(DevCardType::knite);

		createTurnDevCardMessage(getCurrentPlayer(), DevCardType::knite);
	}
	m_iom.updateBoardAndPlayers(m_players, getCurrentPlayer(), m_board);
}

void Game::turnMonopoly()
{
	auto resource = m_iom.chooseResourceToMonopolize();
	m_tm.transferMonopolyResource(resource, const_cast<std::vector<Player>&>(getPlayers()), getCurrentPlayer());
	getCurrentPlayer().turnDevCard(DevCardType::monopoly);

	createTurnDevCardMessage(getCurrentPlayer(), DevCardType::monopoly);
}

void Game::turnRoadBuilding()
{
	//build two roads without paying
	placeFreeRoad(getCurrentPlayer());
	placeFreeRoad(getCurrentPlayer());
	getCurrentPlayer().turnDevCard(DevCardType::roadBuilding);

	createTurnDevCardMessage(getCurrentPlayer(), DevCardType::roadBuilding);
}

void Game::turnYearOfPlenty()
{
	auto resources = m_iom.chooseResourcesToReceive();
	if (m_cm.canDoThisTrade(m_bank, resources) &&
		resources.size() == 2)
	{
		m_tm.transferResources(resources, m_bank, getCurrentPlayer());
		getCurrentPlayer().turnDevCard(DevCardType::yearOfPlenty);

		createTurnDevCardMessage(getCurrentPlayer(), DevCardType::yearOfPlenty);
	}
}

void Game::buyDevCard()
{
	if (m_cm.canDoThisTrade(getCurrentPlayer(), Rules::getDevCardCost()) &&	//has the player enough resources?
		m_cm.canDoThisTrade(m_bank, DevCardType::knite))		//has the bank enough devCards?
	{
		m_tm.transferDevCard(m_bank, getCurrentPlayer());
		m_tm.transferResources(Rules::getDevCardCost(), getCurrentPlayer(), m_bank);
		m_iom.updateCurrentPlayerForm(getCurrentPlayer());

		createBuyDevCardMessage(getCurrentPlayer());
	}
}

void Game::endTurn()
{
	devCardsCanBeUsed();
	calculateLargestArmy();
	calculateLongestRoad();
	getCurrentPlayer().resetTurnedDevCard();
	createEndTurnMessage(getCurrentPlayer());
}

std::pair<uint8_t, uint8_t> Game::getDice() const
{
	return m_currentDice;
}

const std::vector<Player>& Game::getPlayers() const
{
	return m_players;
}

Player& Game::getCurrentPlayer()
{
	return m_players[m_turns % m_players.size()];
}


void Game::calculateLongestRoad()
{
	//find longest road owner
	auto it_longestRoadOwner = std::find_if(m_players.begin(), m_players.end(),
		[](Player& player) {return player.hasLongestRoad(); });

	//set roads lenth for all players
	for (auto& player : m_players)
	{
		player.setLongestRoadLength(m_cm.getMaximumRoadLength(player, m_board));
	}
	//find the player with the maximum road length
	auto const& it_longestRoadCandidate = std::max_element(
		m_players.begin(), m_players.end(),
		[](Player const& lhs, Player const& rhs)
	{
		return lhs.getLongestRoadLength() < rhs.getLongestRoadLength();
	}
	);
	//if the candidate has a longest road than the owner, change the owner
	if (it_longestRoadOwner != m_players.end())
	{
		if (it_longestRoadCandidate->getLongestRoadLength() > it_longestRoadOwner->getLongestRoadLength())
		{
			it_longestRoadOwner->setLongestRoad(false);
			it_longestRoadCandidate->setLongestRoad(true);
		}
	}
	//else if there is no previous owner, candidate becomes new owner
	else if (it_longestRoadCandidate->getLongestRoadLength() >= 5)
	{
		it_longestRoadCandidate->setLongestRoad(true);
	}
}

void Game::createTradeMessage(const Player & currentPlayer, 
	const Player & playerWhoAgrees, 
	const std::vector<ResourceType>& tradedResources, 
	const std::vector<ResourceType>& receivedResources)
{
	std::string message;
	message.append(currentPlayer.getName());
	message.append(" exchanged ");
	vectorOfResourcesToString(message, tradedResources);
	message.append("for ");
	vectorOfResourcesToString(message, receivedResources);
	message.append("with ");
	message.append(playerWhoAgrees.getName());
	m_iom.displayMessage(message);
}

void Game::createTradeMessage(const Player & currentPlayer, const std::vector<ResourceType>& tradedResources, const std::vector<ResourceType>& receivedResources)
{
	std::string message;
	message.append(currentPlayer.getName());
	message.append(" exchanged ");
	vectorOfResourcesToString(message, tradedResources);
	message.append(" for ");
	vectorOfResourcesToString(message, receivedResources);
	m_iom.displayMessage(message);
}

void Game::createTurnDevCardMessage(const Player & currentPlayer, const DevCardType type)
{
	std::string message;
	message.append(currentPlayer.getName());
	message.append(" turned ");
	message.append(Rules::getMapedValue(type));
	m_iom.displayMessage(message);
}

void Game::createBuildingMessage(const Player & currentPlayer, const BuildingType type)
{
	std::string message;
	message.append(currentPlayer.getName());
	message.append(" built ");
	message.append(Rules::getMapedValue(type));
	m_iom.displayMessage(message);
}

void Game::createDiscardMessage(const Player & player, uint8_t numberOfResources)
{
	std::string message;
	message.append(player.getName());
	message.append(", discard ");
	message.append(std::to_string(numberOfResources));
	message.append(" resources");
	m_iom.displayMessage(message);
}

void Game::createDiscardedResourcesMessage(const Player & player, std::vector<ResourceType> resources)
{
	std::string message;
	message.append(player.getName());
	message.append(" discarded ");
	vectorOfResourcesToString(message, resources);
	m_iom.displayMessage(message);
}

void Game::createRobPlayerMessage(const Player & robingPlayer, const Player & robedPlayer)
{
	std::string message;
	message.append(robingPlayer.getName());
	message.append(" robed ");
	message.append(robedPlayer.getName());
	m_iom.displayMessage(message);
}

void Game::createBuyDevCardMessage(const Player & player)
{
	std::string message;
	message.append(player.getName());
	message.append(" buy a developement card");
	m_iom.displayMessage(message);
}

void Game::createEndTurnMessage(const Player & player)
{
	std::string message;
	message.append(player.getName());
	message.append(" ended the turn");
	m_iom.displayMessage(message);
}

void Game::vectorOfResourcesToString(std::string & message, std::vector<ResourceType> resources)
{
	for (auto& resource : resources)
	{
		message.append(Rules::getMapedValue(resource));
		message.append(" ");
	}
}
