#include "VictoryPoint.h"

uint8_t VictoryPoint::s_instancesCreated = 0;

VictoryPoint::VictoryPoint()
{
	m_type = DevCardType::victoryPoint;
	m_instancesAllowed = Rules::getNumOfVictoryPointCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

VictoryPoint::~VictoryPoint()
{
}

void VictoryPoint::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}