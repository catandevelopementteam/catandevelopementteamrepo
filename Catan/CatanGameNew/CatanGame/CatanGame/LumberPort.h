#pragma once
#include "IPort.h"


class LumberPort :public IPort
{
private:
	PortType m_type;
	LumberPort();
	~LumberPort();
public:
	static LumberPort& getInstance();
	std::string typeName() const;
	PortType getType() const;
	LumberPort(LumberPort const&) = delete;
	void operator=(LumberPort const&) = delete;


};