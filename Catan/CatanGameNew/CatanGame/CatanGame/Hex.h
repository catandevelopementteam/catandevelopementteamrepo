#pragma once

#include "Node.h"
#include "ResourceType.h"
#include<iostream>
#include<vector>
#include<algorithm>
#include<memory>

/*
					Class Hex
	represents the hexes from the Catan Board	
*/
class Hex
{
public:
	using node_ptr = std::shared_ptr<Node>;
		
private:
	ResourceType m_type; //resource type	
	uint8_t m_color; //hex color
	int m_id; //hex id (from 0 to 18) - fixed
	int m_assignedNumber; //the DiceNumber - randomly assigned
	bool m_hasRobber; //is the robber placed on this hex?
	std::vector<node_ptr> m_hexIntersections; //vector of 6 surrounding nodes (hex corners)
	
public:
	Hex();
	Hex(int m_id);
	Hex(const Hex& other);
	Hex& operator = (const Hex& other);

	//method used to set hex's nedes when initializing it 
	void addNode(node_ptr inter);

	//find an intersection using it's id
	node_ptr getNode(int id) const;

	//returns all the surrounding nodes
	std::vector<node_ptr> getHexNodes() const;

	//optional: method that prints all the Intersections belonging to this hex
	void printNodes() const;

	//set&get AssignedNumber
	void setAssignedNumber(int assignedNumber);
	int getAssignedNumber() const;

	//set&get ResourceType
	void setResourceType(ResourceType type);
	ResourceType getResourceType() const;

	void setColor(ResourceType type);

	int getId() const;

	bool hasRobber() const; // returns m_hasRobber
	void placeRobber(); //makes m_robber = true;
	void removeRobber(); //makes m_robber = false;
	int getColor() const;

	~Hex();
};

