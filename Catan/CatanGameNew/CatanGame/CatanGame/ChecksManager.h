#pragma once

#include <vector>

#include "ResourceType.h"
#include "BuildingType.h"
#include "DevCardType.h"
#include "PortType.h"
#include "TradeType.h"
#include "Player.h"
#include "Board.h"
#include "Bank.h"

class ChecksManager
{
private:
	using node_ptr = std::shared_ptr<Node>;
	using edge_ptr = std::shared_ptr<Edge>;
	using settlement_ptr = std::unique_ptr<Settlement>;
	using resource_list = std::vector<ResourceType>;

public:	
	ChecksManager();
	~ChecksManager();

	bool canTurnDevelopementCard(const Player& player, DevCardType type) const;

	//method that verifies the player's neccessary resources to build a settlement/city/road
	bool canBuildStructure(const Player& player, BuildingType type) const;

	//method that verifies if the node and all neighbours of the node are vacant
	bool canBuildAtNode(const Board& board, int id) const; 

	//method that verifies if the new road connects to one of player's existing roads, settlements, cities
	bool canBuildAtEdge(const Player& someone, const Board& board, int id1, int id2) const; 
	bool canBuildAtEdge(const Player& someone, const Board& board, int edgeId) const; 

	//method that verifies if the player has an existing settlement at the node that he wants to upgrade
	bool canUpgrade(Player& someone, int id) const; 

	bool canDoThisTrade(const Player& player, const resource_list& resources) const;
	bool canDoThisTrade(const Bank& bank, const resource_list& resources) const;
	bool canDoThisTrade(const Bank& bank, const BuildingType buildingType) const;
	bool canDoThisTrade(const Bank& bank, const DevCardType devCardType) const;
	bool hasAccesToPort(const Player& player, PortType portType) const;

	//checks if you exchange 4 of a kind to 1
	bool canTrade(
		const resource_list& buyer,
		const resource_list& seller,
		std::size_t count
		) const;
	bool canTrade(
		const resource_list& buyer,
		const resource_list& seller,
		const ResourceType resourceType,
		std::size_t count
	) const;

private:
	bool areResourcesTheSame(const resource_list& resources) const;

	template<class ResourceHolder>
	bool canAfford(const ResourceHolder& resourceHolder, const resource_list& resources) const
	{
		uint8_t lumbers = 0;
		uint8_t bricks = 0;
		uint8_t wools = 0;
		uint8_t grains = 0;
		uint8_t ores = 0;

		for (const ResourceType& resource : resources)
		{
			switch (resource)
			{
			case ResourceType::lumber: ++lumbers;
				break;
			case ResourceType::brick: ++bricks;
				break;
			case ResourceType::wool: ++wools;
				break;
			case ResourceType::grain: ++grains;
				break;
			case ResourceType::ore: ++ores;
				break;
			default:
				break;
			}
		}

		return ((resourceHolder.getLumbers().size() >= lumbers) &&
			(resourceHolder.getBricks().size() >= bricks) &&
			(resourceHolder.getWools().size() >= wools) &&
			(resourceHolder.getGrains().size() >= grains) &&
			(resourceHolder.getOres().size() >= ores));
	}
public:
	//method that returns the value of longest road for a given player	
	int getMaximumRoadLength(const Player& someone, Board& board) const;
};

