#include "OptionsForm.h"



OptionsForm::OptionsForm()
{
}

OptionsForm::OptionsForm(std::string defaultBodyPath) : IForm(defaultBodyPath)
{
	m_totalLinesShown = 0;
	InitializePositions(defaultBodyPath);
	m_totalFormLines = m_positions.size();
	InitializeMessages();
}


OptionsForm::~OptionsForm()
{
}

void OptionsForm::updateForm()
{
	for (uint8_t i = 0; i < m_totalFormLines; i++)
	{
		//for each position, starting with the upper one, take the oldest message and write it to the body of the form
		changeText(m_positions[i], m_messages[(m_totalLinesShown + i) % m_totalFormLines]);
	}
}

void OptionsForm::addMessages(std::vector<std::string>& messages)
{
	int lines = messages.size();

	for (uint8_t i = 0; i < lines; i++)
	{
		std::string currentLine = messages[i];
		while (true)
		{
			std::string firstChunk = currentLine.substr(0, m_positions[0].getCapacity());

			//replace the oldest message with the new one
			m_messages[m_totalLinesShown % m_totalFormLines] = firstChunk;

			//increment total messages shown by this form to know next time wich is the oldest message
			m_totalLinesShown += 1;

			//if the current line is bigger than options form width, split it
			if (currentLine.size() > m_positions[0].getCapacity())
			{
				currentLine = currentLine.substr(m_positions[0].getCapacity());
				continue;
			}
			break;
		}

	}

	//add one more empty line
	m_messages[m_totalLinesShown % m_totalFormLines] = "";
	m_totalLinesShown += 1;

	//update the form according to the new messages
	updateForm();
}

void OptionsForm::InitializePositions(std::string defaultBodyPath)
{
	getSinglePositions(m_positions, defaultBodyPath, '#');
}

void OptionsForm::InitializeMessages()
{
	m_messages.reserve(m_totalFormLines);
	for (uint8_t i = 0; i < m_totalFormLines; i++)
	{
		m_messages.push_back("");
	}
}

