#include "Ore.h"

uint8_t Ore::s_instancesCreated = 0;

Ore::Ore()
{
	m_type = ResourceType::ore;
	m_color = Rules::getConsoleColorOfOre();
	m_instancesAllowed = Rules::getNumOfOreCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

Ore::~Ore()
{
}

void Ore::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}