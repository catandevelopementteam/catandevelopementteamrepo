#include "ChecksManager.h"



ChecksManager::ChecksManager()
{
}


ChecksManager::~ChecksManager()
{
}

bool ChecksManager::canTurnDevelopementCard(const Player & player, DevCardType type) const
{
	bool canTurn = player.canTurnDevCard() && player.getNumberOfSpecificHiddenDevCard(type) != 0;
	if (canTurn) { return true; }

	std::string message = player.getName() + " can't turn " + Rules::getMapedValue(type);
	throw std::string(message);
}

bool ChecksManager::canBuildStructure(const Player & player, BuildingType type) const
{
	bool canBuild = false;
	switch (type)
	{
	case BuildingType::settlement: canBuild = player.canBuildSettlement(); break;
	case BuildingType::city: canBuild = player.canBuildCity(); break;
	case BuildingType::road: canBuild = player.canBuildRoad(); break;
	default:
		canBuild = false;
	}

	if (canBuild) { return true; }

	std::string message = player.getName() + " can't build " + Rules::getMapedValue(type);
	throw std::string(message);
}

bool ChecksManager::canBuildAtNode(const Board& board, int id) const
{
	bool verify = true;
	std::vector<ChecksManager::node_ptr> neighbours = board.getNeighbours(id);
	node_ptr node = board.findNode(id);
	if (node->getOwnerId() != 9)
	{
		verify = false;
	}
	else
	{
		for (auto i : neighbours)
		{
			if (i->getOwnerId() != 9)
			{
				verify = false;
			}
		}
	}
	if (verify) { return true; }

	std::string message;
	message.append("Can't build at intersection ");
	message.append(std::to_string(id));
	throw std::string(message);
}

bool ChecksManager::canBuildAtEdge(const Player& someone, const Board & board, int id1, int id2) const
{
	bool verify = false;
	auto edge = board.findEdge(id1, id2);
	//if edge is vacant	
	if (edge->getOwnerId() == 9)
	{
		//verifies if the new road connects to player's existing settlements/cities
		auto pairOfNodes = edge->getPairOfNodes();
		std::vector<node_ptr> nodes;
		nodes.emplace_back(pairOfNodes.first);
		nodes.emplace_back(pairOfNodes.second);
		for (auto node = nodes.begin(); node != nodes.end(); ++node)
		{
			if ((*node)->getOwnerId() == someone.getId())
			{
				verify = true;
			}
			//verifies if the new road connects to player's existing roads
			else if ((*node)->getOwnerId() == 9)
			{
				std::vector<edge_ptr> edges = board.getIncidentEdges(*node);
				for (auto e = edges.begin(); e != edges.end(); ++e)
				{
					if ((*e)->getOwnerId() == someone.getId())
					{
						verify = true;
					}
				}
			}
		}
	}
	if (verify) { return true; }

	std::string message = "Can't build at this edge";
	throw std::string(message);
}

bool ChecksManager::canBuildAtEdge(const Player & someone, const Board & board, int edgeId) const
{
	bool verify = false;
	auto edge = board.findEdge(edgeId);
	//if edge is vacant	
	if (edge->getOwnerId() == 9)
	{
		//verifies if the new road connects to player's existing settlements/cities
		auto pairOfNodes = edge->getPairOfNodes();
		std::vector<node_ptr> nodes;
		nodes.emplace_back(pairOfNodes.first);
		nodes.emplace_back(pairOfNodes.second);
		for (auto node = nodes.begin(); node != nodes.end(); ++node)
		{
			if ((*node)->getOwnerId() == someone.getId())
			{
				verify = true;
			}
			//verifies if the new road connects to player's existing roads
			else if ((*node)->getOwnerId() == 9)
			{
				std::vector<edge_ptr> edges = board.getIncidentEdges(*node);
				for (auto e = edges.begin(); e != edges.end(); ++e)
				{
					if ((*e)->getOwnerId() == someone.getId())
					{
						verify = true;
					}
				}
			}
		}
	}
	if (verify) { return true; }

	std::string message = "Can't build at this edge";
	throw std::string(message);
}

bool ChecksManager::canUpgrade(Player & someone, int nodeId) const
{
	bool verify = false;
	node_ptr i;
	auto const& it = std::find_if(
		someone.getSettlements().begin(),
		someone.getSettlements().end(),
		[nodeId](const settlement_ptr& sett)
	{
		return (sett->getNode())->getId() == nodeId;
	}
	);

	if (it != someone.getSettlements().end()) verify = true;

	if (verify) { return true; }

	std::string message = "Can't upgrage";
	throw std::string(message);
}


bool ChecksManager::canDoThisTrade(const Player& player, const resource_list& resources) const
{
	if (canAfford(player, resources)){ return true;	}

	std::string message = player.getName() + " don't have enough resources";
	throw std::string(message);
}

//checks if the bank can afford to supply certain resources
bool ChecksManager::canDoThisTrade(const Bank & bank, const resource_list& resources) const
{
	if (canAfford(bank, resources)) { return true; }

	throw std::string("Bank don't have enough resources");
}

bool ChecksManager::canDoThisTrade(const Bank & bank, const BuildingType buildingType) const
{
	bool canAfford;
	//bank has enough settlements/cities/roads ?
	switch (buildingType)
	{
	case BuildingType::settlement: canAfford = bank.getSettlements().size() > 0;
		break;
	case BuildingType::city: canAfford = bank.getCities().size() > 0;
		break;
	case BuildingType::road: canAfford = bank.getRoads().size() > 0;
		break;
	default: return false;
	}

	if (canAfford) { return true; }

	std::string message = "Bank don't have enough " + Rules::getMapedValue(buildingType);
	throw std::string(message);
}

bool ChecksManager::canDoThisTrade(const Bank & bank, const DevCardType /*devCardType*/) const
{
	if (!bank.isEmptyDevCardStack()) { return true; }

	throw std::string("Bank don't have enough developement cards");
}

bool ChecksManager::hasAccesToPort(const Player & player, PortType portType) const
{
	using settlement_ptr = std::unique_ptr<Settlement>;
	using city_ptr = std::unique_ptr<City>;
	bool verify = false;
	IResourceHolder::settlements_type const& settlements = player.getSettlements();
	auto port = std::find_if(
		settlements.begin(), 
		settlements.end(),
		[portType](const settlement_ptr& sett) {
			return (portType == sett->getNode()->getType());
	}
	);

	if (port != player.getSettlements().end())
	{
		verify = true;
	}
	else
	{
		IResourceHolder::cities_type const& cities = player.getCities();
		auto port2 = std::find_if(
			cities.begin(), 
			cities.end(),
			[portType](const city_ptr& city) {
				return (portType == city->getNode()->getType());
		}
		);

		if (port2 != player.getCities().end())
		{
			verify = true;
		}
	}

	if (verify) {
		return true;
	}

	std::string message = player.getName() +
		" dont have access to "
		+ Rules::getMapedValue(portType) +
		" port";

	throw std::string(message);
}

bool ChecksManager::canTrade(
	const resource_list& buyer,
	const resource_list& seller,
	std::size_t count
	) const
{
	bool canTrade = 
		(buyer.size() == 1) &&
		(seller.size() == count) &&
		areResourcesTheSame(seller);

	if (canTrade) { 
		return true; 
	}

	std::string s = "Can't trade ";
	s += std::to_string(count);
	s += " to 1 with these resources";
	throw s;
}

bool ChecksManager::canTrade(
	const resource_list& buyer,
	const resource_list& seller,
	const ResourceType resourceType,
	std::size_t count
) const
{
	bool canTrade =
		(buyer.size() == 1) &&
		(seller.size() == count) &&
		areResourcesTheSame(seller) &&
		(seller[0] == resourceType);

	if (canTrade) 
	{
		return true;
	}

	std::string s = "Can't trade ";
	s += std::to_string(count);
	s += " to 1 with these resources";
	throw s;
}

bool ChecksManager::areResourcesTheSame(const resource_list& resources) const
{
	bool areTheSame = true;
	for (ResourceType const& resource : resources)
	{
		if (resource != resources[0])
		{
			areTheSame = false;
		}
	}
	return areTheSame;
}

int ChecksManager::getMaximumRoadLength(const Player& someone, Board& board) const
{
	int max = 0;
	for (auto const& edge : someone.getEdgesOfRoads())
	{
		int length = board.longestRoad(0, edge, someone.getId());
		if (length > max)
		{
			max = length;
		}
	}
	return max;
}
