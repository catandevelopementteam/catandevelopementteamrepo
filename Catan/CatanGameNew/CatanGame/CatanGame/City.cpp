#include "City.h"

uint8_t City::s_instancesCreated = 0;

City::City()
{
	m_type = BuildingType::city;
	m_instancesAllowed = Rules::getNumOfCities();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

void City::setNode(std::shared_ptr<Node> intersection)
{
	m_node = intersection;
}

std::shared_ptr<Node> City::getNode()
{
	return m_node;
}

City::~City()
{
}

void City::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}
