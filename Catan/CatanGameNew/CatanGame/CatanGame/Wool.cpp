#include "Wool.h"

uint8_t Wool::s_instancesCreated = 0;

Wool::Wool()
{
	m_type = ResourceType::wool;
	m_color = Rules::getConsoleColorOfWool();
	m_instancesAllowed = Rules::getNumOfWoolCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

Wool::~Wool()
{
}

void Wool::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}
