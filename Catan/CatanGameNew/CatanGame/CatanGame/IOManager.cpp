#include "IOManager.h"

IOManager::IOManager()
{
}

IOManager::IOManager(IOManager::VisualisatioType visualisationType, 
	const Board& board, 
	const std::vector<Player>& players, 
	const Player& currentPlayer)
{
	m_logging = std::make_shared<Logging>("../catanLogger.txt");

	switch (visualisationType)
	{
	case IOManager::VisualisatioType::console: m_output = std::make_shared<DrawConsole>(board, players, currentPlayer);
		m_input = std::make_shared<InputConsole>();
		break;
	case IOManager::VisualisatioType::window:;
		break;
	default:
		break;
	}
}

DevCardType IOManager::chooseTurnDevCard()
{
	m_output->chooseTurnDevCard();
	m_output->draw();
	return m_input->chooseTurnDevCard();
}

bool IOManager::chooseTurnKnite()
{
	m_output->chooseTurnKnite();
	m_output->draw();
	return m_input->chooseTurnKnite();
}

uint8_t IOManager::chooseHex(const Board& board)
{
	m_output->highlightHexagons();
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	m_output->chooseHex();
	m_output->draw();
	uint8_t hexID = m_input->chooseHex();
	m_output->unhighlightHexagons(board);
	return hexID;
}

const Player& IOManager::choosePlayer(const std::vector<Player>& players, const Player& currentPlayer)
{
	m_output->choosePlayer(players, currentPlayer);
	m_output->draw();
	return m_input->choosePlayer(players, currentPlayer);
}

std::vector<ResourceType> IOManager::chooseDiscardedCards(const Player& currentPlayer, 
	const Player& temporaryCurrentPlayer, 
	uint8_t totalNumber)
{
	m_output->updateCurrentPlayer(temporaryCurrentPlayer);	//show the right player in currentPlayerForm
	m_output->chooseDiscardedCards(totalNumber);
	m_output->draw();
	std::vector<ResourceType> discardedResources = m_input->chooseDiscardedCards(totalNumber);
	m_output->updateCurrentPlayer(currentPlayer);	//show back the currentPlayerForm
	return discardedResources;
}

ActionType IOManager::chooseAction()
{
	m_output->chooseAction();
	m_output->draw();
	return m_input->chooseAction();
}

BuildingType IOManager::chooseBuild()
{
	m_output->chooseBuild();
	m_output->draw();
	return m_input->chooseBuild();
}

uint8_t IOManager::chooseIntersection(const std::vector<Player>& players)
{
	m_output->highlightIntersections();
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	m_output->chooseIntersection();
	m_output->draw();
	uint8_t intersectionID = m_input->chooseIntersection();
	m_output->unhighlightIntersections(players);
	return intersectionID;
}

uint8_t IOManager::chooseEdge(const std::vector<Player>& players)
{
	m_output->highlightEdges();
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	m_output->chooseEdge();
	m_output->draw();
	uint8_t edgeID = m_input->chooseEdge();
	m_output->unhighlightEdges(players);
	return edgeID;
}

TradeType IOManager::chooseTrade()
{
	m_output->chooseTrade();
	m_output->draw();
	return m_input->chooseTrade();
}

PortType IOManager::choosePortType()
{
	m_output->choosePortType();
	m_output->draw();
	return m_input->choosePortType();
}

std::vector<ResourceType> IOManager::chooseResourcesToReceive()
{
	m_output->chooseResourcesToReceive();
	m_output->draw();
	return m_input->chooseResourcesToReceive();
}

std::vector<ResourceType> IOManager::chooseResourcesToTrade()
{
	m_output->chooseResourcesToTrade();
	m_output->draw();
	return m_input->chooseResourcesToTrade();
}

ResourceType IOManager::chooseResourceToMonopolize()
{
	m_output->chooseResourceToMonopolize();
	m_output->draw();
	return m_input->chooseResourceToMonopolize();
}

void IOManager::displayMessage(const std::string message)
{
	m_output->displayMessage(message);
	m_logging->log(message);
}

void IOManager::updatePlayersForm(const std::vector<Player>& players)
{
	m_output->updatePlayersForm(players);
}

void IOManager::updateCurrentPlayerForm(const Player& player)
{
	m_output->updateCurrentPlayer(player);
}

void IOManager::updateBoardForm(const std::vector<Player>& players, const Board& board)
{
	m_output->updateBoardForm(players, board);
}

void IOManager::updateDiceForm(const std::pair<uint8_t, uint8_t> dice)
{
	m_output->updateDiceForm(dice);
}

void IOManager::updateBoardAndPlayers(const std::vector<Player>& players, const Player & currentPlayer, const Board& board)
{
	m_output->updateBoardAndPlayers(players, currentPlayer, board);
}

IOManager::~IOManager()
{
}
