#include "PlayerForm.h"


PlayerForm::PlayerForm(std::string defaultBodyPath) : IForm(defaultBodyPath)
{

	initPositions(defaultBodyPath);
	initColorsPositions();
}

PlayerForm::~PlayerForm()
{
}

#include "Player.h"
void PlayerForm::updateForm(const Player& player)
{
	setName(player);
	setResources(player);
	setDevCards(player);
	setTurnedDevelopementCards(player);
	setColor(player);
}

void PlayerForm::initPositions(const std::string defaultBodyPath)
{
	std::vector<IForm::TextPosition> positions;
	getSinglePositions(positions, defaultBodyPath, '#');
	m_name = positions[0];
	m_resources = positions[1];
	m_devCards = positions[2];
	m_turnedDevelopementCards = positions[3];
}

void PlayerForm::initColorsPositions()
{
	m_colorPositions.emplace_back(IForm::TextPosition(1, 1, 31));
	m_colorPositions.emplace_back(IForm::TextPosition(2, 1, 31));
	m_colorPositions.emplace_back(IForm::TextPosition(3, 1, 31));
	m_colorPositions.emplace_back(IForm::TextPosition(4, 1, 31));
	m_colorPositions.emplace_back(IForm::TextPosition(5, 1, 31));
	m_colorPositions.emplace_back(IForm::TextPosition(6, 1, 31));
}

void PlayerForm::setName(const Player& player)
{
	changeText(m_name, player.getName());
}

void PlayerForm::setResources(const Player& player)
{
	std::string numberOfResources = std::to_string(
		player.getLumbers().size() +
		player.getBricks().size() +
		player.getWools().size() +
		player.getGrains().size() +
		player.getOres().size());

	changeText(m_resources, numberOfResources);
}

void PlayerForm::setDevCards(const Player& player)
{
	//calculate the total number of developement cards
	std::string numberOfDevCards = std::to_string(
		player.getNumberOfSpecificVisibleDevCard(DevCardType::knite) +
		player.getNumberOfSpecificVisibleDevCard(DevCardType::monopoly) +
		player.getNumberOfSpecificVisibleDevCard(DevCardType::roadBuilding) +
		player.getNumberOfSpecificVisibleDevCard(DevCardType::victoryPoint) +
		player.getNumberOfSpecificVisibleDevCard(DevCardType::yearOfPlenty));

	changeText(m_devCards, numberOfDevCards);
}

void PlayerForm::setTurnedDevelopementCards(const Player& player)
{
	std::string turnedDevCardsSymbols;

	//create the array with the symbols of the developement cards
	turnedDevCardsSymbols.append(player.getNumberOfSpecificVisibleDevCard(DevCardType::knite), 'K');
	turnedDevCardsSymbols.append(player.getNumberOfSpecificVisibleDevCard(DevCardType::monopoly), 'M');
	turnedDevCardsSymbols.append(player.getNumberOfSpecificVisibleDevCard(DevCardType::yearOfPlenty), 'Y');
	turnedDevCardsSymbols.append(player.getNumberOfSpecificVisibleDevCard(DevCardType::roadBuilding), 'R');
	turnedDevCardsSymbols.append(player.getNumberOfSpecificVisibleDevCard(DevCardType::victoryPoint), 'V');

	changeText(m_turnedDevelopementCards, turnedDevCardsSymbols);
}

void PlayerForm::setColor(const Player& player)
{
	for (auto& position : m_colorPositions)
	{
		changeColor(position, player.getColor());
	}
}
