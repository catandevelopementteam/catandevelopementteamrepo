#pragma once
#include "IPort.h"


class GenericPort:public IPort
{
private:
	PortType m_type;
	GenericPort();
	~GenericPort();
public:
	static GenericPort& getInstance();	
	std::string typeName() const;
	PortType getType() const;
	GenericPort(GenericPort const&) = delete;
	void operator=(GenericPort const&) = delete;

	
};

