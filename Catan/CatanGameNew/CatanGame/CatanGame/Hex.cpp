#include "Hex.h"




Hex::Hex()
{
}

Hex::Hex(int id)
{
	m_id = id;
	m_hexIntersections.reserve(6);
	m_hasRobber = false;	
}

Hex & Hex::operator=(const Hex & other)
{
	m_id = other.m_id;
	m_type = other.m_type;
	m_assignedNumber = other.m_assignedNumber;
	m_hasRobber = other.m_hasRobber;
	m_hexIntersections = other.m_hexIntersections;
	m_color = other.m_color;
	return *this;

}
Hex::Hex(const Hex & other)
{
	*this = other;
}

void Hex::addNode(node_ptr inter)
{
	
	m_hexIntersections.push_back(inter);
}

Hex::node_ptr Hex::getNode(int id) const
{
	node_ptr i;
	auto it = std::find_if(
		m_hexIntersections.begin(),
		m_hexIntersections.end(),
		[id](const node_ptr& intersec)
	{
		return intersec->getId() == id;
	}
	);
	if (it != m_hexIntersections.end())
	{
		i = (*it);
	}

	return i;
}

std::vector<Hex::node_ptr> Hex::getHexNodes() const
{
	return m_hexIntersections;
}

void Hex::printNodes() const
{
	std::cout << "hex no. " << m_id << "has the following intersections: ";
	for (auto it = m_hexIntersections.begin(); it != m_hexIntersections.end(); it++)
	{
		std::cout << (*it)->getId() << " ";
	}
	std::cout << std::endl;
}

void Hex::setAssignedNumber(int assignedNumber)
{
	m_assignedNumber = assignedNumber;
}

int Hex::getAssignedNumber() const
{
	return m_assignedNumber;
}

void Hex::setResourceType(ResourceType type)
{
	m_type = type;
}

ResourceType Hex::getResourceType() const
{
	return m_type;
}

void Hex::setColor(ResourceType type)
{
	switch (type)
	{
	case ResourceType::brick: m_color = Rules::getConsoleColorOfBrick(); break;
	case ResourceType::lumber: m_color = Rules::getConsoleColorOfLumber(); break;
	case ResourceType::ore: m_color = Rules::getConsoleColorOfOre(); break;
	case ResourceType::wool: m_color = Rules::getConsoleColorOfWool(); break;
	case ResourceType::grain: m_color = Rules::getConsoleColorOfGrain(); break;
	case ResourceType::none: m_color = Rules::getConsoleColorOfDesert(); break;
	}
}

int Hex::getId() const
{
	return m_id;
}

bool Hex::hasRobber() const
{
	return m_hasRobber;
}

void Hex::placeRobber()
{
	m_hasRobber = true;
}

void Hex::removeRobber()
{
	m_hasRobber = false;
}

int Hex::getColor() const
{
	return m_color;
}


Hex::~Hex()
{
}
