#include "Rules.h"



Rules::Rules()
{
}


Rules::~Rules()
{
}

uint8_t Rules::s_numOfSettlements = 20;
uint8_t Rules::s_numOfCities = 16;
uint8_t Rules::s_numOfRoads = 60;
			   
uint8_t Rules::s_numOfHex = 19;
uint8_t Rules::s_numOfIntersections = 54;
			   
uint8_t Rules::s_numOfDesertHex = 1;
uint8_t Rules::s_numOfLumberHex = 4;
uint8_t Rules::s_numOfBrickHex = 3;
uint8_t Rules::s_numOfWoolHex = 4;
uint8_t Rules::s_numOfGrainsHex = 4;
uint8_t Rules::s_numOfOreHex = 3;
			   
uint8_t Rules::s_numOfLumberCards = 24;
uint8_t Rules::s_numOfBrickCards = 24;
uint8_t Rules::s_numOfWoolCards = 24;
uint8_t Rules::s_numOfGrainCards = 24;
uint8_t Rules::s_numOfOreCards = 24;
			   
uint8_t Rules::s_numOfKniteCards = 14;
uint8_t Rules::s_numOfMonopolyCards = 2;
uint8_t Rules::s_numOfYearOfPlentyCards = 2;
uint8_t Rules::s_numOfVictoryPointCards = 5;
uint8_t Rules::s_numOfRoadBuildingCards = 2;
			   
uint8_t Rules::s_numOfGenericPorts = 4;
uint8_t Rules::s_numOfLumberPorts = 1;
uint8_t Rules::s_numOfBrickPorts = 1;
uint8_t Rules::s_numOfWhoolPorts = 1;
uint8_t Rules::s_numOfGrainPorts = 1;
uint8_t Rules::s_numOfOrePorts = 1;

uint8_t Rules::s_consoleColorPlayer1 = 0x30;
uint8_t Rules::s_consoleColorPlayer2 = 0x40;
uint8_t Rules::s_consoleColorPlayer3 = 0xD0;
uint8_t Rules::s_consoleColorPlayer4 = 0x70;
	
uint8_t Rules::s_consoleColorLumber = 0x60;
uint8_t Rules::s_consoleColorBrick = 0xC0;
uint8_t Rules::s_consoleColorWool = 0xA0;
uint8_t Rules::s_consoleColorGrain = 0xE0;
uint8_t Rules::s_consoleColorOre = 0x80;
uint8_t Rules::s_consoleColorDesert = 0xF0;

std::vector<ResourceType> makeVectorForSettlement()
{
	std::vector<ResourceType> v;
	v.push_back(ResourceType::lumber);
	v.push_back(ResourceType::brick);
	v.push_back(ResourceType::wool);
	v.push_back(ResourceType::grain);

	return v;
}

std::vector<ResourceType> makeVectorForCity()
{
	std::vector<ResourceType> v;
	v.push_back(ResourceType::grain);
	v.push_back(ResourceType::grain);
	v.push_back(ResourceType::grain);
	v.push_back(ResourceType::ore);
	v.push_back(ResourceType::ore);
	return v;
}

std::vector<ResourceType> makeVectorForRoad()
{
	std::vector<ResourceType> v;
	v.push_back(ResourceType::lumber);
	v.push_back(ResourceType::brick);

	return v;
}

std::vector<ResourceType> makeVectorForDevCard()
{
	std::vector<ResourceType> v;
	v.push_back(ResourceType::wool);
	v.push_back(ResourceType::grain);
	v.push_back(ResourceType::ore);

	return v;
}

std::vector<ResourceType> Rules::s_settlementCost = makeVectorForSettlement();
std::vector<ResourceType> Rules::s_cityCost = makeVectorForCity();
std::vector<ResourceType> Rules::s_roadCost = makeVectorForRoad();
std::vector<ResourceType> Rules::s_devCardCost = makeVectorForDevCard();

std::map<ResourceType, std::string> Rules::s_resourceToString = 
{ 
	{ResourceType::lumber, "lumber"},
	{ResourceType::brick, "brick"},
	{ResourceType::wool, "wool"},
	{ResourceType::grain, "grain"},
	{ResourceType::ore, "ore"}
};

std::map<BuildingType, std::string> Rules::s_buildingToString = 
{ 
	{BuildingType::settlement, "settlement"},
	{BuildingType::city, "city"},
	{BuildingType::road, "road"}
};

std::map<PortType, std::string> Rules::s_portTypeToString = 
{ 
	{PortType::genericPort, "generic"},
	{PortType::lumberPort, "lumber"},
	{PortType::brickPort, "brick"},
	{PortType::woolPort, "wool"},
	{PortType::grainPort, "grain"},
	{PortType::orePort, "ore"}
};

std::map<DevCardType, std::string> Rules::s_devCardToString =
{
	{DevCardType::knite, "knite"},
	{DevCardType::monopoly, "monopoly"},
	{DevCardType::roadBuilding, "road building"},
	{DevCardType::victoryPoint, "victory point"},
	{DevCardType::yearOfPlenty, "year of plenty"}
};
