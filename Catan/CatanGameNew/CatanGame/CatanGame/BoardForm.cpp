#include "BoardForm.h"



BoardForm::BoardForm()
{
}

BoardForm::BoardForm(const std::string defaultBodyPath, const Board& board) : IForm(defaultBodyPath)
{
	initIntersectionsPosition(defaultBodyPath);

	initRoadsPosition(defaultBodyPath);

	initHexagonsIDPosition(defaultBodyPath);

	initHexagonsBodyPosition(defaultBodyPath);

	initLegendPosition(defaultBodyPath);

	deleteSpecialCharacters();

	//set dice numbers corresponding to each hex
	setHexagonsDice(board);

	//set hexagon colors. This will not chenge during the game
	setHexagonsColor(board);

	//set hexagon background text to highlite robber
	setHexagonBodyText(board);

	//initialize legend colors in the top left corner of the board
	setLegendTextAndColors();
}

BoardForm::~BoardForm()
{
}

void BoardForm::updateForm(const std::vector<Player>& players, const Board& board)
{
	setHexagonBodyText(board);
	showIntesectionOwner(players);
	showRoadsOwner(players);
}

void BoardForm::showIntersectiunID()
{
	//mark each intersection with its ID
	for (unsigned int id = 0; id < m_intersectionsPosition.size(); id++)
	{
		changeText(m_intersectionsPosition[id], std::to_string(id));
	}
}

void BoardForm::showIntesectionOwner(const std::vector<Player>& players)
{
	//mark each intersection with __
	resetIntersectionText();

	//mark intersection occupied by settlements or cities
	for (auto& player : players)
	{
		setIntersectionsTextAndColor(player);
	}
}

void BoardForm::resetIntersectionText()
{
	for (unsigned int id = 0; id < m_intersectionsPosition.size(); id++)
	{
		changeText(m_intersectionsPosition[id], "__");
	}
}

void BoardForm::resetEdgeText()
{
	for (unsigned int id = 0; id < m_roadsPosition.size(); id++)
	{
		if (m_roadsPosition[id].size() == 1)
		{
			changeText(m_roadsPosition[id][0], "_____");
		}
		else
		{
			changeText(m_roadsPosition[id][0], "  ");

		}
	}
}

void BoardForm::setIntersectionsTextAndColor(const Player& player)
{
	//set settlements
	IResourceHolder::settlements_type const& setts = player.getSettlements();
	for (auto const& settlement : setts)
	{
		uint8_t settlementID = settlement->getNode()->getId();
		//for each intersection ID in the settlements vector change text and color
		changeText(m_intersectionsPosition[settlementID], "se");

		//make text underlined by adding the 8 to the most significant byte
		uint16_t settlementColor = player.getColor() | 0x8000;
		changeColor(m_intersectionsPosition[settlementID], settlementColor);
	}

	//set cities
	for (auto& city : player.getCities())
	{
		uint8_t cityID = city->getNode()->getId();
		//for each intersection ID in the settlements vector change text and color
		changeText(m_intersectionsPosition[cityID], "ci");

		//make text underlined by adding the 8 to the most significant byte
		uint16_t cityColor = player.getColor() | 0x8000;
		changeColor(m_intersectionsPosition[cityID], cityColor);
	}
}

void BoardForm::setRoadsTextAndColor(const Player & player)
{
	//set roads
	for (auto& road : player.getRoads())
	{
		uint8_t roadID = road->getEdge()->getId();
		//change text for the first position of road
		changeText(m_roadsPosition[roadID][0], "  ");

		//change color for all positions of road
		if (m_roadsPosition[roadID].size() == 1)
			changeColor(m_roadsPosition[roadID][0], player.getColor());

		else
		{
			changeColor(m_roadsPosition[roadID][0], player.getColor());
			changeColor(m_roadsPosition[roadID][1], player.getColor());
		}
	}
}

void BoardForm::setHexagonBodyText(const Board & board)
{
	std::string emptyHexText = "             ";
	std::string robberHexText = "xxxxxxxxxxxxxx";

	for (auto& hexagon : board.getVectorOfHex())
	{
		if (hexagon.hasRobber()) 
		{ 
			changeText(m_hexagonsBodyPosition[hexagon.getId()][0], robberHexText);
			changeText(m_hexagonsBodyPosition[hexagon.getId()][1], robberHexText);
			changeText(m_hexagonsBodyPosition[hexagon.getId()][3], robberHexText);
			changeText(m_hexagonsBodyPosition[hexagon.getId()][4], robberHexText);
		}
		else
		{
			changeText(m_hexagonsBodyPosition[hexagon.getId()][0], emptyHexText);
			changeText(m_hexagonsBodyPosition[hexagon.getId()][1], emptyHexText);
			changeText(m_hexagonsBodyPosition[hexagon.getId()][3], emptyHexText);
			changeText(m_hexagonsBodyPosition[hexagon.getId()][4], emptyHexText);
		}
	}
}

void BoardForm::showRoadsID()
{
	//mark each road with its ID
	for (unsigned int id = 0; id < m_roadsPosition.size(); id++)
	{
		changeText(m_roadsPosition[id][0], std::to_string(id));
	}
}

void BoardForm::showRoadsOwner(const std::vector<Player>& players)
{
	//mark each road with "  " or "__"
	resetEdgeText();

	for (auto& player : players)
	{
		setRoadsTextAndColor(player);
	}
}

void BoardForm::showHexagonsID()
{
	uint8_t hexID = 0;
	for (auto& hexagonPosition : m_hexagonsBodyPosition)
	{
		changeText(hexagonPosition[0], std::to_string(hexID));
		hexID++;
	}
}

void BoardForm::hideHexagonID(const Board& board)
{
	for (auto& hexagonPosition : m_hexagonsBodyPosition)
	{
		changeText(hexagonPosition[0], " ");
	}
	setHexagonBodyText(board);
}


void BoardForm::deleteSpecialCharacters()
{
	//change each position of each road to "  "
	for (auto& road : m_roadsPosition)
	{
		if (road.size() == 1)
		{
			changeText(road[0], "_____");
		}
		else
		{
			changeText(road[0], "  ");
			changeText(road[1], "__");
		}
	}
}

void BoardForm::setHexagonsDice(const Board& board)
{
	//for all 19 hexagons, change content with the dice numbers assigned to each hexagon
	for (unsigned int number = 0; number < m_hexagonsIDPosition.size(); number++)
	{
		uint8_t assignedNumber = board.getVectorOfHex()[number].getAssignedNumber();
		if (assignedNumber == 0)
		{
			for (auto& position : m_hexagonsBodyPosition[number])
			{
				changeText(position, "                 ");
			}
		}
		else
		{
			changeText(m_hexagonsIDPosition[number], std::to_string(assignedNumber));
		}
	}
}

void BoardForm::setHexagonsColor(const Board& board)
{
	auto& hexagons = board.getVectorOfHex();
	//for each hexagon
	for (uint8_t hexID = 0; hexID < hexagons.size(); hexID++)
	{
		int color = hexagons.at(hexID).getColor();

		//for each position in hexagon
		for (auto& pos : m_hexagonsBodyPosition[hexID])
			changeColor(pos, color);
	}
}

void BoardForm::setLegendTextAndColors()
{
	changeText(m_legentPositions[0], "Lumber");
	changeText(m_legentPositions[1], "Brick");
	changeText(m_legentPositions[2], "Wool");
	changeText(m_legentPositions[3], "Grain");
	changeText(m_legentPositions[4], "Ore");

	changeColor(m_legentPositions[0], Rules::getConsoleColorOfLumber());
	changeColor(m_legentPositions[1], Rules::getConsoleColorOfBrick());
	changeColor(m_legentPositions[2], Rules::getConsoleColorOfWool());
	changeColor(m_legentPositions[3], Rules::getConsoleColorOfGrain());
	changeColor(m_legentPositions[4], Rules::getConsoleColorOfOre());
}

void BoardForm::initIntersectionsPosition(const std::string defaultBodyPath)
{
	getSinglePositions(m_intersectionsPosition, defaultBodyPath, '#');
}

void BoardForm::initRoadsPosition(const std::string defaultBodyPath)
{
	getMultiPositions(m_roadsPosition, defaultBodyPath, "$%&");
}

void BoardForm::initHexagonsIDPosition(const std::string defaultBodyPath)
{
	getSinglePositions(m_hexagonsIDPosition, defaultBodyPath, '"');
}

void BoardForm::initHexagonsBodyPosition(const std::string defaultBodyPath)
{
	getMultiPositions(m_hexagonsBodyPosition, defaultBodyPath, "\"");
}

void BoardForm::initLegendPosition(const std::string defaultBodyPath)
{
	getSinglePositions(m_legentPositions, defaultBodyPath, '!');
}
