#pragma once

//forward declaration of Factory
class Factory;

#include "IResource.h"

class Brick : public IResource
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	Brick();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~Brick();

	Brick(const Brick& other) = delete;
	Brick& operator=(const Brick& other) = delete;

	Brick(Brick&& other) = default;
	Brick& operator=(Brick&& other) = default;

};

