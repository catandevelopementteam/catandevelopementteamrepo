#pragma once

class Player;

#include "IForm.h"

class CurrentPlayerForm : public IForm
{
public:
	CurrentPlayerForm();
	CurrentPlayerForm(std::string defaultBodyPath);
	~CurrentPlayerForm();

	void updateForm(const Player& player);

private:
	IForm::TextPosition m_name;

	IForm::TextPosition m_resources;
	IForm::TextPosition m_lumbers;
	IForm::TextPosition m_bricks;
	IForm::TextPosition m_wools;
	IForm::TextPosition m_grains;
	IForm::TextPosition m_ores;

	IForm::TextPosition m_hiddenDevCards;
	IForm::TextPosition m_knites;
	IForm::TextPosition m_monopolys;
	IForm::TextPosition m_yearOfPlenty;
	IForm::TextPosition m_roadBuilding;
	IForm::TextPosition m_victoryPoints;

private:
	void setName(std::string text);
	void setResources(std::string text);
	void setLumbers(std::string text);
	void setBricks(std::string text);
	void setWools(std::string text);
	void setGrains(std::string text);
	void setOres(std::string text);
	void setHiddenDevCards(std::string text);
	void setKnites(std::string text);
	void setMonopolys(std::string text);
	void setYearOfPlenty(std::string text);
	void setRoadBuilding(std::string text);
	void setVictoryPoints(std::string text);

private:
	void setBackgrounColor(const Player& player);
};

