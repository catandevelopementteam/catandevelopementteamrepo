#pragma once

//forward declaration of Factory
class Factory;

#include "IResource.h"

class Lumber : public IResource
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	Lumber();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~Lumber();

	Lumber(const Lumber& other) = delete;
	Lumber& operator=(const Lumber& other) = delete;

	Lumber(Lumber&& other) = default;
	Lumber& operator=(Lumber&& other) = default;

};

