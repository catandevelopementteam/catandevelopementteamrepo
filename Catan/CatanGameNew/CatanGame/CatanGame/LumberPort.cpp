#include "LumberPort.h"



LumberPort::LumberPort()
{
	m_type = IPort::PortType::lumberPort;
}


LumberPort::~LumberPort()
{
}

LumberPort & LumberPort::getInstance()
{
	static LumberPort instance;
	return instance;
}

std::string LumberPort::typeName() const
{
	return "LumberPort";
}

IPort::PortType LumberPort::getType() const
{
	return m_type;
}
