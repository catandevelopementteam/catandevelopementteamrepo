#include "Board.h"
#include <chrono>
#include <random>


Board::Board(std::string path) :
	m_path(path)
{
	init_graph();
	init_hexes();	
	init_hexes_resources();
	init_hexes_assigned_no();
	init_colors_of_hexes();
	init_robber();
	//m_graph.setAllEdgesIDs();

	std::string fname = fullFilePath("graph.in");
	m_graph.setAllEdgesIDs(fname);
}

std::string Board::fullFilePath(std::string const& fileName) const
{
	std::string path(m_path);

	// check if path is empty
	if (path.length() > 0)
	{
		// add folder separator if missing
		if ('\\' != path[path.length() - 1])
		{
			path += '\\';
		}
	}
	// and finally, add the file name
	path += fileName;
	return path;
}


void Board::init_graph()
{
	std::string fname = fullFilePath("init_graph.in");
	m_graph.initGraph(fname);
}

void Board::init_hexes()
{
	int numOfHexes;
	std::string fname = fullFilePath("init_hex.in");
	std::ifstream fin(fname);
	fin >> numOfHexes;
	m_hexes.reserve(numOfHexes);
	while (numOfHexes--)
	{
		int h;
		fin >> h;
		Hex hex(h);
		for (int i = 0; i < 6; i++)
		{
			int inter;
			fin >> inter;
			node_ptr it = m_graph.findNode(inter);
			hex.addNode(it);
		}				
		m_hexes.push_back(hex);
	}
}

void Board::init_hexes_resources()
{
	std::vector<ResourceType> hexResources = {
		ResourceType::lumber, ResourceType::lumber, ResourceType::lumber, ResourceType::lumber,
		ResourceType::brick, ResourceType::brick, ResourceType::brick,
		ResourceType::wool, ResourceType::wool, ResourceType::wool, ResourceType::wool,
		ResourceType::grain, ResourceType::grain, ResourceType::grain, ResourceType::grain,
		ResourceType::ore, ResourceType::ore, ResourceType::ore,
		ResourceType::none };


	auto seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(hexResources.begin(), hexResources.end(), std::default_random_engine((unsigned int)seed));

	for (int i = 0; i < Rules::getNumOfHex(); ++i)
	{
		m_hexes[i].setResourceType(hexResources[i]);
	}
}

void Board::init_colors_of_hexes()
{
	for (auto& hex : m_hexes)
	{
		hex.setColor(hex.getResourceType());
	}
}

void Board::init_hexes_assigned_no()
{
	std::vector<int> assignedNo = { 2, 12, 3, 3, 4, 4, 5, 5, 6, 6, 8, 8, 9, 9, 10, 10, 11, 11, 0 };

	auto seed = std::chrono::system_clock::now().time_since_epoch().count() + 13;
	std::shuffle(assignedNo.begin(), assignedNo.end(), std::default_random_engine((unsigned int)seed));

	for (int i = 0; i < Rules::getNumOfHex(); ++i)
	{
		if (m_hexes[i].getResourceType() != ResourceType::none)
		{
			if (assignedNo[i] != 0)
			{
				m_hexes[i].setAssignedNumber(assignedNo[i]);
			}
			else m_hexes[i].setAssignedNumber(assignedNo[i + 1]); ;
		}
		else
		{
			m_hexes[i].setAssignedNumber(0);
		}
	}
}

void Board::init_robber()
{
	auto hex = find_if(m_hexes.begin(), m_hexes.end(), 
		[](const Hex& hex) 
	{
		return hex.getResourceType() == ResourceType::none;
	});
	(*hex).placeRobber();
}


Board::~Board()
{
}


const std::vector<Hex>& Board::getVectorOfHex() const
{
	return m_hexes;
}
Board::node_ptr Board::findNode(int id) const
{
	return m_graph.findNode(id);
}

Board::edge_ptr Board::findEdge(int id1, int id2) const
{
	return m_graph.findEdge(id1, id2);
}

Board::edge_ptr Board::findEdge(int id) const
{
	return m_graph.findEdge(id);
}

std::vector<Board::node_ptr> Board::getNeighbours(int id) const
{
	auto n = findNode(id);
	return m_graph.getNeighbours(n);
}

std::vector<Board::edge_ptr> Board::getIncidentEdges(node_ptr node) const
{
	return m_graph.getIncidentEdges(node);
}

void Board::placeRobber(uint8_t hexID)
{
	m_hexes[hexID].placeRobber();
}

void Board::removeRobber(uint8_t hexID)
{
	m_hexes[hexID].removeRobber();
}

bool Board::visitedEdge(edge_ptr edge, std::vector<edge_ptr> vector)
{
	auto found = std::find_if(vector.begin(), vector.end(), 
		[edge](edge_ptr const& e)
	{ 
		return e == edge; 
	}
	);
	return (found != vector.end());
}

bool Board::visitedNode(node_ptr node, std::vector<node_ptr> vector)
{
	auto found = std::find_if(vector.begin(), vector.end(), 
		[node](node_ptr const& n)
	{
		return n == node; 
	}
	);
	return (found != vector.end());
}

int Board::longestRoad(int currentLength, edge_ptr edge, int PlayerId)
{
	std::vector<Board::edge_ptr> visited_edges;
	std::vector<Board::node_ptr> visited_nodes;
	std::vector<int> lenght_of_roads;
	lengthOfRoads(currentLength, edge, PlayerId, visited_edges, visited_nodes, lenght_of_roads);
	auto max = std::max_element(lenght_of_roads.begin(), lenght_of_roads.end());
	return (*max);
}

bool Board::hasAccesToHex(uint8_t playerID, uint8_t hexID)
{
	std::vector<node_ptr> hexNodes = m_hexes[hexID].getHexNodes();
	auto it = std::find_if(hexNodes.begin(), hexNodes.end(),
		[playerID](node_ptr const& n)
	{
		return n->getOwnerId() == playerID;
	}
	);
	return (it != hexNodes.end());	
}


void Board::lengthOfRoads(int length, edge_ptr edge, int playerId,
	std::vector<edge_ptr>& visited_edges, std::vector<node_ptr>& visited_nodes, std::vector<int>& length_of_roads)
{
	//if the edge is not visited and it belongs to player
	if ((visitedEdge(edge, visited_edges) == false) && (edge->getOwnerId() == playerId))
	{
		//visit the edge
		visited_edges.emplace_back(edge);

		//increase the length
		length++;

		//extract the nodes (ends) of the edge in a vector
		auto pairOfNodes = edge->getPairOfNodes();
		std::vector<node_ptr> nodes;
		nodes.emplace_back(pairOfNodes.first);
		nodes.emplace_back(pairOfNodes.second);

		//for each node of the edge
		for (auto node = nodes.begin(); node != nodes.end(); ++node)
		{
			//if the node belongs to player or to nobody and the node was not visited
			if ((((*node)->getOwnerId() == playerId) || ((*node)->getOwnerId() == 9)) &&
				(visitedNode(*node, visited_nodes) == false))
			{
				//visit the node
				visited_nodes.emplace_back(*node);

				//extract the incident edge_ptr of the node in a vector
				std::vector<edge_ptr> edges = m_graph.getIncidentEdges(*node);

				//for each incident edge of the just visited node								
				for (auto edge2 = edges.begin(); edge2 != edges.end(); ++edge2)
				{
					//if this incident edge belongs to player and is different of the edge visited before
					if (((*edge2)->getOwnerId() == playerId) && (*edge2 != edge))
					{
						//repeat the previous steps using the current length and the current edge
						lengthOfRoads(length, *edge2, playerId, visited_edges, visited_nodes, length_of_roads);
					}
				}
			}
		}
	}
	//save the current length of road in the vector
	length_of_roads.emplace_back(length);
}

