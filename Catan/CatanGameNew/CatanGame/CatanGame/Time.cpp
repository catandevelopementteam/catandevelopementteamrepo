#include "Time.h"



Time::Time()
{
	m_outputText = "";
	start = std::chrono::high_resolution_clock::now();
}

Time::Time(std::string outputText)
{
	m_outputText = outputText;
	start = std::chrono::high_resolution_clock::now();
}


Time::~Time()
{
	end = std::chrono::high_resolution_clock::now();
	duration = end - start;

	float ms = duration.count() * 1000.0f;
	std::cout << m_outputText << ". Timer took " << ms << " ms" << std::endl;
}
