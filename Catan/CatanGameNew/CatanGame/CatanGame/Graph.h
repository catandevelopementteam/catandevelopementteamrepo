#pragma once
#include "Node.h"
#include "Edge.h"
#include "Rules.h"
#include <vector>
#include<array>
#include <utility>
#include <iostream>
#include "Rules.h"
#include<fstream>


/*
				Node & Edge Graph
	links all the Nodes between them with the proper Edges
*/

class Graph
{	
	using node_ptr = std::shared_ptr<Node>;
	using edge_ptr = std::shared_ptr<Edge>;

private:
	int m_noOfNodes; //number of nodes
	std::vector<node_ptr> m_nodes; //all the nodes from the board
	std::vector<edge_ptr> m_edges; //all the edges from the board

	//the adjacency list (each Node has a vector of all linked Nodes and the Edges between them):
	std::vector<std::vector <std::pair<node_ptr, edge_ptr>>> m_adj; 

public:	
	Graph();
	~Graph();

	//addEdge in the adjacency list
	void addEdge(node_ptr u, node_ptr v, edge_ptr wt);

	//methods to find a node/edge
	node_ptr findNode(int id) const;
	edge_ptr findEdge(int Node1Id, int Node2Id) const; //search an edge using the Nodes ids that defines it
	edge_ptr findEdge(int id) const; //search an edge using its id

	//method that returns the neighbours of a node
	std::vector<node_ptr> getNeighbours(node_ptr node) const;

	//method that returns the incident edges of a node
	std::vector<edge_ptr> getIncidentEdges(node_ptr node) const;
		
	
	//initialize the graph using an input file
	void initGraph(std::string);

	//set IDs for edges and sorts the vector of edges by ids
	void setAllEdgesIDs(std::string);

	//optional: method that prints the graph
	//void printGraph() const;


};



