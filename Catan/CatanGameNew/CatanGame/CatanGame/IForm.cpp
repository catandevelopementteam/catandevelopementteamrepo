#include "IForm.h"

IForm::IForm()
{
}

IForm::IForm(std::string defaultBodyPath)
{
	initFormBody(defaultBodyPath, m_formBody);
}

IForm::IForm(int rows, int columns)
{
	m_formRows = rows;
	m_formColumns = columns;
	int iterations = rows * columns;
	m_formBody.reserve(iterations);

	for (int i = 0; i < iterations; i++)
	{
		m_formBody.emplace_back(Cell_info(' '));
	}
}

IForm::~IForm()
{
}

uint16_t IForm::getRows() const noexcept
{
	return m_formRows;
}

uint16_t IForm::getColumns() const noexcept
{
	return m_formColumns;
}

const std::vector<Cell_info>& IForm::getFormBody()
{
	return m_formBody;
}

void IForm::initFormBody(std::string path, std::vector<Cell_info>& formText)
{
	m_formRows = 0;
	m_formColumns = 0;
	if (m_formBody.size() == 0)
	{
		//initialize the body of the form with default cell values from file
		std::ifstream form(path);
		std::string row;
		if (form.is_open())
		{
			while (getline(form, row))		//get each line from the file
			{
				m_formColumns = (uint8_t)row.size();

				for (unsigned int i = 0; i < row.size(); i++)
				{
					formText.emplace_back(Cell_info(row[i], 0x0007));		//get each character and push it into the vector
				}
				
				m_formRows++;
			}
		}
		else
		{
			std::cout << "cannot open file" << std::endl;
		}
		form.close();
	}

}

void IForm::changeText(IForm::TextPosition pos, std::string text)
{
	deleteText(pos);
	setText(pos, text);
}

void IForm::changeCharacter(const int rowIndex, const int colIndex, char character)
{
	m_formBody[(rowIndex - 1) * m_formRows + colIndex].setSymbol(character);
}

void IForm::changeColor(IForm::TextPosition pos, int color)
{
	for (int i = 0; i < pos.getCapacity(); i++)
	{
		uint16_t startingCell = (pos.getRows()) * m_formColumns + pos.getColumns();
		m_formBody[startingCell + i].setColor(color);
	}
}

void IForm::copyFormToForm(const IForm& source, IForm& destination, IForm::TextPosition offset)
{
	//copy each element from source to destination tacking into account the offset
	for (int row = 0; row < source.getRows(); row++)
	{
		for (int col = 0; col < source.getColumns(); col++)
		{
			//find the exact destination cell based on the offset of the source form in the destination form
			uint16_t destinationCell = (row + offset.getRows()) * destination.getColumns() + col + offset.getColumns();
			uint16_t sourceCell = (row * source.getColumns()) + col;
			destination.m_formBody[destinationCell] = source.m_formBody[sourceCell];
		}
	}
}

void IForm::getMultiPositions(std::vector<std::vector<IForm::TextPosition> >& multiPositionVector, std::string path, std::string positionIdentifiers)
{
	std::ifstream form(path);
	std::string rowText;
	uint8_t rowIndex = 0;
	if (form.is_open())
	{
		while (getline(form, rowText))		//get each line from the file
		{
			uint8_t columnIndex = 0;

			for (auto& character : rowText)	//iterate trough every char in the current line
			{
				//if current char is found in string of special characters
				if (positionIdentifiers.find(rowText[columnIndex]) != std::string::npos)
				{
					std::vector<IForm::TextPosition> positions;

					//create multiple positions for the forward orientated row
					if (rowText[columnIndex] == '%')
					{
						createForwardRowPositions(positions, rowIndex, columnIndex);
					}
					//create multiple positions for the backward orientated row
					else if(rowText[columnIndex] == '&')
					{
						createBackwardRowPositions(positions, rowIndex, columnIndex);
					}
					//create multiple positions for the hexagon
					else if (rowText[columnIndex] == '"')
					{
						createHexPositions(positions, rowIndex, columnIndex);
					}
					//create multiple positions for the horizontal row
					else if (rowText[columnIndex] == '$')
					{
						//get the length of the TextPosition based on the ASCII character encoded in the file
						uint8_t formLength = (int)rowText[columnIndex + 1] - 65;
						positions.push_back(IForm::TextPosition(rowIndex, columnIndex, formLength));
					}

					multiPositionVector.push_back(positions);
				}
				columnIndex++;
			}
			rowIndex++;
		}
	}
	else
	{
		std::cout << "cannot open file" << std::endl;
	}
	form.close();
}

void IForm::getSinglePositions(std::vector<IForm::TextPosition>& singlePositionVector, std::string path, char positionIdentifier)
{
	std::ifstream form(path);
	std::string rowText;
	uint8_t rowIndex = 0;
	if (form.is_open())
	{
		while (getline(form, rowText))		//get each line from the file
		{
			uint8_t columnIndex = 0;

			for (auto& character : rowText)	//iterate trough every char in the current line
			{
				if (rowText[columnIndex] == positionIdentifier)
				{
					//get the length of the TextPosition based on the ASCII character encoded in the file
					uint8_t formLength = (int)rowText[columnIndex + 1] - 65;

					singlePositionVector.push_back(IForm::TextPosition(rowIndex, columnIndex, formLength));
				}
				columnIndex++;
			}
			rowIndex++;
		}
	}
	else
	{
		std::cout << "cannot open file" << std::endl;
	}
	form.close();
}

void IForm::deleteText(IForm::TextPosition pos)
{
	for (int i = 0; i < pos.getCapacity(); i++)
	{
		uint16_t startingCell = (pos.getRows()) * m_formColumns + pos.getColumns();
		m_formBody[startingCell + i].setSymbol(' ');
	}
}

void IForm::setText(IForm::TextPosition pos, std::string text)
{
	uint16_t startingCell = (pos.getRows()) * m_formColumns + pos.getColumns();
	uint16_t min = pos.getCapacity() < text.size() ? pos.getCapacity() : (uint16_t)text.size();
	for (uint8_t i = 0; i < min; i++)
	{
		m_formBody[startingCell + i].setSymbol(text.at(i));
	}
}

void IForm::createHexPositions(std::vector<IForm::TextPosition>& hexPositions, uint8_t row, uint8_t col)
{
	hexPositions.push_back(IForm::TextPosition(row - 2, col - 3, 7));
	hexPositions.push_back(IForm::TextPosition(row - 1, col - 4, 9));
	hexPositions.push_back(IForm::TextPosition(row, col - 4, 9));
	hexPositions.push_back(IForm::TextPosition(row + 1, col - 3, 7));
	hexPositions.push_back(IForm::TextPosition(row + 2, col - 2, 5));
}

void IForm::createForwardRowPositions(std::vector<IForm::TextPosition>& forwardRowPositions, uint8_t row, uint8_t col)
{
	forwardRowPositions.push_back(IForm::TextPosition(row, col, 2));
	forwardRowPositions.push_back(IForm::TextPosition(row + 1, col - 1, 2));
}

void IForm::createBackwardRowPositions(std::vector<IForm::TextPosition>& backwardRowPositions, uint8_t row, uint8_t col)
{
	backwardRowPositions.push_back(IForm::TextPosition(row, col, 2));
	backwardRowPositions.push_back(IForm::TextPosition(row + 1, col + 1, 2));
}

IForm::TextPosition::TextPosition()
{
}

IForm::TextPosition::TextPosition(int row, int column, int capacity) :m_posRows(row), m_posColumns(column), m_capacity(capacity)
{
}

IForm::TextPosition::~TextPosition()
{
}

uint16_t IForm::TextPosition::getRows() const noexcept
{
	return m_posRows;
}

uint16_t IForm::TextPosition::getColumns() const noexcept
{
	return m_posColumns;
}

uint16_t IForm::TextPosition::getCapacity() const noexcept
{
	return m_capacity;
}

