#include "WoolPort.h"



WoolPort::WoolPort()
{
	m_type = IPort::PortType::woolPort;
}


WoolPort::~WoolPort()
{
}

WoolPort & WoolPort::getInstance()
{
	static WoolPort instance;
	return instance;
}

std::string WoolPort::typeName() const
{
	return "WoolPort";
}

IPort::PortType WoolPort::getType() const
{
	return m_type;
}
