#pragma once

enum class ResourceType
{
	lumber,
	brick,
	wool,
	grain,
	ore,
	none
};