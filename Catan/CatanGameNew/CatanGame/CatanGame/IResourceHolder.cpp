#include "IResourceHolder.h"
#include <cassert>


IResourceHolder::IResourceHolder()
{
}


IResourceHolder::~IResourceHolder()
{
}

const IResourceHolder::lumbers_type& IResourceHolder::getLumbers() const
{
	return m_lumbers;
}

const IResourceHolder::bricks_type& IResourceHolder::getBricks() const
{
	return m_bricks;
}

const IResourceHolder::wools_type& IResourceHolder::getWools() const
{
	return m_wools;
}

const IResourceHolder::grains_type& IResourceHolder::getGrains() const
{
	return m_grains;
}

const IResourceHolder::ores_type& IResourceHolder::getOres() const
{
	return m_ores;
}
const IResourceHolder::roads_type& IResourceHolder::getRoads() const
{
	return m_roads;
}

IResourceHolder::roads_type & IResourceHolder::getRoads()
{
	return m_roads;
}

const IResourceHolder::settlements_type& IResourceHolder::getSettlements() const
{
	return m_settlements;
}

IResourceHolder::settlements_type & IResourceHolder::getSettlements()
{
	return m_settlements;
}

const IResourceHolder::cities_type& IResourceHolder::getCities() const
{
	return m_cities;
}

IResourceHolder::cities_type & IResourceHolder::getCities()
{
	return m_cities;
}

void IResourceHolder::buyLumber(lumber_ptr lumber)
{
	assert(nullptr != lumber);
	m_lumbers.push(std::move(lumber));
}

IResourceHolder::lumber_ptr IResourceHolder::sellLumber()
{
	return m_lumbers.pop();
}

void IResourceHolder::buyBrick(brick_ptr brick)
{
	assert(nullptr != brick);
	m_bricks.push(std::move(brick));
}

IResourceHolder::brick_ptr IResourceHolder::sellBrick()
{
	return m_bricks.pop();	
}

void IResourceHolder::buyWool(wool_ptr wool)
{
	assert(nullptr != wool);
	m_wools.push(std::move(wool));
}

IResourceHolder::wool_ptr IResourceHolder::sellWool()
{
	return m_wools.pop();		
}

void IResourceHolder::buyGrain(grain_ptr grain)
{
	assert(nullptr != grain);
	m_grains.push(std::move(grain));
}

IResourceHolder::grain_ptr IResourceHolder::sellGrain()
{
		return m_grains.pop();		
}

void IResourceHolder::buyOre(ore_ptr ore)
{
	assert(nullptr != ore);
	m_ores.push(std::move(ore));
}

IResourceHolder::ore_ptr IResourceHolder::sellOre()
{
	return m_ores.pop();	
}

void IResourceHolder::addSettlement(settlement_ptr sett)
{
	assert(nullptr != sett);
	m_settlements.push(std::move(sett));
}

IResourceHolder::settlement_ptr IResourceHolder::getSettlement()
{
	return m_settlements.pop();
}

void IResourceHolder::addCity(city_ptr city)
{
	assert(nullptr != city);
	m_cities.push(std::move(city));
}

IResourceHolder::city_ptr IResourceHolder::getCity()
{
	return m_cities.pop();
}

void IResourceHolder::addRoad(road_ptr road)
{
	assert(nullptr != road);
	m_roads.push(std::move(road));
}

IResourceHolder::road_ptr IResourceHolder::getRoad()
{
	return m_roads.pop();
}


