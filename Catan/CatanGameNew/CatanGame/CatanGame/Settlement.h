#pragma once

//forward declaration of Factory
class Factory;

#include "Node.h"
#include "IBuilding.h"

class Settlement : public IBuilding
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	//no initialisation in constructor because the default value for a smart pointer is nullptr
	std::shared_ptr<Node> m_node;
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

private:
	Settlement();

public:
	void setNode(std::shared_ptr<Node> intersection);
	std::shared_ptr<Node> getNode();

	~Settlement();

	Settlement(const Settlement& other) = delete;
	Settlement& operator=(const Settlement& other) = delete;

	Settlement(Settlement&& other) = default;
	Settlement& operator=(Settlement&& other) = default;

};




