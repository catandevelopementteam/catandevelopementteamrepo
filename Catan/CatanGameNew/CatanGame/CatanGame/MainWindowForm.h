#pragma once

#include "IForm.h"
#include "PlayerForm.h"
#include "BoardForm.h"
#include "CurrentPlayerForm.h"
#include "DiceForm.h"
#include "OptionsForm.h"
#include "Player.h"

class MainWindowForm : public IForm
{
private:
	std::vector<PlayerForm> m_playerForms;
	std::vector<IForm::TextPosition> m_playerFrom_pos;

	BoardForm m_boardForm;
	IForm::TextPosition m_boardForm_pos;

	CurrentPlayerForm m_currentPlayerForm;
	IForm::TextPosition m_currentPlayerForm_pos;

	OptionsForm m_optionsForm;
	IForm::TextPosition m_optionsForm_pos;

	DiceForm m_diceForm;
	IForm::TextPosition m_diceForm_pos;

public:
	MainWindowForm();

	MainWindowForm(std::string defaultBodyPath, const Board& board);
	MainWindowForm(int rows, int columns, const Board& board);
	~MainWindowForm();

	void updateForm(const Player& currentPlayer, 
		const std::vector<Player>& players, 
		const std::pair<uint8_t, uint8_t> dice,
		const Board& board);

	void updateCurrentPlayerForm(const Player& player);
	void updatePlayerForms(const std::vector<Player>& players);
	void updateDiceForm(const std::pair<uint8_t, uint8_t> dice);
	void updateBoardForm(const std::vector<Player>& players, const Board& board);
	void updateOptionsForm();

	void showIntersectiunID();
	void showIntesectionOwner(const std::vector<Player>& players);
	void showRoadsID();
	void showRoadsOwner(const std::vector<Player>& players);
	void showHexagonsID();
	void hideHexagonID(const Board& board);
	void showMessages(std::vector<std::string> messages);

private:
	void initForms(const Board& board);
	void initFormPositions();
	void initBackground();

	//make last character of each line an EOL character (\n)
	void makeEOL();

	//updates all the forms
	void updateAllForms(const Player& currentPlayer, 
		const std::vector<Player>& players, 
		const std::pair<uint8_t, uint8_t> dice, 
		const Board& board);

	//copy from each form to the mainWindowForm
	void includeAllForms();

	//update OptionsForm
	void includePlayerForms();
	void includeCurrentPlayerForm();
	void includeBoardForm();
	void includeOptionsForm();
	void includeDiceForm();

};

