#pragma once

#include "Cell_info.h"
#include "IDraw.h"
#include "MainWindowForm.h"
#include "Player.h"

#include <string>

class DrawConsole : public IDraw
{
private:
	MainWindowForm m_mainWindowForm;

public:
	DrawConsole(const Board& board, 
		const std::vector<Player>& players, 
		const Player& currentPlayer);

	~DrawConsole();

	// Inherited via IDraw

public:
	virtual void draw() override;

	virtual void displayMessage(const std::string message) override;
	virtual void highlightIntersections() override;
	virtual void unhighlightIntersections(const std::vector<Player>& players) override;
	virtual void highlightEdges() override;
	virtual void unhighlightEdges(const std::vector<Player>& players) override;
	virtual void highlightHexagons() override;
	virtual void unhighlightHexagons(const Board& board) override;


	virtual void turnKniteOrNot() override;

	virtual void chooseTurnDevCard() override;

	virtual void chooseTurnKnite() override;

	virtual void chooseHex() override;

	virtual void choosePlayer(const std::vector<Player>& players, const Player& currentPlayer) override;

	virtual void chooseDiscardedCards(uint8_t totalNumber) override;

	virtual void chooseAction() override;

	virtual void chooseBuild() override;

	virtual void chooseIntersection() override;

	virtual void chooseEdge() override;

	virtual void chooseTrade() override;

	virtual void choosePortType() override;

	virtual void chooseResourcesToReceive() override;

	virtual void chooseResourcesToTrade() override;

	virtual void chooseResourceToMonopolize() override;

	virtual void updateCurrentPlayer(const Player& player) override;

	virtual void updatePlayersForm(const std::vector<Player>& players) override;

	virtual void updateBoardForm(const std::vector<Player>& players, const Board& board) override;

	virtual void updateDiceForm(const std::pair<uint8_t, uint8_t> dice) override;

	virtual void updateBoardAndPlayers(const std::vector<Player>& players, const Player& player, const Board& board) override;

};

std::ostream& operator<<(std::ostream& out, const Cell_info& cell);
