#pragma once

enum class PortType
{
	none,
	genericPort,
	brickPort,
	lumberPort,
	orePort,
	woolPort,
	grainPort
};