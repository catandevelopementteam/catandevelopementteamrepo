#include "Brick.h"

uint8_t Brick::s_instancesCreated = 0;

Brick::Brick()
{
	m_type = ResourceType::brick;
	m_color = Rules::getConsoleColorOfBrick();
	m_instancesAllowed = Rules::getNumOfBrickCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

Brick::~Brick()
{
}

void Brick::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}