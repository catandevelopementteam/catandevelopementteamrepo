#pragma once

#include <string>
#include <iostream>
#include <vector>

#include "DevCardType.h"
#include "Rules.h"

class IDevCard
{
protected:
	DevCardType m_type;
	uint8_t m_instancesAllowed;

protected:
	virtual void checkForInstanceOverflow() = 0;

	IDevCard();
	IDevCard(const IDevCard&) = default;
	IDevCard& operator=(const IDevCard&) = default;
	IDevCard(IDevCard&&) = default;
	IDevCard& operator=(IDevCard&&) = default;

public:
	virtual ~IDevCard();
	virtual DevCardType getType();
};

