#pragma once

//forward declaration of Factory
class Factory;

#include "IResource.h"

class Wool : public IResource
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	Wool();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~Wool();

	Wool(const Wool& other) = delete;
	Wool& operator=(const Wool& other) = delete;

	Wool(Wool&& other) = default;
	Wool& operator=(Wool&& other) = default;

};

