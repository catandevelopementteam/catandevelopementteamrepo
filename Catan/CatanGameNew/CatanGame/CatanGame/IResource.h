#pragma once

#include <string>
#include <iostream>
#include <vector>

#include "Rules.h"
#include "ResourceType.h"


class IResource
{
protected:
	ResourceType m_type;
	uint8_t m_color;
	uint8_t m_instancesAllowed;

protected:
	virtual void checkForInstanceOverflow() = 0;

	IResource();
	IResource(const IResource&) = default;
	IResource& operator=(const IResource&) = default;
	IResource(IResource&&) = default;
	IResource& operator=(IResource&&) = default;

public:
	virtual ~IResource();
	virtual ResourceType getType();
	virtual uint8_t getColor();
};


