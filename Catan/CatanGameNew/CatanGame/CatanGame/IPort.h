#pragma once
#include <string>
class IPort
{
public:
	enum class PortType
	{
		none,
		genericPort,
		brickPort,
		lumberPort,
		orePort,
		woolPort,
		grainPort
	};

	virtual std::string typeName() const = 0;	
	virtual PortType getType() const = 0;
	virtual ~IPort() = 0;
};

