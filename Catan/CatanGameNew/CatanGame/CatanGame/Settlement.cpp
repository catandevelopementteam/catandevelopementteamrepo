#include "Settlement.h"

uint8_t Settlement::s_instancesCreated = 0;

Settlement::Settlement()
{
	m_type = BuildingType::settlement;
	m_instancesAllowed = Rules::getNumOfSettlements();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

void Settlement::setNode(std::shared_ptr<Node> intersection)
{
	m_node = intersection;
}

std::shared_ptr<Node> Settlement::getNode()
{
	return m_node;
}

Settlement::~Settlement()
{
}

void Settlement::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}