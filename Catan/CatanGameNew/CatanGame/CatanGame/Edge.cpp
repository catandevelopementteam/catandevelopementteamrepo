#include "Edge.h"

Edge::Edge()
{
}


Edge::Edge(const Edge & other)
{
	*this = other;
}


Edge::Edge(node_ptr id1, node_ptr id2)
{
	m_id1 = id1;
	m_id2 = id2;	
	m_ownerId = 9;
}


Edge & Edge::operator=(const Edge & other)
{
	m_id1 = other.m_id1;
	m_id2 = other.m_id2;
	m_id = other.m_id;	
	m_ownerId = other.m_ownerId;
	return *this;
}

std::pair<Edge::node_ptr, Edge::node_ptr> Edge::getPairOfNodes() const
{
	return std::make_pair(m_id1, m_id2);
}

int Edge::getId1() const
{
	return m_id1->getId();
}

int Edge::getId2() const
{
	return m_id2->getId();
}

void Edge::setId(int id)
{
	m_id = id;
}

int Edge::getId() const
{
	return m_id;
}

void Edge::setOwnerId(int ownerId)
{
	m_ownerId = ownerId;
}

int Edge::getOwnerId() const
{
	return m_ownerId;
}


Edge::~Edge()
{
}
