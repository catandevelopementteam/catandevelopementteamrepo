#pragma once

//forward declaration of Factory
class Factory;

#include "Edge.h"
#include "IBuilding.h"

class Road : public IBuilding
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

	using edge_ptr = std::shared_ptr<Edge>;

private:
	
	edge_ptr m_edge;

private:
	Road();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	void setEdge(edge_ptr edge);
	edge_ptr getEdge() const;

	~Road();

	Road(const Road& other) = delete;
	Road& operator=(const Road& other) = delete;

	Road(Road&& other) = default;
	Road& operator=(Road&& other) = default;

};




