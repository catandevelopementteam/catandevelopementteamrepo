#pragma once

//forward declaration of Factory
class Factory;

#include "IResource.h"

class Ore : public IResource
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	Ore();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~Ore();

	Ore(const Ore& other) = delete;
	Ore& operator=(const Ore& other) = delete;

	Ore(Ore&& other) = default;
	Ore& operator=(Ore&& other) = default;

};

