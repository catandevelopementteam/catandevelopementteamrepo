#pragma once

#include <iostream>
#include <chrono>
#include <thread>
#include <string>

class Time
{
private:
	std::string m_outputText;
	std::chrono::time_point<std::chrono::steady_clock> start, end;
	std::chrono::duration<float> duration;
public:
	Time();
	Time(std::string outputText);
	~Time();
};

