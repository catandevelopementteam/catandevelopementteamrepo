#include "BrickPort.h"



BrickPort::BrickPort()
{
	m_type = IPort::PortType::brickPort;
}


BrickPort::~BrickPort()
{
}

BrickPort & BrickPort::getInstance()
{
	static BrickPort instance;
	return instance;
}

std::string BrickPort::typeName() const
{
	return "BrickPort";
}

IPort::PortType BrickPort::getType() const
{
	return m_type;
}
