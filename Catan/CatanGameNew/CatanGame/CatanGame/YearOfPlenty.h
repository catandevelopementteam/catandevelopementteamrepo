#pragma once

//forward declaration of Factory
class Factory;

#include "IDevCard.h"

class YearOfPlenty : public IDevCard
{
public:
	//allow Factory to acces the private constructor
	friend class Factory;

private:
	YearOfPlenty();
	static uint8_t s_instancesCreated;
	void checkForInstanceOverflow() override;

public:
	~YearOfPlenty();

	YearOfPlenty(const YearOfPlenty& other) = delete;
	YearOfPlenty& operator=(const YearOfPlenty& other) = delete;

	YearOfPlenty(YearOfPlenty&& other) = default;
	YearOfPlenty& operator=(YearOfPlenty&& other) = default;

};

