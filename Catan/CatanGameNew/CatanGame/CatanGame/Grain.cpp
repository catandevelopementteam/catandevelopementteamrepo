#include "Grain.h"

uint8_t Grain::s_instancesCreated = 0;

Grain::Grain()
{
	m_type = ResourceType::grain;
	m_color = Rules::getConsoleColorOfGrain();
	m_instancesAllowed = Rules::getNumOfGrainCards();

	//if the maximum allowed instances reached trow exception
	checkForInstanceOverflow();

	s_instancesCreated++;
}

Grain::~Grain()
{
}


void Grain::checkForInstanceOverflow()
{
	if (s_instancesCreated >= m_instancesAllowed)
	{
		throw std::string("Maximum allowed instances reached");
	}
}
