#include "GenericPort.h"



GenericPort::GenericPort()
{
	m_type = IPort::PortType::genericPort;
}


GenericPort::~GenericPort()
{
}

GenericPort & GenericPort::getInstance()
{
	static GenericPort instance;
	return instance;
}

std::string GenericPort::typeName() const
{
	return "GenericPort";
}

IPort::PortType GenericPort::getType() const
{
	return m_type;
}
