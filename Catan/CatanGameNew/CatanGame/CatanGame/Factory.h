#pragma once

#include "IResource.h"
#include "Lumber.h"
#include "Brick.h"
#include "Wool.h"
#include "Grain.h"
#include "Ore.h"
#include "Rules.h"
#include "IDevCard.h"
#include "Knite.h"
#include "Monopoly.h"
#include "RoadBuilding.h"
#include "YearOfPlenty.h"
#include "VictoryPoint.h"

#include <vector>

class Factory
{
public:
	static Factory& getInstance();

private:
	Factory();
	~Factory();

public:
	Factory(const Factory&) = delete;
	Factory& operator=(const Factory&) = delete;
	Factory(Factory&&) = delete;
	Factory& operator=(Factory&&) = delete;

public:
	template <class BankItem>
	std::vector<std::unique_ptr<BankItem>> createBankItem()
	{
		std::vector<std::unique_ptr<BankItem>> bankItems;

		while (true)
		{
			//get instances until we get an exception meaning that we cannot instantiate anymore
			try
			{
				bankItems.emplace_back(std::make_unique<BankItem>(std::move(BankItem{})));
			}
			catch (std::string)
			{
				break;
			}
		}
		return bankItems;
	}

};
