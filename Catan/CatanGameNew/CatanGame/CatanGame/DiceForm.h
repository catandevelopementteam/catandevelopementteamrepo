#pragma once

#include "IForm.h"

class DiceForm : public IForm
{
public:
	DiceForm();
	DiceForm(std::string defaultBodyPath);
	~DiceForm();

private:
	IForm::TextPosition m_FirstDicePos;
	IForm::TextPosition m_SecondDicePos;

	IForm m_dice0;
	IForm m_dice1;
	IForm m_dice2;
	IForm m_dice3;
	IForm m_dice4;
	IForm m_dice5;
	IForm m_dice6;

public:
	void updateForm(const std::pair<uint8_t, uint8_t> dice);

private:
	void updateDice(uint8_t diceNumber, const IForm::TextPosition& offset);
};

