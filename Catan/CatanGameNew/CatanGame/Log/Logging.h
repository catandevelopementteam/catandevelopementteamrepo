#pragma once

#include <fstream>
#include <string>
#include <chrono>
#include <iomanip>

class /*__declspec(dllexport)*/ Logging
{
public:
	__declspec(dllexport) Logging(std::string path);
	__declspec(dllexport) ~Logging();

	__declspec(dllexport) void log(std::string message);

private:
	std::fstream m_file;
	std::string m_path;

};

