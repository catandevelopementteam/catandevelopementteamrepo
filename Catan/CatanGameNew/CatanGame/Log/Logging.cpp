#include "Logging.h"


Logging::Logging(std::string path)
{
	m_path = path;
	m_file.open(path, std::ios_base::out | std::ios_base::trunc);
	m_file.close();
}

Logging::~Logging()
{
	m_file.close();
}

void Logging::log(std::string message)
{
	m_file.open(m_path, std::ios_base::out | std::ios_base::app);

	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::time_t now_t = std::chrono::system_clock::to_time_t(now);
	std::tm tm;
	localtime_s(&tm, &now_t);

	m_file << "[" << std::put_time(&tm, "%T") << "]" << message << std::endl;
	m_file.close();
}
