#include "stdafx.h"
#include "CppUnitTest.h"
#include "../CatanGame/ChecksManager.h"
#include "../CatanGame/TransferManager.h"
#include "../CatanGame/Board.h"
#include "../CatanGame/Player.h"
#include "../CatanGame/Rules.h"
#include "../CatanGame/Bank.h"




using namespace Microsoft::VisualStudio::CppUnitTestFramework;

/*
!! Due to the actual Factory design that initilizes all bank items
(resources/buildings/dev cards) as multiton, with a static getInstance(),
they cannot be instantiated in more than one single test method,
so not much testing can be done
*/


namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(CanBuildAtAnEmptyNode)
		{
			Board m_board("..\\CatanGame");
			ChecksManager m_cm;

			Assert::IsTrue(m_cm.canBuildAtNode(m_board, 2));
		}

		TEST_METHOD(CanUpgradeToCity) //The only method that can use bank items
		{
			Board m_board("..\\CatanGame");
			Bank m_bank;
			ChecksManager m_cm;
			TransferManager m_tm;
			Player player1(0, "John", 3);
			Player player2(1, "Marry", 3);

			m_tm.transferBuilding(BuildingType::settlement, m_bank, player1);
			player1.buildSettlement(m_board.findNode(46));
			m_tm.transferBuilding(BuildingType::settlement, m_bank, player1);
			player1.buildSettlement(m_board.findNode(34));
			m_tm.transferBuilding(BuildingType::settlement, m_bank, player2);
			player2.buildSettlement(m_board.findNode(48));

			Assert::IsTrue(m_cm.canUpgrade(player1, 34));
		}


		TEST_METHOD(CanBuildAtEdgeLinkedToSett)
		{
			Board m_board("..\\CatanGame");
			ChecksManager m_cm;
			TransferManager m_tm;
			Player player1(0, "p1", 3);
			Player player2(1, "p2", 3);

			m_board.findNode(3)->setOwnerId(1);

			Assert::IsTrue(m_cm.canBuildAtEdge(player2, m_board, 1));
		}

		TEST_METHOD(CanBuildAtEdgeLinkedToRoad)
		{
			Board m_board("..\\CatanGame");
			ChecksManager m_cm;
			TransferManager m_tm;
			Player player1(0, "p1", 3);
			Player player2(1, "p2", 3);

			m_board.findEdge(3)->setOwnerId(1);

			Assert::IsTrue(m_cm.canBuildAtEdge(player2, m_board, 5));
		}

		TEST_METHOD(LongestRoadForPlayer)
		{
			Board m_board("..\\CatanGame");
			Player player1(0, "p1", 3);

			m_board.findEdge(3)->setOwnerId(1);
			m_board.findEdge(6)->setOwnerId(1);
			m_board.findEdge(1)->setOwnerId(1);
			m_board.findEdge(0)->setOwnerId(1);

			Assert::AreEqual(3, m_board.longestRoad(0, m_board.findEdge(3), 1));
		}

		TEST_METHOD(LongestRoadForPlayerWhenInterrupted)
		{
			Board m_board("..\\CatanGame");
			Player player1(0, "p1", 3);
			Player player2(1, "p2", 3);

			m_board.findEdge(27)->setOwnerId(1);
			m_board.findEdge(32)->setOwnerId(1);
			m_board.findEdge(23)->setOwnerId(1);
			m_board.findEdge(19)->setOwnerId(1);
			m_board.findEdge(24)->setOwnerId(1);
			m_board.findEdge(28)->setOwnerId(1);

			m_board.findNode(22)->setOwnerId(0);

			Assert::AreEqual(4, m_board.longestRoad(0, m_board.findEdge(27), 1));
		}

		TEST_METHOD(HasAccessToHex)
		{
			Board m_board("..\\CatanGame");
			Player player1(0, "p1", 3);

			m_board.findNode(5)->setOwnerId(1);
			m_board.findNode(29)->setOwnerId(1);

			Assert::IsTrue(m_board.hasAccesToHex(1, 2));
		}

		TEST_METHOD(NodeConstructor)
		{
			Node node(3);

			Assert::IsTrue((node.getId() == 3) && (node.getOwnerId() == 9));
		}

		TEST_METHOD(EdgeConstructor)
		{
			std::shared_ptr<Node> node1 = std::make_shared<Node>(33);
			std::shared_ptr<Node> node2 = std::make_shared<Node>(27);
			Edge edge(node1, node2);

			Assert::IsTrue((edge.getId1() == 33) && (edge.getId2() == 27));
		}

		TEST_METHOD(HexConstructor)
		{
			Hex hex(4);

			Assert::IsTrue((hex.getId() == 4) && (hex.hasRobber() == false));
		}

		TEST_METHOD(PlayerConstructor)
		{
			Player player(1, "John", 3);

			Assert::IsTrue((player.getId() == 1) && (player.getName() == "John")
				&& (player.hasLargestArmy() == false) && (player.hasLongestRoad() == false)
				&& (player.getColor() == 3));
		}

	};
}